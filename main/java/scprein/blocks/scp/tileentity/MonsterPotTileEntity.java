package alexsocol.scprein.blocks.scp.tileentity;

import alexsocol.scprein.entity.mob.Vialer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class MonsterPotTileEntity extends TileEntity {

    private int timer;

    @Override
    public void updateEntity() {
        timer++;
        if (timer >= 600) {
            if (!worldObj.isRemote) {
                int rand = worldObj.rand.nextInt(3);
                for (int i = 0; i < rand; ++i) {
                    Vialer ent = new Vialer(worldObj);
                    ent.setPosition(xCoord + 1, yCoord + 1, zCoord + 1);
                    worldObj.spawnEntityInWorld(ent);
                    worldObj.spawnParticle("flame", xCoord, yCoord, zCoord, 0, 0, 0);
                }
            }
            timer = 0;
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        timer = p_145839_1_.getInteger("time");
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        p_145841_1_.setInteger("time", timer);
    }
}
