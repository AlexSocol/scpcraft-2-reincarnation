package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemAxe;

public class BladeAxe extends ItemAxe {

    public BladeAxe() {
        super(SCPMain.BLADE);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setTextureName(ModInfo.MODID + ":BladeAxe");
        this.setUnlocalizedName("BladeAxe");
    }
}
