package alexsocol.scprein;

public class ModInfo {

    public static final String MODID = "scprein";
    public static final String NAME = "SCP-Craft 2: Reincarnation";
    public static final String VERSION = "0.0.3";
}
