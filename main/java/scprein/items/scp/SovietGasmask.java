package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class SovietGasmask extends ItemArmor {

    public SovietGasmask(int id, int armorType) {
        super(SCPMain.DClass, id, armorType);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setMaxStackSize(1);
    }

    @Override
    public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, String type) {
        return ModInfo.MODID + ":textures/model/armor/SovietGasmask.png";
    }
}
