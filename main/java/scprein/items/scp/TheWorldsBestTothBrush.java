package alexsocol.scprein.items.scp;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;

public class TheWorldsBestTothBrush extends ItemPickaxe {

    //SCP-063
    public TheWorldsBestTothBrush() {
        super(SCPMain.TWBTB);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxDamage(0);
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":TheWorldsBestTothBrush");
        this.setUnlocalizedName("TheWorldsBestTothBrush");
    }

    private void isDamageable(boolean b) {
        this.isDamageable(false);
    }

    //Based on code from bedrockminer.jimdo.com/
    @Override
    public Set<String> getToolClasses(ItemStack stack) {
        return ImmutableSet.of("pickaxe", "spade");
    }
    private static Set effectiveAgainst = Sets.newHashSet(new Block[]{Blocks.grass, Blocks.dirt, Blocks.sand, Blocks.gravel, Blocks.snow_layer, Blocks.snow, Blocks.clay, Blocks.farmland, Blocks.soul_sand, Blocks.mycelium, Blocks.planks, Blocks.bookshelf, Blocks.log, Blocks.log2, Blocks.chest, Blocks.pumpkin, Blocks.lit_pumpkin, Blocks.leaves, Blocks.leaves2, Blocks.sponge, Blocks.glass, Blocks.glass_pane, Blocks.stained_glass, Blocks.stained_glass_pane, Blocks.piston, Blocks.piston_extension, Blocks.piston_head, Blocks.web, Blocks.wool, Blocks.ladder, Blocks.cactus, Blocks.glowstone, Blocks.monster_egg, Blocks.melon_block, Blocks.redstone_lamp, Blocks.lit_redstone_lamp, Blocks.beacon, Blocks.packed_ice, RegistrationsList.livingRoomBoneFloor, RegistrationsList.liveRoomFleshWall, RegistrationsList.electricalWool, RegistrationsList.ironWool, RegistrationsList.camera, RegistrationsList.searchlight});

    @Override
    public boolean func_150897_b(Block block) {
        return effectiveAgainst.contains(block) ? true : super.func_150897_b(block);
    }

    @Override
    public float func_150893_a(ItemStack stack, Block block) {
        if (block.getMaterial() == Material.wood || block.getMaterial() == Material.vine || block.getMaterial() == Material.plants) {
            return this.efficiencyOnProperMaterial;
        }
        return effectiveAgainst.contains(block) ? this.efficiencyOnProperMaterial : super.func_150893_a(stack, block);
    }
}
