package alexsocol.scprein.blocks.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;

public class BladesTreeLog extends Block implements SCPBlock {

    public BladesTreeLog() {
        super(Material.wood);
        this.setBlockName("BladesTreeLog");
        this.setBlockTextureName(ModInfo.MODID + ":BladesTreeLog");
        this.setCreativeTab(SCPMain.SCPBlocks);
        this.setHardness(2.0F);
        this.setHarvestLevel("Axe", 1);
        this.setResistance(150.0F);
    }

    @Override
    public void registerBlockIcons(IIconRegister reg) {
        for (int i = 0; i < 2; i++) {
            this.icons[i] = reg.registerIcon(this.textureName + (i == 0 ? "Side" : "Top"));
        }
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        switch (side) {
            case 1:
            case 0:
                return icons[1];
            default:
                return icons[0];
        }
    }

    private final IIcon[] icons = new IIcon[2];
}
