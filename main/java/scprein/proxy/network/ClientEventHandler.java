package alexsocol.scprein.proxy.network;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;

import org.lwjgl.opengl.GL11;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.utils.PotionRegistry;
import alexsocol.scprein.utils.RegistrationsList;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class ClientEventHandler {
	
	static Minecraft mc = Minecraft.getMinecraft();

	static final ResourceLocation furnaceGuiTextures = new ResourceLocation("textures/gui/container/furnace.png");
	int xSize = 176;
	int ySize = 166;

	@SubscribeEvent
	public void render(RenderGameOverlayEvent.Pre event) {
		ScaledResolution scaledresolution = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
        int k = scaledresolution.getScaledWidth();
        int l = scaledresolution.getScaledHeight();
		ItemStack itemstack = this.mc.thePlayer.inventory.armorItemInSlot(3);
		if (this.mc.gameSettings.thirdPersonView == 0 && itemstack != null && itemstack.getItem() == RegistrationsList.gasmask) {
		    switch(event.type) {
			    case HELMET:{
			        renderGasMask(k, l);
			        break;
			    }
			    default: break;
		    }
	    }
	}
	
	public void renderGasMask(int p_73836_1_, int p_73836_2_) {
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(false);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        this.mc.getTextureManager().bindTexture(new ResourceLocation(ModInfo.MODID + ":textures/entity/GasMask.png"));
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(0.0D, (double)p_73836_2_, -90.0D, 0.0D, 1.0D);
        tessellator.addVertexWithUV((double)p_73836_1_, (double)p_73836_2_, -90.0D, 1.0D, 1.0D);
        tessellator.addVertexWithUV((double)p_73836_1_, 0.0D, -90.0D, 1.0D, 0.0D);
        tessellator.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
        tessellator.draw();
        GL11.glDepthMask(true);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    }
	
	@SubscribeEvent
	public void renderPlayer(RenderPlayerEvent event) {
		if (mc.thePlayer.isPotionActive(PotionRegistry.potionCrystalID)) {
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.mc.renderEngine.getTexture(new ResourceLocation(ModInfo.MODID + ":/textures/blocks/InfectionCrystal.png")).getGlTextureId());
		}
	}
}