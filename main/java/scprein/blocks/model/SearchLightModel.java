package alexsocol.scprein.blocks.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class SearchLightModel extends ModelBase {

    ModelRenderer Base;
    ModelRenderer Front;
    ModelRenderer Back;
    ModelRenderer Right;
    ModelRenderer Left;
    ModelRenderer Lence;
    ModelRenderer Light;

    public SearchLightModel() {
        textureWidth = 64;
        textureHeight = 64;
        Base = new ModelRenderer(this, 0, 0);
        Base.addBox(0F, 0F, 0F, 16, 8, 16);
        Base.setRotationPoint(-8F, 16F, -8F);
        Base.setTextureSize(64, 64);
        Base.mirror = true;
        setRotation(Base, 0F, 0F, 0F);
        Front = new ModelRenderer(this, 0, 24);
        Front.addBox(0F, 0F, 0F, 16, 8, 1);
        Front.setRotationPoint(-8F, 8F, -8F);
        Front.setTextureSize(64, 64);
        Front.mirror = true;
        setRotation(Front, 0F, 0F, 0F);
        Back = new ModelRenderer(this, 0, 33);
        Back.addBox(0F, 0F, 0F, 16, 8, 1);
        Back.setRotationPoint(-8F, 8F, 7F);
        Back.setTextureSize(64, 64);
        Back.mirror = true;
        setRotation(Back, 0F, 0F, 0F);
        Right = new ModelRenderer(this, 0, 42);
        Right.addBox(0F, 0F, -1F, 16, 8, 1);
        Right.setRotationPoint(-8F, 8F, -8F);
        Right.setTextureSize(64, 64);
        Right.mirror = true;
        setRotation(Right, 0F, -1.570796F, 0F);
        Left = new ModelRenderer(this, 0, 51);
        Left.addBox(0F, 0F, -1F, 16, 8, 1);
        Left.setRotationPoint(7F, 8F, -8F);
        Left.setTextureSize(64, 64);
        Left.mirror = true;
        setRotation(Left, 0F, -1.570796F, 0F);
        Lence = new ModelRenderer(this, 34, 24);
        Lence.addBox(0F, 0F, 0F, 14, 14, 1);
        Lence.setRotationPoint(-7F, 9F, -7F);
        Lence.setTextureSize(64, 64);
        Lence.mirror = true;
        setRotation(Lence, 1.570796F, 0F, 0F);
        Light = new ModelRenderer(this, 34, 39);
        Light.addBox(0F, 0F, 0F, 8, 8, 4);
        Light.setRotationPoint(-4F, 16F, -4F);
        Light.setTextureSize(64, 64);
        Light.mirror = true;
        setRotation(Light, 1.570796F, 0F, 0F);
    }

    public void render(float f5) {
        Base.render(f5);
        Front.render(f5);
        Back.render(f5);
        Right.render(f5);
        Left.render(f5);
        Lence.render(f5);
        Light.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}
