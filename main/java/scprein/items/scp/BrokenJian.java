package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class BrokenJian extends ItemSword {

    public BrokenJian() {
        super(SCPMain.JIAN);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxDamage(0);
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":BrokenJian");
        this.setUnlocalizedName("BrokenJian");
        setFull3D();
    }

    public boolean func_150897_b(Block p_150897_1_) {
        return p_150897_1_ == Blocks.web;
    }

    public float func_150893_a(ItemStack p_150893_1_, Block p_150893_2_) {
        if (p_150893_2_ == Blocks.web) {
            return 15.0F;
        } else {
            Material material = p_150893_2_.getMaterial();
            return material != Material.plants && material != Material.vine && material != Material.coral && material != Material.leaves && material != Material.gourd ? 1.0F : 1.5F;
        }
    }

    private void isDamageable(boolean b) {
        this.isDamageable(false);
    }
}
