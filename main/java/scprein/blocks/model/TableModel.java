package alexsocol.scprein.blocks.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class TableModel extends ModelBase {

    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;

    public TableModel() {
        textureWidth = 64;
        textureHeight = 18;
        Shape1 = new ModelRenderer(this, 0, 0);
        Shape1.addBox(0F, 0F, 0F, 2, 12, 2);
        Shape1.setRotationPoint(6F, 12F, -8F);
        Shape1.setTextureSize(64, 18);
        Shape1.mirror = true;
        setRotation(Shape1, 0F, 0F, 0F);
        Shape2 = new ModelRenderer(this, 8, 0);
        Shape2.addBox(0F, 0F, 0F, 2, 12, 2);
        Shape2.setRotationPoint(-8F, 12F, -8F);
        Shape2.setTextureSize(64, 18);
        Shape2.mirror = true;
        setRotation(Shape2, 0F, 0F, 0F);
        Shape3 = new ModelRenderer(this, 48, 0);
        Shape3.addBox(0F, 0F, 0F, 2, 12, 2);
        Shape3.setRotationPoint(-8F, 12F, 6F);
        Shape3.setTextureSize(64, 18);
        Shape3.mirror = true;
        setRotation(Shape3, 0F, 0F, 0F);
        Shape4 = new ModelRenderer(this, 56, 0);
        Shape4.addBox(0F, 0F, 0F, 2, 12, 2);
        Shape4.setRotationPoint(6F, 12F, 6F);
        Shape4.setTextureSize(64, 18);
        Shape4.mirror = true;
        setRotation(Shape4, 0F, 0F, 0F);
        Shape5 = new ModelRenderer(this, 0, 0);
        Shape5.addBox(0F, 0F, 0F, 16, 2, 16);
        Shape5.setRotationPoint(-8F, 10F, -8F);
        Shape5.setTextureSize(64, 18);
        Shape5.mirror = true;
        setRotation(Shape5, 0F, 0F, 0F);
    }

    public void render(float f5) {
        Shape1.render(f5);
        Shape2.render(f5);
        Shape3.render(f5);
        Shape4.render(f5);
        Shape5.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}
