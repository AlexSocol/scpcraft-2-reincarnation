package alexsocol.scprein.world.generation;

import java.util.Random;

import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class CubeSmall extends WorldGenerator {
	protected Block[] GetValidSpawnBlocks() {
		return new Block[] { RegistrationsList.moskowDirt, };
	}

	public boolean LocationIsValidSpawn(World world, int x, int y, int z) {

		Block checkBlock = world.getBlock(x, y - 1, z);
		Block blockAbove = world.getBlock(x, y , z);
		Block blockBelow = world.getBlock(x, y - 2, z);

		for (Block i : GetValidSpawnBlocks()) {
			if (blockAbove != Blocks.air) {
				return false;
			}
			if (checkBlock == i) {
				return true;
			} else if (checkBlock == Blocks.snow_layer && blockBelow == i) {
				return true;
			} else if (checkBlock.getMaterial() == Material.plants && blockBelow == i) {
				return true;
			}
		}
		return false;
	}

	public boolean generate(World world, Random rand, int x, int y, int z) {
		if (!LocationIsValidSpawn(world, x + 8, y, z + 8)) {
			return false;
		}

		for (int i = 0; i < 16; i++) {
			for (int j = 0; j < 16; j++) {
				world.setBlock(x + i, y + 1, z + j, RegistrationsList.moskowRock);
				world.setBlock(x + i, y + 17, z + j, RegistrationsList.moskowRock);

				world.setBlock(x, y + i, z + j, RegistrationsList.moskowRock);
				world.setBlock(x + 16, y + i, z + j, RegistrationsList.moskowRock);
				
				world.setBlock(x + j, y + i, z, RegistrationsList.moskowRock);
				world.setBlock(x + j, y + i, z + 16, RegistrationsList.moskowRock);
			}
		}
		return true;
	}
}