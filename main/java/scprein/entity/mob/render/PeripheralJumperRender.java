package alexsocol.scprein.entity.mob.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.entity.mob.PeripheralJumper;

public class PeripheralJumperRender extends RenderLiving {


    public PeripheralJumperRender(ModelBase par1ModelBase, float par2) {
        super(par1ModelBase, par2);
    }

    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
    	boolean flag = false;
    	if (entity instanceof PeripheralJumper) {
    		PeripheralJumper mob = (PeripheralJumper) entity;
    		flag = mob.isVisible;
    	}
        return flag ? new ResourceLocation(ModInfo.MODID, "textures/entity/PeripheralJumper.png") : new ResourceLocation(ModInfo.MODID, "textures/entity/empty.png");
    }
}
