package alexsocol.scprein.utils;

import java.lang.reflect.Field;

import net.minecraft.potion.Potion;
import alexsocol.scprein.potions.PotionBlinkDelay;
import alexsocol.scprein.potions.PotionCrystal;
import alexsocol.scprein.potions.PotionFlashBang;
import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.relauncher.ReflectionHelper;

public class PotionRegistry {
	public static int potionCrystalID = 2;
	public static int potionFlashBangID = 15;
	public static int potionBlinkDelayID = 15;
	
	/**
	 * This code was taken from Azanor's Utils from Thaumcraft 
	 * It extends potionlist to add new potions
	 * */
	public static void enlargePotionList() {
		int customPotions = 8;
	    int potionOffset = Potion.potionTypes.length;
	    int start = 0;
	    if (potionOffset < 128 - customPotions) {
	    	Potion[] potionTypes = new Potion[potionOffset + customPotions];
	    	System.arraycopy(Potion.potionTypes, 0, potionTypes, 0, potionOffset);
	    	PotionRegistry.setPrivateFinalValue(Potion.class, null, potionTypes, new String[] { "potionTypes", "field_76425_a", "a" });
	    	start = potionOffset++ - 1;
	    } else {
	        start = -1;
	    }
	    start = getNextPotionId(start);
	    if (start >= 0) {
	      potionFlashBangID = start;
	      PotionFlashBang.instance = new PotionFlashBang(potionFlashBangID, true, 0);
	      PotionFlashBang.init();
	    }
	    start = getNextPotionId(start);
	    if (start >= 0) {
	      potionBlinkDelayID = start;
	      PotionBlinkDelay.instance = new PotionBlinkDelay(potionBlinkDelayID, false, 0);
	      PotionBlinkDelay.init();
	    }
	    start = getNextPotionId(start);
	    if (start >= 0) {
	      potionCrystalID = start;
	      PotionCrystal.instance = new PotionCrystal(potionCrystalID, true, 0);
	      PotionCrystal.init();
	    }
	}
	
	/** This code was taken from Azanor's Utils from Thaumcraft */
	static int getNextPotionId(int start) {
		if ((Potion.potionTypes != null) && (start > 0) && (start < Potion.potionTypes.length) && (Potion.potionTypes[start] == null)) {
			return start;
		}
		start++;
		if (start < 128) {
			start = getNextPotionId(start);
		} else {
			start = -1;
		}
		return start;
	}
	
	/** This code was taken from Azanor's Utils from Thaumcraft */
	public static <T, E> void setPrivateFinalValue( Class<? super T> classToAccess, T instance, E value, String... fieldNames) {
		Field field = ReflectionHelper.findField( classToAccess, ObfuscationReflectionHelper.remapFieldNames(classToAccess.getName(), fieldNames));
		try {
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & 0xFFFFFFEF);

			field.set(instance, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
