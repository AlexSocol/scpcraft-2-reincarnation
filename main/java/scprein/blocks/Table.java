package alexsocol.scprein.blocks;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.TableTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class Table extends Block implements ITileEntityProvider {

    public Table() {
        super(Material.iron);
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.875F, 1.0F);
        this.setBlockName("Table");
        this.setBlockTextureName(ModInfo.MODID + ":TableIcon");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(2.0F);
        this.setHarvestLevel("pickaxe", 1);
        this.setResistance(50.0F);
        this.setStepSound(soundTypeMetal);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new TableTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
}
