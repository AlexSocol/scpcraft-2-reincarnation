package alexsocol.scprein.blocks;

import static net.minecraftforge.common.util.ForgeDirection.EAST;
import static net.minecraftforge.common.util.ForgeDirection.NORTH;
import static net.minecraftforge.common.util.ForgeDirection.SOUTH;
import static net.minecraftforge.common.util.ForgeDirection.WEST;

import java.util.Random;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.KCSlotTileEntity;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class KCSlotLevel3 extends Block implements ITileEntityProvider {

	public KCSlotLevel3() {
		super(Material.circuits);
		this.setCreativeTab(SCPMain.NotSCPBlocks);
		this.setBlockName("SlotLevel3");
	}
	
	// Tech stuff

	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return new KCSlotTileEntity();
	}

	@Override
	public boolean canProvidePower() {
		return true;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	} 

	@Override
	public int getRenderType() {
		return -1;
	}

	@Override
	public int quantityDropped(Random random) {
		return 1;
	}

	@Override
	public int damageDropped(int i) {
		return 0;
	}
	
	// Block placing stuff
	
	@Override
	public boolean canPlaceBlockOnSide(World world, int x, int y, int z, int meta) {
		ForgeDirection dir = ForgeDirection.getOrientation(meta);
		return (dir == NORTH && world.isSideSolid(x, y, z + 1, NORTH))
				|| (dir == SOUTH && world.isSideSolid(x, y, z - 1, SOUTH))
				|| (dir == WEST && world.isSideSolid(x + 1, y, z, WEST))
				|| (dir == EAST && world.isSideSolid(x - 1, y, z, EAST));
	}

	@Override
	public boolean canPlaceBlockAt(World world, int x, int y, int z) {
		return (world.isSideSolid(x - 1, y, z, EAST))
				|| (world.isSideSolid(x + 1, y, z, WEST))
				|| (world.isSideSolid(x, y, z - 1, SOUTH))
				|| (world.isSideSolid(x, y, z + 1, NORTH));
	}

	@Override
	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta) {
		int j1 = world.getBlockMetadata(x, y, z);
		int k1 = j1 & 8;
		j1 &= 7;

		ForgeDirection dir = ForgeDirection.getOrientation(side);

		if (dir == NORTH && world.isSideSolid(x, y, z + 1, NORTH)) {
			j1 = 4;
		} else if (dir == SOUTH && world.isSideSolid(x, y, z - 1, SOUTH)) {
			j1 = 3;
		} else if (dir == WEST && world.isSideSolid(x + 1, y, z, WEST)) {
			j1 = 2;
		} else if (dir == EAST && world.isSideSolid(x - 1, y, z, EAST)) {
			j1 = 1;
		} else {
			j1 = this.getSolidSide(world, x, y, z);
		}

		return j1 + k1;
	}

	public int getSolidSide(World world, int x, int y, int z) {
		if (world.isSideSolid(x - 1, y, z, EAST))
			return 1;
		if (world.isSideSolid(x + 1, y, z, WEST))
			return 2;
		if (world.isSideSolid(x, y, z - 1, SOUTH))
			return 3;
		if (world.isSideSolid(x, y, z + 1, NORTH))
			return 4;
		return 1;
	}

	public boolean shouldDropBlock(World world, int x, int y, int z) {
		if (!this.canPlaceBlockAt(world, x, y, z)) {
			this.dropBlockAsItem(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
			world.setBlockToAir(x, y, z);
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		if ((meta & 8) > 0) {
			int i1 = meta & 7;
			this.notifyAboutChange(world, x, y, z, i1);
		}
		super.breakBlock(world, x, y, z, block, meta);
	}

	public void notifyAboutChange(World world, int x, int y, int z, int meta) {
		world.notifyBlocksOfNeighborChange(x, y, z, this);
		world.notifyBlocksOfNeighborChange(x - 1, y, z, this);
		world.notifyBlocksOfNeighborChange(x + 1, y, z, this);
		world.notifyBlocksOfNeighborChange(x, y, z - 1, this);
		world.notifyBlocksOfNeighborChange(x, y, z + 1, this);
		world.notifyBlocksOfNeighborChange(x, y - 1, z, this);
		world.notifyBlocksOfNeighborChange(x, y + 1, z, this);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor) {
		if (this.shouldDropBlock(world, x, y, z)) {
			int l = world.getBlockMetadata(x, y, z) & 7;
			boolean flag = false;

			if (!world.isSideSolid(x - 1, y, z, EAST) && l == 1) {
				flag = true;
			}

			if (!world.isSideSolid(x + 1, y, z, WEST) && l == 2) {
				flag = true;
			}

			if (!world.isSideSolid(x, y, z - 1, SOUTH) && l == 3) {
				flag = true;
			}

			if (!world.isSideSolid(x, y, z + 1, NORTH) && l == 4) {
				flag = true;
			}

			if (flag) {
				this.dropBlockAsItem(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
				world.setBlockToAir(x, y, z);
			}
		}
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k) {
		int l = world.getBlockMetadata(i, j, k);         
		int m = l & 7;     
		if (m == 0 || m == 4) {
			setBlockBounds(0.25F, 0.16F, 0.875F, 0.75F, 0.85F, 1.0F);
		}
		if (m == 1 || m == 5) {
			setBlockBounds(0.0F, 0.16F, 0.25F, 0.125F, 0.85F, 0.75F);
		}
		if (m == 2 || m == 6) {
			setBlockBounds(0.875F, 0.16F, 0.25F, 1.0F, 0.85F, 0.75F);
		}
		if (m == 3 || m == 7) {
			setBlockBounds(0.25F, 0.16F, 0.0F, 0.75F, 0.85F, 0.125F);
		}
	}    

	// Activation stuff
	
	@Override
	public void onBlockClicked(World world, int x, int y, int z, EntityPlayer player) {
		this.onBlockActivated(world, x, y, z, player, 0, 0F, 0F, 0F);
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
		ItemStack itemstack = player.inventory.getCurrentItem();
		int meta = world.getBlockMetadata(x, y, z);
		if (world.isRemote) {
			return true;
		} else {
			if (itemstack != null && (itemstack.getItem() == RegistrationsList.keyCard3 || itemstack.getItem() == RegistrationsList.keyCard4 || itemstack.getItem() == RegistrationsList.keyCard5)) {
				world.setBlockMetadataWithNotify(x, y, z, meta + 4, 2);
				world.playSoundEffect((double) x + 0.5D, (double) y + 0.5D, (double) z + 0.5D, "random.click", 0.3F, 0.6F);
				this.notifyAboutChange(world, x, y, z, world.getBlockMetadata(x, y, z));
				((KCSlotTileEntity)world.getTileEntity(x, y, z)).timer = 100;
			} else {
				if (itemstack != null && (itemstack.getItem() == RegistrationsList.wrenchOmni)) {
					world.setBlock(x, y, z, RegistrationsList.lock4);
					itemstack.damageItem(1, player);
					if (world.isRemote) {
						player.addChatComponentMessage(new ChatComponentText("Keycard Level: 4"));
					}
					if (!world.isRemote) {
						player.addChatComponentMessage(new ChatComponentText("Keycard Level: 4"));
					}
				} else {
					if (world.isRemote) {
						player.addChatComponentMessage(new ChatComponentText("You need a Level 3 or higher Keycard to activate."));
					}
					if (!world.isRemote) {
						player.addChatComponentMessage(new ChatComponentText("You need a Level 3 or higher Keycard to activate."));
					}
				}
			}
		}
		return true;
	}

	@Override
	public int isProvidingStrongPower(IBlockAccess world, int x, int y, int z, int direction) {
		int meta = world.getBlockMetadata(x, y, z);
		return meta >= 4 && meta <= 7 ? 15 : 0;
	}

	@Override
	public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int par5) {
		int meta = world.getBlockMetadata(x, y, z);
		return meta >= 4 && meta <= 7 ? 15 : 0;
	}
}