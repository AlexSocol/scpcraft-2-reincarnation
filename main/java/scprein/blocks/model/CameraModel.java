package alexsocol.scprein.blocks.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class CameraModel extends ModelBase {

    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Shape6;

    public CameraModel() {
        textureWidth = 20;
        textureHeight = 16;
        Shape1 = new ModelRenderer(this, 0, 0);
        Shape1.addBox(0F, 0F, 0F, 4, 1, 4);
        Shape1.setRotationPoint(-2F, 8F, 3F);
        Shape1.setTextureSize(20, 16);
        Shape1.mirror = true;
        setRotation(Shape1, 0F, 0F, 0F);
        Shape2 = new ModelRenderer(this, 0, 0);
        Shape2.addBox(0F, 0F, 0F, 1, 3, 1);
        Shape2.setRotationPoint(-0.5F, 9F, 4.5F);
        Shape2.setTextureSize(20, 16);
        Shape2.mirror = true;
        setRotation(Shape2, 0F, 0F, 0F);
        Shape3 = new ModelRenderer(this, 0, 0);
        Shape3.addBox(0F, 0F, -1F, 1, 2, 1);
        Shape3.setRotationPoint(-0.5F, 12F, 5.5F);
        Shape3.setTextureSize(20, 16);
        Shape3.mirror = true;
        setRotation(Shape3, -0.7853982F, 0F, 0F);
        Shape4 = new ModelRenderer(this, 0, 5);
        Shape4.addBox(-1.5F, 0F, -6F, 3, 3, 6);
        Shape4.setRotationPoint(0F, 11.5F, 4F);
        Shape4.setTextureSize(20, 16);
        Shape4.mirror = true;
        setRotation(Shape4, 0.1745329F, 0F, 0F);
        Shape5 = new ModelRenderer(this, 0, 5);
        Shape5.addBox(-0.5F, 1F, -6.5F, 1, 1, 1);
        Shape5.setRotationPoint(0F, 11.5F, 4F);
        Shape5.setTextureSize(20, 16);
        Shape5.mirror = true;
        setRotation(Shape5, 0.1745329F, 0F, 0F);
        Shape6 = new ModelRenderer(this, 0, 7);
        Shape6.addBox(-1F, 0.5F, -7.5F, 2, 2, 1);
        Shape6.setRotationPoint(0F, 11.5F, 4F);
        Shape6.setTextureSize(20, 16);
        Shape6.mirror = true;
        setRotation(Shape6, 0.1745329F, 0F, 0F);
    }

    public void render(float f5) {
        Shape1.render(f5);
        Shape2.render(f5);
        Shape3.render(f5);
        Shape4.render(f5);
        Shape5.render(f5);
        Shape6.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}
