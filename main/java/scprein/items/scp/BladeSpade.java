package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemSpade;

public class BladeSpade extends ItemSpade {

    public BladeSpade() {
        super(SCPMain.BLADE);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setTextureName(ModInfo.MODID + ":BladeSpade");
        this.setUnlocalizedName("BladeSpade");
    }
}
