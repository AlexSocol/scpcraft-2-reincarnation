package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class LovecraftianLocket extends Item {

    //SCP-427
    public LovecraftianLocket() {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":LovecraftianLocket");
        this.setUnlocalizedName("LovecraftianLocket");
    }

    @Override
    public boolean hasEffect(ItemStack par1ItemStack, int pass) {
        return true;
    }

    //Thanks To Asd73 For This Recharge Method
    @Override
    public void onUpdate(ItemStack itemstack, World world, Entity p_77663_3_, int p_77663_4_, boolean p_77663_5_) {
        if (itemstack.hasTagCompound() && !world.isRemote) {
            NBTTagCompound nbt = itemstack.getTagCompound();
            int counter = nbt.getInteger("counter");
            if (counter > 0) {
                nbt.setInteger("counter", --counter);
                itemstack.setTagCompound(nbt);
            }
        }
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player) {
        if (!world.isRemote) {
            int counter;
            if (itemstack.hasTagCompound()) {
                counter = itemstack.getTagCompound().getInteger("counter");
            } else {
                counter = 0;
            }
            if (counter == 0) {
                NBTTagCompound nbt = new NBTTagCompound();
                nbt.setInteger("counter", 500);
                itemstack.setTagCompound(nbt);
                player.addPotionEffect((new PotionEffect(Potion.heal.getId(), 20, 4)));
            } else {
            }
        }
        return itemstack;
    }
}
