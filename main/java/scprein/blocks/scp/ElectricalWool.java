package alexsocol.scprein.blocks.scp;

import net.minecraft.block.Block;
import static net.minecraft.block.Block.soundTypeCloth;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;

public class ElectricalWool extends Block implements SCPBlock {

    public ElectricalWool() {
        super(Material.carpet);
        this.setBlockName("ElectricalWool");
        this.setBlockTextureName(ModInfo.MODID + ":ElectricalWool");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(1F);
        this.setHarvestLevel("shears", 0);
        this.setResistance(5F);
        this.setStepSound(soundTypeCloth);
    }

    @Override
    public boolean canProvidePower() {
        return true;
    }

    @Override
    public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int meta) {
        return 15;
    }

    @Override
    public int isProvidingStrongPower(IBlockAccess world, int x, int y, int z, int meta) {
        return 15;
    }
}
