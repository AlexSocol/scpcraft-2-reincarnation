package alexsocol.scprein.blocks.render;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.blocks.model.TableModel;

public class TableRender extends TileEntitySpecialRenderer {

    public static final TableModel model = new TableModel();
    public static final ResourceLocation texture = new ResourceLocation(ModInfo.MODID + ":textures/blocks/Table.png");

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float scale) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y + 1.5, z + 0.5);
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        Minecraft.getMinecraft().renderEngine.bindTexture(texture);
        this.model.render(0.0625F);
        GL11.glPopMatrix();
    }
}
