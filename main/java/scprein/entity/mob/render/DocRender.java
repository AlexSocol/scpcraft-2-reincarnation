package alexsocol.scprein.entity.mob.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class DocRender extends RenderLiving {

	public static final ResourceLocation texture = new ResourceLocation("scprein", "textures/entity/Doc.png");

    public DocRender(ModelBase par1ModelBase, float par2) {
        super(par1ModelBase, par2);
    }

    @Override
    public ResourceLocation getEntityTexture(Entity par1Entity) {
        return texture;
    }
}