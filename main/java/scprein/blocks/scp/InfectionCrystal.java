package alexsocol.scprein.blocks.scp;

import java.util.Random;

import net.minecraft.block.BlockPackedIce;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.AISCPList;
import alexsocol.scprein.utils.PotionRegistry;
import alexsocol.scprein.utils.RegistrationsList;

public class InfectionCrystal extends BlockPackedIce implements SCPBlock {

    //SCP-409
    public InfectionCrystal() {
        this.setBlockName("InfectionCrystal");
        this.setBlockTextureName(ModInfo.MODID + ":InfectionCrystal");
        this.setCreativeTab(SCPMain.SCPBlocks);
        this.setHardness(5.0F);
        this.setHarvestLevel("Pickaxe", 2);
        this.setResistance(25.0F);
        this.setStepSound(soundTypeGlass);
        setTickRandomly(true);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public void onEntityWalking(World world, int x, int y, int z, Entity entity) {
    	if (entity instanceof EntityLivingBase) {
    		((EntityLivingBase)entity).addPotionEffect(new PotionEffect(PotionRegistry.potionCrystalID, 160, 0));
    	}
    }
    
    @Override
    public void updateTick(World world, int x, int y, int z, Random random) {
    	if (!world.isRemote) {
	        if (world.rand.nextInt(10) == 0) {
	        	if (AISCPList.canSCPBreakBlock(world.getBlock(x + 1, y, z))) world.setBlock(x + 1, y, z, RegistrationsList.infectionCrystal);
	        	if (AISCPList.canSCPBreakBlock(world.getBlock(x - 1, y, z))) world.setBlock(x - 1, y, z, RegistrationsList.infectionCrystal);
	        	if (AISCPList.canSCPBreakBlock(world.getBlock(x, y + 1, z))) world.setBlock(x, y + 1, z, RegistrationsList.infectionCrystal);
	        	if (AISCPList.canSCPBreakBlock(world.getBlock(x, y - 1, z))) world.setBlock(x, y - 1, z, RegistrationsList.infectionCrystal);
	        	if (AISCPList.canSCPBreakBlock(world.getBlock(x, y, z + 1))) world.setBlock(x, y, z + 1, RegistrationsList.infectionCrystal);
	        	if (AISCPList.canSCPBreakBlock(world.getBlock(x, y, z - 1))) world.setBlock(x, y, z - 1, RegistrationsList.infectionCrystal);
	        }
    	}
    }
}
