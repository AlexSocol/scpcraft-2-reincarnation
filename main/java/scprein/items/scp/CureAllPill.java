package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class CureAllPill extends ItemFood {

    //SCP-500
    public CureAllPill() {
        super(0, false);
        this.setAlwaysEdible();
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(4);
        this.setTextureName(ModInfo.MODID + ":CureAllPill");
        this.setUnlocalizedName("CureAllPill");
    }

    public int getMaxItemUseDuration(ItemStack itemstack) {
        return 8;
    }

    public EnumAction getItemUseAction(ItemStack itemstack) {
        return EnumAction.drink;
    }

    public ItemStack onEaten(ItemStack is, World world, EntityPlayer player) {
        super.onEaten(is, world, player);
        if (!world.isRemote) {
            player.addPotionEffect(new PotionEffect(Potion.blindness.id, 1, 99));
            player.addPotionEffect(new PotionEffect(Potion.confusion.id, 1, 99));
            player.addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 1, 99));
            player.addPotionEffect(new PotionEffect(Potion.heal.id, 5, 99));
            player.addPotionEffect(new PotionEffect(Potion.hunger.id, 1, 99));
            player.addPotionEffect(new PotionEffect(Potion.jump.id, 100, 1));
            player.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 1, 99));
            player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 100, 1));
            player.addPotionEffect(new PotionEffect(Potion.poison.id, 1, 99));
            player.addPotionEffect(new PotionEffect(Potion.regeneration.id, 200, 4));
            player.addPotionEffect(new PotionEffect(Potion.resistance.id, 150, 4));
            player.addPotionEffect(new PotionEffect(Potion.weakness.id, 1, 99));
            player.addPotionEffect(new PotionEffect(Potion.wither.id, 1, 99));
        }
        return is;
    }
}
