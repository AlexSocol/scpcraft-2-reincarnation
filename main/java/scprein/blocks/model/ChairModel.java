package alexsocol.scprein.blocks.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class ChairModel extends ModelBase {

    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Shape6;
    ModelRenderer Shape7;
    ModelRenderer Shape8;

    public ChairModel() {
        textureWidth = 64;
        textureHeight = 18;
        Shape1 = new ModelRenderer(this, 0, 0);
        Shape1.addBox(0F, 0F, 0F, 2, 8, 2);
        Shape1.setRotationPoint(-6F, 16F, -6F);
        Shape1.setTextureSize(64, 18);
        Shape1.mirror = true;
        setRotation(Shape1, 0F, 0F, 0F);
        Shape2 = new ModelRenderer(this, 0, 0);
        Shape2.addBox(0F, 0F, 0F, 2, 8, 2);
        Shape2.setRotationPoint(-6F, 16F, 4F);
        Shape2.setTextureSize(64, 18);
        Shape2.mirror = true;
        setRotation(Shape2, 0F, 0F, 0F);
        Shape3 = new ModelRenderer(this, 0, 0);
        Shape3.addBox(0F, 0F, 0F, 2, 8, 2);
        Shape3.setRotationPoint(4F, 16F, 4F);
        Shape3.setTextureSize(64, 18);
        Shape3.mirror = true;
        setRotation(Shape3, 0F, 0F, 0F);
        Shape4 = new ModelRenderer(this, 0, 0);
        Shape4.addBox(0F, 0F, 0F, 2, 8, 2);
        Shape4.setRotationPoint(4F, 16F, -6F);
        Shape4.setTextureSize(64, 18);
        Shape4.mirror = true;
        setRotation(Shape4, 0F, 0F, 0F);
        Shape5 = new ModelRenderer(this, 0, 0);
        Shape5.addBox(0F, 0F, 0F, 12, 2, 12);
        Shape5.setRotationPoint(-6F, 14F, -6F);
        Shape5.setTextureSize(64, 18);
        Shape5.mirror = true;
        setRotation(Shape5, 0F, 0F, 0F);
        Shape6 = new ModelRenderer(this, 0, 0);
        Shape6.addBox(0F, 0F, 0F, 2, 10, 1);
        Shape6.setRotationPoint(-6F, 4F, 5F);
        Shape6.setTextureSize(64, 18);
        Shape6.mirror = true;
        setRotation(Shape6, 0F, 0F, 0F);
        Shape7 = new ModelRenderer(this, 0, 0);
        Shape7.addBox(0F, 0F, 0F, 2, 10, 1);
        Shape7.setRotationPoint(4F, 4F, 5F);
        Shape7.setTextureSize(64, 18);
        Shape7.mirror = true;
        setRotation(Shape7, 0F, 0F, 0F);
        Shape8 = new ModelRenderer(this, 0, 0);
        Shape8.addBox(0F, 0F, 0F, 8, 4, 1);
        Shape8.setRotationPoint(-4F, 2F, 5F);
        Shape8.setTextureSize(64, 18);
        Shape8.mirror = true;
        setRotation(Shape8, 0F, 0F, 0F);
    }

    public void render(float f5) {
        Shape1.render(f5);
        Shape2.render(f5);
        Shape3.render(f5);
        Shape4.render(f5);
        Shape5.render(f5);
        Shape6.render(f5);
        Shape7.render(f5);
        Shape8.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}
