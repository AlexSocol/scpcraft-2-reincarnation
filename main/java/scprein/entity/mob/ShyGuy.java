package alexsocol.scprein.entity.mob;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.utils.AISCPList;

public class ShyGuy extends EntityMob {

	public boolean isAngry;
	public int angerTimer;
	public int cryTimer;
	
    public ShyGuy(World world) {
        super(world);
        this.setSize(0.8F, 2.8F);
        this.setCustomNameTag("SCP-096");
        this.setAlwaysRenderNameTag(true);
        this.isAngry = false;
		this.isImmuneToFire = true;
		this.getNavigator().setAvoidsWater(true);
		this.getNavigator().setBreakDoors(true);
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(1, new EntityAIBreakDoor(this));
		this.tasks.addTask(2, new EntityAIAttackOnCollide(this, EntityPlayer.class, 80.0D, false));
        this.tasks.addTask(2, new EntityAIAttackOnCollide(this, EntityLiving.class, 80.0D, true));
		this.tasks.addTask(3, new EntityAIMoveTowardsRestriction(this, 80.0D));
		this.tasks.addTask(4, new EntityAIMoveThroughVillage(this, 80.0D, false));
		this.tasks.addTask(5, new EntityAIWander(this, 80.0D));
		this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(6, new EntityAILookIdle(this));
		this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));
		this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 0, true));	
    }
    
    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(9001.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(50.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(200.0D);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public void onUpdate() {
        super.onUpdate();
    	if (!isAngry) {
        	this.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 10, 255));
    	}
    	
    	if (angerTimer > 0) --angerTimer;
    	if (angerTimer == 1) this.isAngry = false;
    	if (cryTimer > 0) --cryTimer;
    	if (cryTimer == 1) this.isAngry = true;
    	
    	int X = (int) this.posX;
		int Y = (int) this.posY;
		int Z = (int) this.posZ;

		if (this.posX < 0) {
			X = (int) this.posX - 1;
		}
		if (this.posZ < 0) {
			Z = (int) this.posZ - 1;
		}
    	
    	if (this.isAngry) {
    		for (int y = 1; y < 3; y++) {
    			for (int x = -1; x < 2; x++) {
    				if (AISCPList.canSCPBreakBlock(this.worldObj.getBlock(X + x, Y + y, Z))) {
    					this.worldObj.setBlock(X + x, Y + y, Z, Blocks.air);
    				}
    			}
    			for (int z = -1; z < 2; z++) {
    				if (AISCPList.canSCPBreakBlock(this.worldObj.getBlock(X, Y + y, Z + z))) {
    					this.worldObj.setBlock(X, Y + y, Z + z, Blocks.air);
    				}
    			}
    		}
    	}
    }
    
    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
    	if (!this.isAngry) this.cryTimer = 760;
    	if (source.getSourceOfDamage() != null) source.getSourceOfDamage().worldObj.playSoundAtEntity(source.getSourceOfDamage(), ModInfo.MODID + ":scp096seen", 1.0F, worldObj.rand.nextFloat() * 0.1F + 0.9F);
    	this.entityToAttack = source.getEntity();
		return false;
    }
    
    @Override
    public void attackEntity(Entity entity, float amount) {
    	if (this.isAngry) {
    		this.attackEntityAsMob(entity);
    		this.angerTimer = 200;
    	}
    }
    
    @Override
	public String getLivingSound() {
		return this.isAngry ? ModInfo.MODID + ":scp096scream" : this.cryTimer == 0 ? ModInfo.MODID + ":scp096cry" : null;
	}

    @Override
    public String getDeathSound() {
		return "mob.ghast.scream";
	}
}
