package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class TheBestXXXInTheWorld extends ItemFood {

    //SCP-420-J
    public TheBestXXXInTheWorld() {
        super(0, false);
        this.setAlwaysEdible();
        this.setCreativeTab(SCPMain.SCPItems);
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":TheBestXXXInTheWorld");
        this.setUnlocalizedName("TheBestXXXInTheWorld");
    }

    public int getMaxItemUseDuration(ItemStack itemstack) {
        return 100;
    }

    public EnumAction getItemUseAction(ItemStack itemstack) {
        return EnumAction.bow;
    }

    public ItemStack onEaten(ItemStack is, World world, EntityPlayer player) {
        super.onEaten(is, world, player);
        if (!world.isRemote) {
            player.addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 0));
            player.addPotionEffect(new PotionEffect(Potion.blindness.id, 60, 0));
            player.addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 200, 1));
            player.addPotionEffect(new PotionEffect(Potion.jump.id, 600, 1));
            player.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 600, 1));
            player.addPotionEffect(new PotionEffect(Potion.poison.id, 100, 1));
            player.addPotionEffect(new PotionEffect(Potion.resistance.id, 600, 2));
            player.addPotionEffect(new PotionEffect(Potion.weakness.id, 600, 1));
        }
        return is;
    }
}
