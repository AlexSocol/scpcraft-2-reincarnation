package alexsocol.scprein.utils;

import java.util.Random;

import alexsocol.scprein.blocks.scp.SCPBlock;
import alexsocol.scprein.potions.PotionBlinkDelay;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBush;
import net.minecraft.block.BlockCrops;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;

public class AISCPList {

	public static final int transparentBlocks[] =
		{
			8, 9, 10, 11, 18, 27, 28, 30, 31,
			32, 37, 38, 39, 40, 44, 50, 51, 52, 59,
			64, 65, 66, 67, 69, 70, 72, 75, 76,
			77, 78, 83, 85, 90, 92, 106, 71,
			107, 108, 109, 111, 113, 114, 114, 117
		};
	
	public static boolean canSCPBreakBlock(Block block) {
        if (block == Blocks.air || block == Blocks.bedrock || block == RegistrationsList.granite || block == Blocks.end_portal || block == Blocks.end_portal_frame || block == Blocks.portal || block instanceof SCPBlock) {
        	return false;
        }
        return false;
	}
	
	public static void corruptBlock(World world, int x, int y, int z) {
		Block block = world.getBlock(x, y, z);
		int meta = world.getBlockMetadata(x, y, z);
		if (block == RegistrationsList.reinforcedIronBlock || block == Blocks.iron_block || block == RegistrationsList.fancyIronBlock) {
			world.setBlock(x, y, z, RegistrationsList.corrodedIronBlock);
		}
		if (block == Blocks.iron_door) {
			world.setBlock(x, y, z, RegistrationsList.corrodedIronDoor, meta, 2);
		}
		if (block == Blocks.grass || block == Blocks.farmland || (block == Blocks.dirt && world.getBlockMetadata(x, y, z) == 2)) world.setBlock(x, y, z, Blocks.dirt);
		if (block == Blocks.stone || block == RegistrationsList.armoredStone) world.setBlock(x, y, z, Blocks.cobblestone);
		if (block == Blocks.stonebrick) world.setBlockMetadataWithNotify(x, y, z, 2, 2);
		if (block == Blocks.leaves || block == Blocks.leaves2 || block == Blocks.glass || block == Blocks.glass_pane || block == Blocks.stained_glass || block == Blocks.stained_glass_pane || block instanceof BlockBush || block instanceof BlockCrops) world.setBlock(x, y, z, Blocks.air);
	}
	
	public static boolean teleportRandomly(Entity entity) {
        double d = entity.posX + (entity.worldObj.rand.nextDouble() - 0.5D) * 64D;
        double d1 = entity.posY + (double)(entity.worldObj.rand.nextInt(64) - 32);
        double d2 = entity.posZ + (entity.worldObj.rand.nextDouble() - 0.5D) * 64D;
        Random var1 = ((EntityLivingBase) entity).getRNG();

        for (int var2 = 0; var2 < 10; ++var2) {
            int var3 = MathHelper.floor_double(entity.posX + (double)var1.nextInt(20) - 10.0D);
            int var4 = MathHelper.floor_double(entity.boundingBox.minY + (double)var1.nextInt(6) - 3.0D);
            int var5 = MathHelper.floor_double(entity.posZ + (double)var1.nextInt(20) - 10.0D);

            if (entity.worldObj.getSavedLightValue(EnumSkyBlock.Block, var3, var4, var5) < 6) {
                return teleportTo(entity, var3, var4, var5);
            }
        }
		return true;
    }

	public static boolean teleportToEntity(Entity entity, Entity entitythis) {
		Vec3 vec3 = Vec3.createVectorHelper(entitythis.posX - entity.posX, entitythis.boundingBox.minY + (double)(entitythis.height / 2.0F) - entity.posY + (double)entity.getEyeHeight(), entitythis.posZ - entity.posZ);
        vec3 = vec3.normalize();
        double d0 = 16.0D;
        double d1 = entitythis.posX + (entitythis.worldObj.rand.nextDouble() - 0.5D) * 8.0D - vec3.xCoord * d0;
        double d2 = entitythis.posY + (double)(entitythis.worldObj.rand.nextInt(16) - 8) - vec3.yCoord * d0;
        double d3 = entitythis.posZ + (entitythis.worldObj.rand.nextDouble() - 0.5D) * 8.0D - vec3.zCoord * d0;
        return teleportTo(entitythis, d1, d2, d3);
    }

	public static boolean teleportTo(Entity entity, double par1, double par3, double par5) {
        double d = entity.posX;
        double d1 = entity.posY;
        double d2 = entity.posZ;
        entity.posX = par1;
        entity.posY = par3;
        entity.posZ = par5;
        boolean flag = false;
        int i = MathHelper.floor_double(entity.posX);
        int j = MathHelper.floor_double(entity.posY);
        int k = MathHelper.floor_double(entity.posZ);

        if (entity.worldObj.blockExists(i, j, k)) {
            boolean flag1;

            for (flag1 = false; !flag1 && j > 0;) {
                Block block = entity.worldObj.getBlock(i, j - 1, k);

                if (block.getIdFromBlock(block) == 0 || !block.getMaterial().blocksMovement()) {
                	entity.posY--;
                    j--;
                } else {
                    flag1 = true;
                }
            }

            if (flag1) {
            	entity.setPosition(entity.posX, entity.posY, entity.posZ);

                if (entity.worldObj.getCollidingBoundingBoxes(entity, entity.boundingBox).size() == 0 && !entity.worldObj.isAnyLiquid(entity.boundingBox)) {
                    flag = true;
                }
            }
        }

        if (!flag) {
        	entity.setPosition(d, d1, d2);
            return false;
        }

        int l = 128;

        for (int j1 = 0; j1 < l; j1++) {
            double d3 = (double)j1 / ((double)l - 1.0D);
            float f = (entity.worldObj.rand.nextFloat() - 0.5F) * 0.2F;
            float f1 = (entity.worldObj.rand.nextFloat() - 0.5F) * 0.2F;
            float f2 = (entity.worldObj.rand.nextFloat() - 0.5F) * 0.2F;
            double d4 = d + (entity.posX - d) * d3 + (entity.worldObj.rand.nextDouble() - 0.5D) * (double)entity.width * 2D;
            double d5 = d1 + (entity.posY - d1) * d3 + entity.worldObj.rand.nextDouble() * (double)entity.height;
            double d6 = d2 + (entity.posZ - d2) * d3 + (entity.worldObj.rand.nextDouble() - 0.5D) * (double)entity.width * 2D;
        }
        return true;
    }
	
	public static boolean canSCPBeSeen(Entity entity, EntityPlayer player) {
		if (entity.worldObj.getFullBlockLightValue(MathHelper.floor_double(entity.posX), MathHelper.floor_double(entity.posY), MathHelper.floor_double(entity.posZ)) < 1) {
			return false;
		}
		if (player != null && player.isPotionActive(PotionRegistry.potionBlinkDelayID) && player.getActivePotionEffect(PotionBlinkDelay.instance).getDuration() <= 20) return false;
		if (player.canEntityBeSeen(entity) || LineOfSightCheck(entity, player)) {
			return isInFieldOfVision(entity, player, 100F, 100F);
		} else {
			return false;
		}
	}
	
	public static boolean LineOfSightCheck(Entity entitythis, EntityLivingBase entityliving) {
		return rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX, entitythis.posY + (double)entitythis.getEyeHeight(), entitythis.posZ), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX, entitythis.posY + (double)entitythis.height, entitythis.posZ), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX, entitythis.posY + (double)entitythis.height * 0.10000000000000001D, entitythis.posZ), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX + 0.69999999999999996D, entitythis.posY + (double)entitythis.getEyeHeight(), entitythis.posZ), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX - 0.69999999999999996D, entitythis.posY + (double)entitythis.getEyeHeight(), entitythis.posZ), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX, entitythis.posY + (double)entitythis.getEyeHeight(), entitythis.posZ + 0.69999999999999996D), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX, entitythis.posY + (double)entitythis.getEyeHeight(), entitythis.posZ - 0.69999999999999996D), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX, entitythis.posY + (double)entitythis.height * 1.2D, entitythis.posZ - 0.69999999999999996D), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null || rayTraceBlocks(entitythis, Vec3.createVectorHelper(entitythis.posX, entitythis.posY + (double)entitythis.height * 1.2D + 1.0D, entitythis.posZ), Vec3.createVectorHelper(entityliving.posX, entityliving.posY + (double)entityliving.getEyeHeight(), entityliving.posZ)) == null;
	}
	
	public static boolean isInFieldOfVision(Entity entitythis, EntityLivingBase entityliving, float f, float f1) {
		float f2 = entityliving.rotationYaw;
		float f3 = entityliving.rotationPitch;
		if (entityliving instanceof EntityLiving) {
			((EntityLiving)entityliving).faceEntity(entitythis, 360F, 360F);
		}
		float f4 = entityliving.rotationYaw;
		float f5 = entityliving.rotationPitch;
		entityliving.rotationYaw = f2;
		entityliving.rotationPitch = f3;
		f2 = f4;
		f3 = f5;
		float f6 = f;
		float f7 = f1;
		float f8 = entityliving.rotationYaw - f6;
		float f9 = entityliving.rotationYaw + f6;
		float f10 = entityliving.rotationPitch - f7;
		float f11 = entityliving.rotationPitch + f7;
		boolean flag = GetFlag(f8, f9, f2, 0.0F, 360F);
		boolean flag1 = GetFlag(f10, f11, f3, -180F, 180F);
		return flag && flag1 && (entityliving.canEntityBeSeen(entitythis) || LineOfSightCheck(entitythis, entityliving));
	}

	public static boolean GetFlag(float f, float f1, float f2, float f3, float f4) {
		if (f < f3) {
			if (f2 >= f + f4) {
				return true;
			}

			if (f2 <= f1) {
				return true;
			}
		}

		if (f1 >= f4) {
			if (f2 <= f1 - f4) {
				return true;
			}

			if (f2 >= f) {
				return true;
			}
		}

		if (f1 < f4 && f >= f3) {
			return f2 <= f1 && f2 > f;
		} else {
			return false;
		}
	}
	
	public static MovingObjectPosition rayTraceBlocks(Entity entitythis, Vec3 Vec3, Vec3 Vec31) {
		boolean flag = false;
		boolean flag1 = false;

		if (Double.isNaN(Vec3.xCoord) || Double.isNaN(Vec3.yCoord) || Double.isNaN(Vec3.zCoord)) {
			return null;
		}

		if (Double.isNaN(Vec31.xCoord) || Double.isNaN(Vec31.yCoord) || Double.isNaN(Vec31.zCoord)) {
			return null;
		}

		int i = MathHelper.floor_double(Vec31.xCoord);
		int j = MathHelper.floor_double(Vec31.yCoord);
		int k = MathHelper.floor_double(Vec31.zCoord);
		int l = MathHelper.floor_double(Vec3.xCoord);
		int i1 = MathHelper.floor_double(Vec3.yCoord);
		int j1 = MathHelper.floor_double(Vec3.zCoord);
		int l1 = entitythis.worldObj.getBlockMetadata(l, i1, j1);
		Block block = entitythis.worldObj.getBlock(l, i1, j1);

		if ((!flag1 || block == null || block.getCollisionBoundingBoxFromPool(entitythis.worldObj, l, i1, j1) != null) && Block.getIdFromBlock(block) > 0 && block.canCollideCheck(l1, flag)) {
			MovingObjectPosition movingobjectposition = block.collisionRayTrace(entitythis.worldObj, l, i1, j1, Vec3, Vec31);

			if (movingobjectposition != null) {
				return movingobjectposition;
			}
		}

		for (int i2 = 200; i2-- >= 0;) {
			if (Double.isNaN(Vec3.xCoord) || Double.isNaN(Vec3.yCoord) || Double.isNaN(Vec3.zCoord)) {
				return null;
			}

			if (l == i && i1 == j && j1 == k) {
				return null;
			}

			boolean flag2 = true;
			boolean flag3 = true;
			boolean flag4 = true;
			double d = 999D;
			double d1 = 999D;
			double d2 = 999D;

			if (i > l) {
				d = (double)l + 1.0D;
			} else if (i < l) {
				d = (double)l + 0.0D;
			} else {
				flag2 = false;
			}

			if (j > i1) {
				d1 = (double)i1 + 1.0D;
			} else if (j < i1) {
				d1 = (double)i1 + 0.0D;
			} else {
				flag3 = false;
			}

			if (k > j1) {
				d2 = (double)j1 + 1.0D;
			} else if (k < j1) {
				d2 = (double)j1 + 0.0D;
			} else {
				flag4 = false;
			}

			double d3 = 999D;
			double d4 = 999D;
			double d5 = 999D;
			double d6 = Vec31.xCoord - Vec3.xCoord;
			double d7 = Vec31.yCoord - Vec3.yCoord;
			double d8 = Vec31.zCoord - Vec3.zCoord;

			if (flag2) {
				d3 = (d - Vec3.xCoord) / d6;
			}

			if (flag3) {
				d4 = (d1 - Vec3.yCoord) / d7;
			}

			if (flag4) {
				d5 = (d2 - Vec3.zCoord) / d8;
			}

			byte byte0 = 0;

			if (d3 < d4 && d3 < d5) {
				if (i > l) {
					byte0 = 4;
				} else {
					byte0 = 5;
				}

				Vec3.xCoord = d;
				Vec3.yCoord += d7 * d3;
				Vec3.zCoord += d8 * d3;
			} else if (d4 < d5) {
				if (j > i1) {
					byte0 = 0;
				} else {
					byte0 = 1;
				}

				Vec3.xCoord += d6 * d4;
				Vec3.yCoord = d1;
				Vec3.zCoord += d8 * d4;
			} else {
				if (k > j1) {
					byte0 = 2;
				} else {
					byte0 = 3;
				}

				Vec3.xCoord += d6 * d5;
				Vec3.yCoord += d7 * d5;
				Vec3.zCoord = d2;
			}

			Vec3 Vec32 = net.minecraft.util.Vec3.createVectorHelper(Vec3.xCoord, Vec3.yCoord, Vec3.zCoord);
			l = (int)(Vec32.xCoord = MathHelper.floor_double(Vec3.xCoord));

			if (byte0 == 5) {
				l--;
				Vec32.xCoord++;
			}

			i1 = (int)(Vec32.yCoord = MathHelper.floor_double(Vec3.yCoord));

			if (byte0 == 1) {
				i1--;
				Vec32.yCoord++;
			}

			j1 = (int)(Vec32.zCoord = MathHelper.floor_double(Vec3.zCoord));

			if (byte0 == 3) {
				j1--;
				Vec32.zCoord++;
			}

			int k2 = entitythis.worldObj.getBlockMetadata(l, i1, j1);
			Block block1 = entitythis.worldObj.getBlock(l, i1, j1);

			if ((!flag1 || block1 == null || block1.getCollisionBoundingBoxFromPool(entitythis.worldObj, l, i1, j1) != null) && Block.getIdFromBlock(block1) > 0 && block1.canCollideCheck(k2, flag) && !isBlockTransparent(Block.getIdFromBlock(block1))) {
				MovingObjectPosition movingobjectposition1 = block1.collisionRayTrace(entitythis.worldObj, l, i1, j1, Vec3, Vec31);

				if (movingobjectposition1 != null) {
					return movingobjectposition1;
				}
			}
		}
		return null;
	}
	
	// TODO: change this metod!
	public static boolean isBlockTransparent(int i) {
		for (int j = 0; j < transparentBlocks.length; j++) {
			if (i == transparentBlocks[j]) {
				return true;
			}
		}
		return true;
	}

	public static boolean SCPDirectLook(Entity entitythis, EntityPlayer entityplayer) {
		if (entitythis.worldObj.getFullBlockLightValue(MathHelper.floor_double(entitythis.posX), MathHelper.floor_double(entitythis.posY), MathHelper.floor_double(entitythis.posZ)) < 1) {
			return false;
		}

		Vec3 Vec3 = entityplayer.getLook(1.0F).normalize();
		Vec3 Vec31 = net.minecraft.util.Vec3.createVectorHelper(entitythis.posX - entityplayer.posX, ((entitythis.boundingBox.minY + (double)entitythis.height) - entityplayer.posY) + (double)entityplayer.getEyeHeight(), entitythis.posZ - entityplayer.posZ);
		double d = Vec31.lengthVector();
		Vec31 = Vec31.normalize();
		double d1 = Vec3.dotProduct(Vec31);

		if (d1 > 1.0D - 0.025000000000000001D / d) {
			return entityplayer.canEntityBeSeen(entitythis);
		} else {
			return false;
		}
	}
}
