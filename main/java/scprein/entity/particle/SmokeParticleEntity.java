package alexsocol.scprein.entity.particle;

import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class SmokeParticleEntity extends EntityFX {

    public SmokeParticleEntity(World par1World, double par2, double par4, double par6, double par8, double par10, double par12) {
        this(par1World, par2, par4, par6, par8, par10, par12, 1.0F);
    }

    public SmokeParticleEntity(World par1World, double par2, double par4, double par6, double par8, double par10, double par12, float par14) {
        super(par1World, par2, par4, par6, 0.0D, 0.0D, 0.0D);
        this.motionX = par8;
        this.motionY = par10;
        this.motionZ = par12;
        this.particleRed = this.particleGreen = this.particleBlue = 0F;
        this.particleScale = par14;
        this.particleMaxAge = 64;
        this.particleMaxAge *= par14;
        this.noClip = false;
    }

    public void renderParticle(Tessellator par1Tessellator, float par2, float par3, float par4, float par5, float par6, float par7) {
        float f = (((float)particleAge + par2) / (float)particleMaxAge) * 32F;

        if (f < 0.0F) {
            f = 0.0F;
        }

        if (f > 1.0F) {
            f = 1.0F;
        }

        particleScale *= f;
        super.renderParticle(par1Tessellator, par2, par3, par4, par5, par6, par7);
    }
    
    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate() {
    	EntityPlayer player = this.worldObj.getClosestVulnerablePlayerToEntity(this, 16.0D);
    	if(player != null) {
    		ItemStack stack = player.inventory.armorItemInSlot(3);
    		if(stack != null) {
    			if(stack.getItem() != RegistrationsList.gasmask && this.getDistanceToEntity(player) < 1D) {
    				player.addPotionEffect(new PotionEffect(Potion.blindness.id, 7*20, 1));
    			}
    		} else if (this.getDistanceToEntity(player) < 1D) {
    			player.addPotionEffect(new PotionEffect(Potion.blindness.id, 7*20, 1));
    		}
    	}

        setParticleTextureIndex(7 - (this.particleAge * 8) / this.particleMaxAge);

        this.motionY *= 0.95D;
        this.motionZ *= 0.95D;
        
        if (this.particleAge++ >= this.particleMaxAge) {
        	this.setDead();
        }
        super.onUpdate();
    }
}