package alexsocol.scprein.blocks.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class BarbedWireModel extends ModelBase {

    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Shape6;
    ModelRenderer Shape7;
    ModelRenderer Shape8;
    ModelRenderer Shape9;
    ModelRenderer Shape0;

    public BarbedWireModel() {
        textureWidth = 256;
        textureHeight = 256;
        Shape1 = new ModelRenderer(this, 58, 0);
        Shape1.addBox(0F, 0F, 0F, 1, 18, 1);
        Shape1.setRotationPoint(-3F, 10.33333F, 3.5F);
        Shape1.setTextureSize(256, 256);
        Shape1.mirror = true;
        setRotation(Shape1, 0F, 0F, -0.5576792F);
        Shape2 = new ModelRenderer(this, 51, 0);
        Shape2.addBox(0F, 0F, 0F, 1, 18, 1);
        Shape2.setRotationPoint(2F, 10F, -4.5F);
        Shape2.setTextureSize(256, 256);
        Shape2.mirror = true;
        setRotation(Shape2, 0F, 0F, 0.4621508F);
        Shape3 = new ModelRenderer(this, 75, 30);
        Shape3.addBox(0F, 0F, 0F, 6, 0, 16);
        Shape3.setRotationPoint(-3F, 23F, -8F);
        Shape3.setTextureSize(256, 256);
        Shape3.mirror = true;
        setRotation(Shape3, 0F, 0F, 0F);
        Shape4 = new ModelRenderer(this, 123, 43);
        Shape4.addBox(0F, 0F, 0F, 6, 0, 16);
        Shape4.setRotationPoint(-3F, 13F, -8F);
        Shape4.setTextureSize(256, 256);
        Shape4.mirror = true;
        setRotation(Shape4, 0F, 0F, 0F);
        Shape5 = new ModelRenderer(this, 34, 40);
        Shape5.addBox(0F, 0F, 0F, 0, 6, 16);
        Shape5.setRotationPoint(6F, 15F, -8F);
        Shape5.setTextureSize(256, 256);
        Shape5.mirror = true;
        setRotation(Shape5, 0F, 0F, 0F);
        Shape6 = new ModelRenderer(this, 0, 40);
        Shape6.addBox(0F, 0F, 0F, 0, 6, 16);
        Shape6.setRotationPoint(-6F, 15F, -8F);
        Shape6.setTextureSize(256, 256);
        Shape6.mirror = true;
        setRotation(Shape6, 0F, 0F, 0F);
        Shape7 = new ModelRenderer(this, 75, 51);
        Shape7.addBox(0F, 0F, 0F, 6, 0, 16);
        Shape7.setRotationPoint(2.2F, 12.5F, -8F);
        Shape7.setTextureSize(256, 256);
        Shape7.mirror = true;
        setRotation(Shape7, 0F, 0F, 0.7504916F);
        Shape8 = new ModelRenderer(this, 75, 0);
        Shape8.addBox(0F, 0F, 0F, 6, 0, 16);
        Shape8.setRotationPoint(-6.5F, 20F, -8F);
        Shape8.setTextureSize(256, 256);
        Shape8.mirror = true;
        setRotation(Shape8, 0F, 0F, 0.5934119F);
        Shape9 = new ModelRenderer(this, 125, 0);
        Shape9.addBox(0F, 0F, 0F, 6, 0, 16);
        Shape9.setRotationPoint(2F, 23.3F, -8F);
        Shape9.setTextureSize(256, 256);
        Shape9.mirror = true;
        setRotation(Shape9, 0F, 0F, -0.6806784F);
        Shape0 = new ModelRenderer(this, 125, 18);
        Shape0.addBox(0F, 0F, 0F, 6, 0, 16);
        Shape0.setRotationPoint(-6.8F, 16F, -8F);
        Shape0.setTextureSize(256, 256);
        Shape0.mirror = true;
        setRotation(Shape0, 0F, 0F, -0.5934119F);
    }

    public void render(float f5) {
        Shape1.render(f5);
        Shape2.render(f5);
        Shape3.render(f5);
        Shape4.render(f5);
        Shape5.render(f5);
        Shape6.render(f5);
        Shape7.render(f5);
        Shape8.render(f5);
        Shape9.render(f5);
        Shape0.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}
