package alexsocol.scprein.items;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class SlidingDoorItem extends Item {
	
	public SlidingDoorItem() {
        this.maxStackSize = 1;
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setTextureName(ModInfo.MODID + ":SlidingDoor");
        this.setUnlocalizedName("SlidingDoor");
    }

	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int meta, float hitX, float hitY, float hitZ) {
		if (meta != 1) {
			return false;
		} else {
			++y;
			Block block = RegistrationsList.slidingDoor;

			if (player.canPlayerEdit(x, y, z, meta, stack) && player.canPlayerEdit(x, y + 1, z, meta, stack)) {
				if (!block.canPlaceBlockAt(world, x, y, z)) {
					return false;
				} else {
					int i1 = MathHelper.floor_double((double) ((player.rotationYaw + 90.0F) * 4.0F / 360.0F) - 0.5D) & 3;
					placeDoorBlock(world, x, y, z, i1, block);
					--stack.stackSize;
					return true;
				}
			} else {
				return false;
			}
		}
	}

    public static void placeDoorBlock(World world, int x, int y, int z, int meta, Block block) {
    	world.setBlock(x, y, z, block, meta, 2);
    	world.setBlock(x, y + 1, z, block, meta + 8, 2);
        world.notifyBlocksOfNeighborChange(x, y, z, block);
        world.notifyBlocksOfNeighborChange(x, y + 1, z, block);
    }
}
