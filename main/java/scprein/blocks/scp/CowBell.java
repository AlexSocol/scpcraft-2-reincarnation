package alexsocol.scprein.blocks.scp;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.scp.tileentity.CowBellTileEntity;

public class CowBell extends Block implements ITileEntityProvider, SCPBlock {

    public CowBell() {
        super(Material.iron);
        this.setBlockName("CowBell");
        this.setBlockBounds(0.4F, 0.0F, 0.4F, 0.6F, 0.2F, 0.6F);
        this.setCreativeTab(SCPMain.SCPBlocks);
        this.setHardness(1.0F);
        this.setResistance(5.0F);
        setBlockTextureName(ModInfo.MODID + ":CowBellIcon");
    }

    public TileEntity createNewTileEntity(World world, int par2) {
        return new CowBellTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		if (!world.isRemote) {
			world.playSoundEffect(x, y, z, ModInfo.MODID + ":scp513ring", 1.0F, world.rand.nextFloat() * 0.1F + 0.9F);
		}
		return super.onBlockActivated(world, x, y, z, player, side, hitX, hitY, hitZ);		
	}
}
