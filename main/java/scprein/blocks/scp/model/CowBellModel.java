package alexsocol.scprein.blocks.scp.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class CowBellModel extends ModelBase {

    ModelRenderer cb1;
    ModelRenderer cb2;
    ModelRenderer cb3;
    ModelRenderer cb4;
    ModelRenderer cb5;
    ModelRenderer cb6;
    ModelRenderer cb7;
    ModelRenderer cb8;
    ModelRenderer cb9;
    ModelRenderer cb10;
    ModelRenderer cb11;

    public CowBellModel() {
        textureWidth = 64;
        textureHeight = 32;
        cb1 = new ModelRenderer(this, 16, 2);
        cb1.addBox(-0.5F, 0F, 0F, 1, 1, 0);
        cb1.setRotationPoint(0F, 20.5F, 0F);
        cb1.setTextureSize(64, 32);
        cb1.mirror = true;
        setRotation(cb1, 0F, 0F, 0F);
        cb2 = new ModelRenderer(this, 0, 0);
        cb2.addBox(0F, 0F, -1F, 3, 2, 1);
        cb2.setRotationPoint(-1.5F, 22.5F, 1.5F);
        cb2.setTextureSize(64, 32);
        cb2.mirror = true;
        setRotation(cb2, 0.1047198F, 0F, 0F);
        cb3 = new ModelRenderer(this, 0, 11);
        cb3.addBox(0F, 0F, 0F, 2, 1, 2);
        cb3.setRotationPoint(-1F, 23.6F, -1F);
        cb3.setTextureSize(64, 32);
        cb3.mirror = true;
        setRotation(cb3, 0F, 0F, 0F);
        cb4 = new ModelRenderer(this, 3, 2);
        cb4.addBox(0F, 0F, -2F, 1, 2, 2);
        cb4.setRotationPoint(-1F, 21F, 1F);
        cb4.setTextureSize(64, 32);
        cb4.mirror = true;
        setRotation(cb4, 0F, 0F, 0.2443461F);
        cb5 = new ModelRenderer(this, 0, 0);
        cb5.addBox(0F, 0F, 0F, 2, 1, 2);
        cb5.setRotationPoint(-1F, 21F, -1F);
        cb5.setTextureSize(64, 32);
        cb5.mirror = true;
        setRotation(cb5, 0F, 0F, 0F);
        cb6 = new ModelRenderer(this, 0, 0);
        cb6.addBox(0F, 0F, 0F, 2, 2, 1);
        cb6.setRotationPoint(-1F, 21F, -1F);
        cb6.setTextureSize(64, 32);
        cb6.mirror = true;
        setRotation(cb6, -0.2443461F, 0F, 0F);
        cb7 = new ModelRenderer(this, 0, 0);
        cb7.addBox(0F, 0F, -1F, 2, 2, 1);
        cb7.setRotationPoint(-1F, 21F, 1F);
        cb7.setTextureSize(64, 32);
        cb7.mirror = true;
        setRotation(cb7, 0.2443461F, 0F, 0F);
        cb8 = new ModelRenderer(this, 0, 0);
        cb8.addBox(-1F, 0F, -2F, 1, 2, 2);
        cb8.setRotationPoint(1F, 21F, 1F);
        cb8.setTextureSize(64, 32);
        cb8.mirror = true;
        setRotation(cb8, 0F, 0F, -0.2443461F);
        cb9 = new ModelRenderer(this, 0, 0);
        cb9.addBox(-1F, 0F, 0F, 1, 2, 3);
        cb9.setRotationPoint(1.5F, 22.5F, -1.5F);
        cb9.setTextureSize(64, 32);
        cb9.mirror = true;
        setRotation(cb9, 0F, 0F, -0.1047198F);
        cb10 = new ModelRenderer(this, 3, 2);
        cb10.addBox(0F, 0F, 0F, 3, 2, 1);
        cb10.setRotationPoint(-1.5F, 22.5F, -1.5F);
        cb10.setTextureSize(64, 32);
        cb10.mirror = true;
        setRotation(cb10, -0.1047198F, 0F, 0F);
        cb11 = new ModelRenderer(this, 0, 2);
        cb11.addBox(0F, 0F, 0F, 1, 2, 3);
        cb11.setRotationPoint(-1.5F, 22.5F, -1.5F);
        cb11.setTextureSize(64, 32);
        cb11.mirror = true;
        setRotation(cb11, 0F, 0F, 0.1047198F);
    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        cb1.render(f5);
        cb2.render(f5);
        cb3.render(f5);
        cb4.render(f5);
        cb5.render(f5);
        cb6.render(f5);
        cb7.render(f5);
        cb8.render(f5);
        cb9.render(f5);
        cb10.render(f5);
        cb11.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}
