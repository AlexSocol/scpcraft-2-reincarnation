package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class JadeRing extends Item {

    //SCP-714
    public JadeRing() {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":JadeRing");
        this.setUnlocalizedName("JadeRing");
    }

    public void onUpdate(ItemStack itemstack, World world, Entity entity, int par4, boolean par5) {
        super.onUpdate(itemstack, world, entity, par4, par5);
        EntityLivingBase el = (EntityLivingBase) entity;
        el.addPotionEffect(new PotionEffect(2, 1, 2));
        el.addPotionEffect(new PotionEffect(4, 1, 2));
        el.addPotionEffect(new PotionEffect(17, 1, 0));
        el.addPotionEffect(new PotionEffect(18, 1, 2));
    }
}
