package alexsocol.scprein.blocks;

import java.util.List;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.CameraTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class Camera extends Block implements ITileEntityProvider {

    public Camera() {
        super(Material.glass);
        this.setBlockName("Camera");
        this.setBlockTextureName(ModInfo.MODID + ":CameraIcon");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(0.1F);
        this.setResistance(0.0F);
        this.setStepSound(soundTypeMetal);
    }

    public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB aabb, List list, Entity entity) {
    	int meta = world.getBlockMetadata(x, y, z);
    	switch (meta) {
	    	case 0: this.setBlockBounds(0.4F, 0.525F, 0.3F, 0.6F, 1.0F, 0.95F); break;
			case 1: this.setBlockBounds(0.05F, 0.525F, 0.4F, 0.7F, 1.0F, 0.6F); break;
			case 2: this.setBlockBounds(0.4F, 0.525F, 0.05F, 0.6F, 1.0F, 0.7F); break;
	    	case 3: this.setBlockBounds(0.3F, 0.525F, 0.4F, 0.95F, 1.0F, 0.6F); break;
    	}
    	super.addCollisionBoxesToList(world, x, y, z, aabb, list, entity); 
	}
    
    public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
    	int meta = world.getBlockMetadata(x, y, z);
    	switch (meta) {
    		case 0: this.setBlockBounds(0.4F, 0.525F, 0.3F, 0.6F, 1.0F, 0.95F); break;
    		case 1: this.setBlockBounds(0.05F, 0.525F, 0.4F, 0.7F, 1.0F, 0.6F); break;
    		case 2: this.setBlockBounds(0.4F, 0.525F, 0.05F, 0.6F, 1.0F, 0.7F); break;
	    	case 3: this.setBlockBounds(0.3F, 0.525F, 0.4F, 0.95F, 1.0F, 0.6F); break;
    	}
    }
    
    public TileEntity createNewTileEntity(World world, int par2) {
        return new CameraTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
        int l = MathHelper.floor_double((double)(entity.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
        world.setBlockMetadataWithNotify(x, y, z, l, 2);
    }
}
