package alexsocol.scprein.entity.mob;

import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.world.World;

public class DClass extends EntityVillager {

    public DClass(World world) {
        super(world);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public boolean allowLeashing() {
    	return true;
    }
}
