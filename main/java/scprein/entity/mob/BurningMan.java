package alexsocol.scprein.entity.mob;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BurningMan extends EntityMob {

    public BurningMan(World world) {
        super(world);
        this.setSize(0.8F, 1.8F);
        this.isImmuneToFire = true;
        this.setCustomNameTag("SCP-457");
        this.setAlwaysRenderNameTag(true);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(50.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.3D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(5.0D);
    }

    @Override
    public void onLivingUpdate() {
		for (int j = 0; j < 4; j++) {
			int l = MathHelper.floor_double(posX + (double)((float)((j % 2) * 2 - 1) * 0.25F));
			int i1 = MathHelper.floor_double(posY);
			int j1 = MathHelper.floor_double(posZ + (double)((float)(((j / 2) % 2) * 2 - 1) * 0.25F));

			if (worldObj.getBlock(l, i1, j1) == Blocks.air) {
				worldObj.setBlock(l, i1, j1, Blocks.fire);
			}
		}

		if (this.getHealth() <= 0) {
			float f = (rand.nextFloat() - 0.5F) * 8F;
			float f2 = (rand.nextFloat() - 0.5F) * 4F;
			float f4 = (rand.nextFloat() - 0.5F) * 8F;
			worldObj.spawnParticle("flame", posX + (double)f, posY + 2D + (double)f2, posZ + (double)f4, 0.0D, 0.0D, 0.0D);
			return;
		}

		if (rand.nextInt(24) == 0) {
			worldObj.playSoundEffect(posX + 0.5D, posY + 0.5D, posZ + 0.5D, "fire.fire", 1.0F + rand.nextFloat(), rand.nextFloat() * 0.7F + 0.3F);
		}

		for (int i = 0; i < 2; i++) {
			worldObj.spawnParticle("flame", posX + (rand.nextDouble() - 0.5D) * (double)width, posY + rand.nextDouble() * (double)height, posZ + (rand.nextDouble() - 0.5D) * (double)width, 0.0D, 0.0D, 0.0D);
			worldObj.spawnParticle("largesmoke", posX + (rand.nextDouble() - 0.5D) * (double)width, posY + rand.nextDouble() * (double)height, posZ + (rand.nextDouble() - 0.5D) * (double)width, 0.0D, 0.0D, 0.0D);
		}
		super.onLivingUpdate();
	}
    
    @Override
    public void attackEntity(Entity entity, float f) {
		super.attackEntity(entity, f);
		entity.setFire(8);
    }
    
    @Override
    public boolean isBurning() {
        return true;
    }

    @Override
    public void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, new Byte((byte)0));
	}
	
	@Override
	public void onDeath(DamageSource source) {
		super.onDeath(source);
		worldObj.setBlock(chunkCoordX, chunkCoordY, chunkCoordZ, Blocks.fire);
	}

    @Override
	public int getBrightnessForRender(float par1) {
		return 0xf000f0;
	}

    @Override
	public float getBrightness(float par1) {
		return 1.0F;
	}

    @Override
	public String getLivingSound() {
		return "fire.fire";
	}

    @Override
	public String getHurtSound() {
		return "mob.blaze.breathe";
	}

    @Override
    public String getDeathSound() {
		return "mob.blaze.death";
	}
}
