package alexsocol.scprein.blocks.render;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.IconFlipped;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.init.Blocks;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import alexsocol.scprein.blocks.SlidingDoor;
import alexsocol.scprein.proxy.ClientProxy;
import alexsocol.scprein.utils.RegistrationsList;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class SlidingDoorRender implements ISimpleBlockRenderingHandler {

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
		return;
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
		renderSlideDoor((SlidingDoor)block, x, y, z, renderer);
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int modelId) {
		return false;
	}

	@Override
	public int getRenderId() {
		return ClientProxy.slidingRenderID;
	}
	
	public boolean renderSlideDoor(SlidingDoor block, int i, int j, int k, RenderBlocks renderblocks) {
		int l = renderblocks.blockAccess.getBlockMetadata(i, j, k);	
		colorFix(block, renderblocks, i, j, k);
		
		IIcon top = SlidingDoor.upper;
		IIcon bot = SlidingDoor.lower;
		
		if(l == 0) {
			renderblocks.setRenderBounds(0F, 0F, 0.33F, 1F, 1F, 0.66F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 1) {
			renderblocks.setRenderBounds(0.33F, 0F, 0F, 0.66F, 1F, 1F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 2) {
			renderblocks.setRenderBounds(0F, 0F, 0.33F, 1F, 1F, 0.66F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 3) {
			renderblocks.setRenderBounds(0.33F, 0F, 0F, 0.66F, 1F, 1F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 4) {
			renderblocks.setRenderBounds(1.1F, 0F, 0.33F, 1.9F, 1F, 0.66F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 5) {
			renderblocks.setRenderBounds(0.33F, 0F, 1.1F, 0.66F, 1F, 1.9F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 6) {
			renderblocks.setRenderBounds(1.1F, 0F, 0.33F, 1.9F, 1F, 0.66F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 7) {
			renderblocks.setRenderBounds(0.33F, 0F, 1.1F, 0.66F, 1F, 1.9F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, bot);
		}
		
		if(l == 8) {
			renderblocks.setRenderBounds(0F, 0F, 0.33F, 1F, 1F, 0.66F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, top);
		}
		
		if(l == 9) {
			renderblocks.setRenderBounds(0.33F, 0F, 0F, 0.66F, 1F, 1F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
		}
		
		if(l == 10) {
			renderblocks.setRenderBounds(0F, 0F, 0.33F, 1F, 1F, 0.66F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, top);
		}
		
		if(l == 11) {
			renderblocks.setRenderBounds(0.33F, 0F, 0F, 0.66F, 1F, 1F);
			colorFix(block, renderblocks, i, j, k);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
		}
		
		if(l == 12) {
			renderblocks.setRenderBounds(-0.9F, 0F, 0.33F, -0.1F, 1F, 0.66F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, top);
		}
		
		if(l == 13) {
			renderblocks.setRenderBounds(0.33F, 0F, -0.9F, 0.66F, 1F, -0.1F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
		}
		
		if(l == 14) {
			renderblocks.setRenderBounds(-0.9F, 0F, 0.33F, -0.1F, 1F, 0.66F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, top);
		}
		
		if(l == 15) {
			renderblocks.setRenderBounds(0.33F, 0F, -0.9F, 0.66F, 1F, -0.1F);
			renderblocks.renderFaceXPos(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceXNeg(block, (double)i, (double)j, (double)k, top);
			renderblocks.renderFaceYPos(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceYNeg(block, (double)i, (double)j, (double)k, bot);
			renderblocks.renderFaceZPos(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
			renderblocks.renderFaceZNeg(block, (double)i, (double)j, (double)k, new IconFlipped(bot, false, true));
		}
		return true;
	}
	
	public void colorFix(Block par1Block, RenderBlocks renderblocks, int par2, int par3, int par4) {
		Tessellator var6 = Tessellator.instance;
		var6.setBrightness(par1Block.getMixedBrightnessForBlock(renderblocks.blockAccess, par2, par3, par4));
		float var7 = 1.0F;
		int var8 = par1Block.colorMultiplier(renderblocks.blockAccess, par2, par3, par4);
		float var9 = (float)(var8 >> 16 & 255) / 255.0F;
		float var10 = (float)(var8 >> 8 & 255) / 255.0F;
		float var11 = (float)(var8 & 255) / 255.0F;

		if (EntityRenderer.anaglyphEnable) {
			float var12 = (var9 * 30.0F + var10 * 59.0F + var11 * 11.0F) / 100.0F;
			float var13 = (var9 * 30.0F + var10 * 70.0F) / 100.0F;
			float var14 = (var9 * 30.0F + var11 * 70.0F) / 100.0F;
			var9 = var12;
			var10 = var13;
			var11 = var14;
		}

		var6.setColorOpaque_F(var7 * var9, var7 * var10, var7 * var11);		
	}
}
