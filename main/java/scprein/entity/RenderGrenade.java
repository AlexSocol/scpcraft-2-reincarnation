package alexsocol.scprein.entity;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import alexsocol.scprein.ModInfo;

public class RenderGrenade extends Render {

    public RenderGrenade() {
        shadowSize = 0.5F;
    }

    @Override
    public void doRender(Entity entity, double d, double d1, double d2, float f, float f1) {
        bindEntityTexture(entity);
        GL11.glPushMatrix();
        GL11.glTranslatef((float) d, (float) d1 + 1, (float) d2);
        GL11.glRotatef(f, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(180, 1, 0, 0);
        GL11.glScalef(0.5F, 0.5F, 0.5F);
        model.render(entity, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return new ResourceLocation(ModInfo.MODID, "textures/entity/grenade.png");
    }
    private static final ModelGrenade model = new ModelGrenade();
}
