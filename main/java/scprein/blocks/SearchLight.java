package alexsocol.scprein.blocks;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.SearchLightTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class SearchLight extends Block implements ITileEntityProvider {

    public SearchLight() {
        super(Material.circuits);
        this.setBlockName("SearchLight");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(0.5F);
        this.setResistance(5.0F);
        this.setStepSound(soundTypeStone);
        setBlockTextureName(ModInfo.MODID + ":SearchLightIcon");
    }

    public TileEntity createNewTileEntity(World world, int par2) {
        return new SearchLightTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
	
    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
        int l = BlockPistonBase.determineOrientation(world, x, y, z, entity);
        world.setBlockMetadataWithNotify(x, y, z, l, 2);
    }
}
