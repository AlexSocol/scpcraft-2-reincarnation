package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import scala.collection.immutable.Stack;

public class BladeBucket extends Item {

    private Block isFull;

    public BladeBucket(Block block) {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.isFull = block;
        this.setMaxStackSize(16);
        this.setTextureName(ModInfo.MODID + ":BladeBucket");
        this.setUnlocalizedName("BladeBucket");
    }

    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        boolean flag = this.isFull == Blocks.air;
        MovingObjectPosition mop = this.getMovingObjectPositionFromPlayer(world, player, flag);

        if (mop == null) {
            return stack;
        } else {
        	if (!player.capabilities.isCreativeMode) {
        		if (world.getBlock(mop.blockX, mop.blockY, mop.blockZ) == RegistrationsList.healingWatertBlock) {
            		--stack.stackSize;
            		world.spawnEntityInWorld(new EntityItem(world, player.posX, player.posY, player.posZ, new ItemStack(RegistrationsList.bladeBucketHealingWater)));
            		world.setBlock(mop.blockX, mop.blockY, mop.blockZ, Blocks.air);
            	} else if (world.getBlock(mop.blockX, mop.blockY, mop.blockZ) == RegistrationsList.liquidBloodBlock) {
            		--stack.stackSize;
            		world.spawnEntityInWorld(new EntityItem(world, player.posX, player.posY, player.posZ, new ItemStack(RegistrationsList.bladeBucketLiquidBlood)));
            		world.setBlock(mop.blockX, mop.blockY, mop.blockZ, Blocks.air);
            	}
        	} else {
        		world.setBlock(mop.blockX, mop.blockY, mop.blockZ, Blocks.air);
        	}
        	return stack;
        }
    }
}
