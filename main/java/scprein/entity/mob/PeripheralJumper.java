package alexsocol.scprein.entity.mob;

import alexsocol.scprein.utils.AISCPList;
import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class PeripheralJumper extends EntityMob {

	public int directionX = 0, directionZ = 0, seen;
	public boolean isVisible;
	public boolean isAngry = false;
	
    public PeripheralJumper(World world) {
        super(world);
        this.setSize(1.5F, 0.5F);
        this.stepHeight = 1.0F;
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(16.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(500.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(5.0D);
    }

	@Override
	public boolean isAIEnabled() {
		return false;
	}

	@Override
	public void entityInit() {
		super.entityInit();
		dataWatcher.addObject(16, Byte.valueOf((byte)0));
	}

	@Override
	public boolean canDespawn() {
		return false;
	}

	@Override
	public void onLivingUpdate() {
		super.onLivingUpdate();
		if(rand.nextInt(200) == 0) for (int m = 0; m <= 1000; m++) isVisible = true; else isVisible = false;
	}

	@Override
	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2) {       
		return false;
	}

	@Override
	public void onUpdate() {
		super.onUpdate();
		EntityPlayer entityplayer = this.worldObj.getClosestVulnerablePlayerToEntity(this, 16.0D);
		if (entityplayer != null && AISCPList.canSCPBeSeen(this, entityplayer) && !worldObj.isRemote ) {
			seen++;
			if(entityplayer.isDead) seen = 0;
			getNavigator().tryMoveToXYZ(entityplayer.posX + directionX, entityplayer.posY, entityplayer.posZ + directionZ, 1.25F);
		}
		if (seen >= 1500) this.isAngry = true;
		if (!worldObj.isRemote) {
			func_40148_a(isCollidedHorizontally);
		}
	}

	@Override
	public double getMountedYOffset() {
		return (double)height * 0.90D - 0.5D;
	}

	@Override
	public boolean canTriggerWalking() {
		return false;
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound) {
		super.writeEntityToNBT(nbttagcompound);
        nbttagcompound.setInteger("Seen", seen);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
		super.readEntityFromNBT(nbttagcompound);
		seen = nbttagcompound.getInteger("Seen");
	}

	public void func_40148_a(boolean flag) {
		byte byte0 = dataWatcher.getWatchableObjectByte(16);

		if (flag) {
			byte0 |= 1;
		} else {
			byte0 &= 0xfe;
		}
		dataWatcher.updateObject(16, Byte.valueOf(byte0));
	}

	@Override
    public String getLivingSound() {
        return "mob.spider.say";
    }
	
	@Override
	public String getDeathSound() {
		return "mob.spider.death";
	}

    @Override
	public EnumCreatureAttribute getCreatureAttribute() {
		return EnumCreatureAttribute.ARTHROPOD;
	}
}
