package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class BladeStick extends Item {

    public BladeStick() {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setTextureName(ModInfo.MODID + ":BladeStick");
        this.setUnlocalizedName("BladeStick");
    }
}
