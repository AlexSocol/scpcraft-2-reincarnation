package alexsocol.scprein.items;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.entity.EntitySmokeGrenade;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class SmokeGrenade extends Item {

    public SmokeGrenade() {
        String name = "smokeGrenade";
        setMaxStackSize(4);
        setTextureName(ModInfo.MODID + ":" + name);
        setUnlocalizedName(name);
        setCreativeTab(SCPMain.SCPItems);
        GameRegistry.registerItem(this, name, ModInfo.MODID);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
        p_77659_3_.setItemInUse(p_77659_1_, getMaxItemUseDuration(p_77659_1_));
        return p_77659_1_;
    }

    @Override
    public ItemStack onEaten(ItemStack p_77654_1_, World p_77654_2_, EntityPlayer p_77654_3_) {
        if (!p_77654_3_.capabilities.isCreativeMode) {
            --p_77654_1_.stackSize;
        }
        if (!p_77654_2_.isRemote) {
            p_77654_2_.spawnEntityInWorld(new EntitySmokeGrenade(p_77654_2_, p_77654_3_));
        }
        return p_77654_1_;
    }

    @Override
    public int getMaxItemUseDuration(ItemStack p_77626_1_) {
        return 24;
    }

    @Override
    public EnumAction getItemUseAction(ItemStack p_77661_1_) {
        return EnumAction.block;
    }
}
