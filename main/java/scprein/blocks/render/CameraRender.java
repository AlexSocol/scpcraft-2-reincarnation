package alexsocol.scprein.blocks.render;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import alexsocol.scprein.blocks.model.CameraModel;

public class CameraRender extends TileEntitySpecialRenderer {

    public static final CameraModel model = new CameraModel();
    public static final ResourceLocation textures = (new ResourceLocation("scprein:textures/blocks/Camera.png"));

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float scale) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y + 1.5, z + 0.5);
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        if (tile != null) {
            switch(tile.getBlockMetadata()) {
            case 0:{GL11.glRotatef(0F, 0.0F, 1.0F, 0.0F); break;}
            case 1:{GL11.glRotatef(90F, 0.0F, 1.0F, 0.0F); break;}
            case 2:{GL11.glRotatef(180F, 0.0F, 1.0F, 0.0F); break;}
            case 3:{GL11.glRotatef(270F, 0.0F, 1.0F, 0.0F); break;}
            }
        }
        Minecraft.getMinecraft().renderEngine.bindTexture(textures);
        this.model.render(0.0625F);
        GL11.glPopMatrix();
    }
}
