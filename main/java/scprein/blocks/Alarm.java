package alexsocol.scprein.blocks;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.AlarmTileEntity;
import alexsocol.scprein.blocks.tileentity.AlarmTileEntity.AlarmSounds;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class Alarm extends Block implements ITileEntityProvider {

	public IIcon frontI, sideI;
	
	public Alarm() {
		super(Material.iron);
		this.setBlockBounds(0F, 0F, 0F, 1F, 0.25F, 1F);
		this.setBlockName("Alarm");
		this.setBlockTextureName(ModInfo.MODID + ":");
		this.setCreativeTab(SCPMain.NotSCPBlocks);
		this.setHardness(2.0F);
		this.setLightOpacity(0);
		this.setResistance(60.0F);
		this.setStepSound(soundTypeMetal);
	}

	@Override
	public void registerBlockIcons(IIconRegister reg) {
		this.frontI = reg.registerIcon(ModInfo.MODID + ":" + "AlarmFront");
		this.sideI = reg.registerIcon(ModInfo.MODID + ":" + "AlarmSide");
	}
	
	@Override
	public IIcon getIcon(int side, int meta) {
		return side == meta ? frontI : sideI;
	}
	
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z) {
		int meta = world.getBlockMetadata(x, y, z);
		switch(meta) {
			case 0: setBlockBounds(0.0F, 0.5F, 0.0F, 1.0F, 1.0F, 1.0F); break;
			case 1: setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F); break;
			case 2: setBlockBounds(0.0F, 0.0F, 0.5F, 1.0F, 1.0F, 1.0F); break;
			case 3: setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.5F); break;
			case 4: setBlockBounds(0.5F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F); break;
			case 5: setBlockBounds(0.0F, 0.0F, 0.0F, 0.5F, 1.0F, 1.0F); break;
		}
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		return new AlarmTileEntity();
	}
	
    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
        int l = BlockPistonBase.determineOrientation(world, x, y, z, entity);
        world.setBlockMetadataWithNotify(x, y, z, l, 2);
    }
    
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		if (!world.isRemote){
			AlarmTileEntity tile = (AlarmTileEntity) world.getTileEntity(x, y, z);
			if (!player.isSneaking()) {
				int id = AlarmSounds.toInt(tile.getSound());
				tile.setSound(AlarmSounds.toType(id + 1));
				player.addChatComponentMessage(new ChatComponentText("Alarm sound type changed to " + AlarmSounds.toName(tile.getSound())));
			}
		}
		return super.onBlockActivated(world, x, y, z, player, side, hitX, hitY, hitZ);		
	}
	
	@Override
    public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor) {
		if (!world.isRemote && world.getBlockPowerInput(x, y, z) != 0 && ((AlarmTileEntity)world.getTileEntity(x, y, z)).getSound() == AlarmSounds.SPEAKER) {
			((AlarmTileEntity)world.getTileEntity(x, y, z)).playSound();
        }
    }

	@Override
	public boolean canProvidePower() {
		return true;
	}
}
