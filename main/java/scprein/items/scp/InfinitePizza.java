package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class InfinitePizza extends ItemFood {

    //SCP-458
    public InfinitePizza() {
        super(50, false);
        this.setAlwaysEdible();
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":InfinitePizza");
        this.setUnlocalizedName("InfinitePizza");
    }

    //Infinite Amount Of Pizza by Raven
    @Override
    public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
        par3EntityPlayer.getFoodStats().func_151686_a(this, par1ItemStack);
        par2World.playSoundAtEntity(par3EntityPlayer, "random.burp", 0.5F, par2World.rand.nextFloat() * 0.1F + 0.9F);
        this.onFoodEaten(par1ItemStack, par2World, par3EntityPlayer);
        return par1ItemStack;
    }
}
