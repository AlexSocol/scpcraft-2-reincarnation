package alexsocol.scprein.potions;

import net.minecraft.potion.Potion;

public class PotionBlinkDelay extends Potion {

	public static PotionBlinkDelay instance = null;
	
    public PotionBlinkDelay(int par1, boolean par2, int par3) {
        super(par1, par2, par3);
        setIconIndex(0, 0);
    }
    
    public static void init() {
        instance.setIconIndex(5, 2);
        instance.setPotionName("BlinkDelay");
    }
}
