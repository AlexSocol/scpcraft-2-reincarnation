package alexsocol.scprein.items;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class GuardArmor extends ItemArmor {

    private String texturePath = ModInfo.MODID + ":textures/model/armor/";

    public GuardArmor(int id, int armorType) {
        super(ArmorMaterial.DIAMOND, id, armorType);
        this.setCreativeTab(SCPMain.NotSCPItems);
        this.setMaxStackSize(1);
        this.setTextureName();
    }

    public void setTextureName() {
        if (armorType == 0 || armorType == 1 || armorType == 3) {
            this.texturePath += "GuardArmor_" + 1 + ".png";
        } else {
            this.texturePath += "GuardArmor_" + 2 + ".png";
        }
    }

    @Override
    public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, String type) {
        return this.texturePath;
    }
}
