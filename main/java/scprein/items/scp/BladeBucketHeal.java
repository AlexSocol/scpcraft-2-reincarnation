package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BladeBucketHeal extends BladeBucket {

    public BladeBucketHeal() {
        super(RegistrationsList.healingWatertBlock);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":BladeBucketHeal");
        this.setUnlocalizedName("BladeBucketWaterHeal");
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        if (side == 0) {
            --y;
        }
        if (side == 1) {
            ++y;
        }
        if (side == 2) {
            --z;
        }
        if (side == 3) {
            ++z;
        }
        if (side == 4) {
            --x;
        }
        if (side == 5) {
            ++x;
        }
        if (!player.canPlayerEdit(x, y, z, side, stack)) {
            return false;
        } else {
            if (world.isAirBlock(x, y, z)) {
                world.setBlock(x, y, z, RegistrationsList.healingWatertBlock);
                if (!player.capabilities.isCreativeMode) --stack.stackSize;
            }
            return true;
        }
    }
}
