package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class AvengeApple extends ItemFood {

    public AvengeApple() {
        super(4, false);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":AvengeApple");
        this.setUnlocalizedName("AvengeApple");
    }

    @Override
    public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer player) {
        if (!world.isRemote) {
            player.addPotionEffect(new PotionEffect(Potion.harm.id, 1, 0));
        }
        player.getFoodStats().func_151686_a(this, itemstack);
        this.onFoodEaten(itemstack, world, player);
        return itemstack;
    }
}
