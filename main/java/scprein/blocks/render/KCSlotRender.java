package alexsocol.scprein.blocks.render;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.blocks.model.KeyCardSlotModel;;

public class KCSlotRender extends TileEntitySpecialRenderer {

    public static final KeyCardSlotModel model = new KeyCardSlotModel();
    public static final ResourceLocation texture = new ResourceLocation(ModInfo.MODID + ":textures/blocks/KeyCardSlot.png");

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float scale) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y + 0.85, z + 0.5);
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        int meta = tile.blockMetadata & 7;
        if (meta == 0 || meta == 1 || meta == 4 || meta == 5) GL11.glRotatef(90.0F * meta, 0.0F, 1.0F, 0.0F);
        if (meta == 2 || meta == 6) GL11.glRotatef(270.0F, 0.0F, 1.0F, 0.0F);
        if (meta == 3 || meta == 7) GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        GL11.glTranslatef(0.0F, 0.0F, 0.4F);
        
        
        GL11.glScalef(0.5F, 0.5F, 0.5F);
        Minecraft.getMinecraft().renderEngine.bindTexture(texture);
        this.model.render(0.0625F);
        GL11.glPopMatrix();
    }
}
