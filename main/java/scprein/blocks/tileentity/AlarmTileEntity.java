package alexsocol.scprein.blocks.tileentity;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.blocks.tileentity.AlarmTileEntity.AlarmSounds;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class AlarmTileEntity extends TileEntity {

	public enum AlarmSounds {
		DEFAULT, RETRO, SCARY, SPEAKER;
		
		public static String toString(AlarmSounds sound) {
			return ModInfo.MODID + ":" + (sound == AlarmSounds.RETRO ? "alarmretro" : sound == AlarmSounds.SCARY ? "alarmscary" : sound == AlarmSounds.SPEAKER ? "alarmspeaker" : "alarmdefault");
		}
		
		public static int toInt(AlarmSounds sound) {
			return sound == AlarmSounds.RETRO ? 1 : sound == AlarmSounds.SCARY ? 2 : sound == AlarmSounds.SPEAKER ? 3 : 0;
		}
		
		public static AlarmSounds toType(int id) {
			return id == 1 ? AlarmSounds.RETRO : id == 2 ? AlarmSounds.SCARY : id == 3 ? AlarmSounds.SPEAKER : AlarmSounds.DEFAULT ;
		}
		
		public static String toName(AlarmSounds sound) {
			return sound == AlarmSounds.RETRO ? "RETRO" : sound == AlarmSounds.SCARY ? "SCARY" : sound == AlarmSounds.SPEAKER ? "SPEAKER" : "DEFAULT";
		}
	}
	
	public AlarmSounds sound;
	public int delay;
	
	public AlarmTileEntity() {
		this.setSound(AlarmSounds.DEFAULT);
		delay = 0;
	}
	
	/** Sets alarm's current sound */
	public void setSound(AlarmSounds set) {
		this.sound = set;
	}

	/** Gets alarm's current sound */
	public AlarmSounds getSound() {
		return this.sound;
	}

	/** Plays alarm's current sound */
	public void playSound() {
		if (this.delay == 0) {
			this.worldObj.playSoundEffect(xCoord, yCoord, zCoord, AlarmSounds.toString(this.getSound()), 1.0F, worldObj.rand.nextFloat() * 0.1F + 0.9F);
			switch(this.getSound()) {
				case DEFAULT: this.delay = 60; break;
				case RETRO: this.delay = 65; break;
				case SCARY: this.delay = 500; break;
				case SPEAKER: this.delay = 120; break;
			}
		}
	}
	
	public void readFromNBT(NBTTagCompound nbt) {
		this.setSound(AlarmSounds.toType(nbt.getInteger("sound")));
		super.readFromNBT(nbt);
	}
	
	public void writeToNBT(NBTTagCompound nbt) {
		nbt.setInteger("sound", AlarmSounds.toInt(this.getSound()));
		super.writeToNBT(nbt);
	}
	
	public void updateEntity() {
		super.updateEntity();
		if (delay > 0) delay--;
		if (worldObj.getBlockPowerInput(xCoord, yCoord, zCoord) != 0 && this.getSound() != AlarmSounds.SPEAKER && this.delay == 0) {
			this.playSound();
        }
	}
}
