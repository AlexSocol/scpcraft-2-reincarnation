package alexsocol.scprein.items;

import alexsocol.asjlib.Utilities;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class HandRocket extends Item {
	
	public HandRocket() {
        this.setCreativeTab(CreativeTabs.tabTransport);
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":HandRocket");
        this.setUnlocalizedName("HandRocket");
    }
	
	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
		if (!world.isRemote) Utilities.sendToDimensionWithoutPortal(player, SCPMain.JUPITERUPPERID, player.posX, 512, player.posZ);
        return stack;
    }
}
