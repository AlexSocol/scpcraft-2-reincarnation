package alexsocol.scprein.entity.mob;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class TikleMonster extends EntityMob {

    public TikleMonster(World world) {
        super(world);
        this.setSize(0.8F, 0.8F);
        this.setCustomNameTag("SCP-999");
        this.setAlwaysRenderNameTag(true);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(24.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(3.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(0.0D);
    }

    @Override
    public void onCollideWithPlayer(EntityPlayer player) {
		player.addPotionEffect(new PotionEffect(Potion.heal.id, 10, 0));
		player.addPotionEffect(new PotionEffect(Potion.confusion.id, 80, 1));
    }
    
    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public boolean allowLeashing() {
    	return true;
    }

    @Override
    public boolean attackEntityAsMob(Entity entity) {
        return false;
    }
    
    @Override
    public String getLivingSound() {
        return "mob.slime.big";
    }
    
    @Override
    public String getHurtSound() {
        return "mob.slime.big";
    }

    @Override
    public String getDeathSound() {
        return "mob.slime.big";
    }
}
