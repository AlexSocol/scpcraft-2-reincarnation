package alexsocol.scprein;

import alexsocol.scprein.proxy.CommonProxy;
import alexsocol.scprein.utils.PotionRegistry;
import alexsocol.scprein.utils.RegistrationsList;
import alexsocol.scprein.utils.SCPGuiHandler;
import alexsocol.scprein.world.generation.SCPWorldGeneration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;

@Mod(modid = ModInfo.MODID, name = ModInfo.NAME, version = ModInfo.VERSION)

public class SCPMain {
	public SCPGuiHandler guiHandler = new SCPGuiHandler();
	public SCPWorldGeneration generator = new SCPWorldGeneration();
	public static final int SCP1499ID = 1499;
    public static final ToolMaterial TWBTB = EnumHelper.addToolMaterial("TWBTB", 10, 1, 1000.0F, 0.0F, 0);
    public static final ToolMaterial BLADE = EnumHelper.addToolMaterial("Blade", 5, 1000, 12.0F, 5.0F, 25);
    public static final ToolMaterial JIAN = EnumHelper.addToolMaterial("JIAN", 0, 1000, 0, 9996, 0);
    public static final ArmorMaterial DClass = EnumHelper.addArmorMaterial("DCLASS", 5, new int[]{1, 3, 2, 1}, 15);

    @Instance(ModInfo.MODID)
    public static SCPMain instance;

    @SidedProxy(clientSide = "alexsocol." + ModInfo.MODID + ".proxy.ClientProxy", serverSide = "alexsocol." + ModInfo.MODID + ".proxy.CommonProxy")
    public static CommonProxy proxy;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	proxy.preinit();
        proxy.registerRenderThings();
        proxy.initializeAndRegisterHandlers();
        GameRegistry.registerWorldGenerator(generator, 0);
        PotionRegistry.enlargePotionList();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
		NetworkRegistry.INSTANCE.registerGuiHandler(SCPMain.instance, guiHandler);
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
		NetworkRegistry.INSTANCE.registerGuiHandler(SCPMain.instance, guiHandler);
    }
    
	public static CreativeTabs NotSCPBlocks = new CreativeTabs("NotSCPBlocks") {
		@Override
		@SideOnly(Side.CLIENT)
		public Item getTabIconItem() {
			return Item.getItemFromBlock(RegistrationsList.reinforcedIronBlock);
		}
	};
	
    public static CreativeTabs NotSCPItems = new CreativeTabs("NotSCPItems") {
    	@Override
        @SideOnly(Side.CLIENT)
        public Item getTabIconItem() {
            return RegistrationsList.wrenchOmni;
        }
    };
    
    public static CreativeTabs SCPBlocks = new CreativeTabs("SCPBlocks") {
    	@Override
        @SideOnly(Side.CLIENT)
        public Item getTabIconItem() {
            return Item.getItemFromBlock(RegistrationsList.infectionCrystal);
        }
    };
    
    public static CreativeTabs SCPItems = new CreativeTabs("SCPItems") {
    	@Override
        @SideOnly(Side.CLIENT)
        public Item getTabIconItem() {
            return RegistrationsList.panacea;
        }
    };
}