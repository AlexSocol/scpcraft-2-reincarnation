package alexsocol.scprein.potions;

import net.minecraft.potion.Potion;

public class PotionFlashBang extends Potion {

	public static PotionFlashBang instance = null;
	
    public PotionFlashBang(int par1, boolean par2, int par3) {
        super(par1, par2, par3);
        setIconIndex(0, 0);
    }
    
    public static void init() {
        instance.setIconIndex(5, 1);
        instance.setPotionName("Flashbang");
    }
    
    @Override
    public boolean hasStatusIcon() {
        return false;
    }
}
