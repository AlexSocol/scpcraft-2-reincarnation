package alexsocol.scprein.entity.mob;

import alexsocol.scprein.ModInfo;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class HDReptile extends EntityMob {

    public HDReptile(World world) {
        super(world);
        this.setSize(1.5F, 0.75F);
        this.setCustomNameTag("SCP-682");
        this.setAlwaysRenderNameTag(true);
        this.stepHeight = 1.0F;
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(9001.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.6D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(50.0D);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public void onUpdate() {
        int unlimitedInt = 20 * 60 * 60 * 24 * 365;
        addPotionEffect(new PotionEffect(Potion.fireResistance.id, unlimitedInt, 0));
        addPotionEffect(new PotionEffect(Potion.regeneration.id, unlimitedInt, 0));
        addPotionEffect(new PotionEffect(Potion.resistance.id, unlimitedInt, 2));
        super.onUpdate();
    }

    @Override
    public String getLivingSound() {
    	return ModInfo.MODID + ":scp682living";
    }

    @Override
    public String getHurtSound() {
        return ModInfo.MODID + ":scp682hurt";
    }

    @Override
    public String getDeathSound() {
        return "mob.wither.death";
    }
}
