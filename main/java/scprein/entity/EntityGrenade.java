package alexsocol.scprein.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityGrenade extends EntityThrowable {

    public EntityGrenade(World p_77659_2_, EntityPlayer p_77659_3_) {
        super(p_77659_2_, p_77659_3_);
        setSize(0.5F, 0.5F);
        yOffset = height / 2.0F;
    }

    public EntityGrenade(World w) {
        super(w);
    }

    @Override
    public void onUpdate() {
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;
        motionY -= 0.039999999105930328D;
        moveEntity(motionX, motionY, motionZ);
        motionX *= 0.98000001907348633D;
        motionY *= 0.98000001907348633D;
        motionZ *= 0.98000001907348633D;
        if (onGround) {
            motionX *= 0.69999998807907104D;
            motionZ *= 0.69999998807907104D;
            motionY *= -0.5D;
        }
        if (fuse++ < 80) {
            worldObj.spawnParticle("smoke", posX, posY + 0.5d, posZ, 0, 0, 0);
        }
        if (fuse++ >= 80) {
            worldObj.createExplosion(this, posX, posY, posZ, 1f, true);
            setDead();
        }
    }

    @Override
    protected void onImpact(MovingObjectPosition p_70184_1_) {
    }
    private int fuse = 0;
}
