package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BladeBucketBlood extends BladeBucket {

    public BladeBucketBlood() {
        super(RegistrationsList.liquidBloodBlock);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":BladeBucketBlood");
        this.setUnlocalizedName("BladeBucketBlood");
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        if (side == 0) {
            --y;
        }
        if (side == 1) {
            ++y;
        }
        if (side == 2) {
            --z;
        }
        if (side == 3) {
            ++z;
        }
        if (side == 4) {
            --x;
        }
        if (side == 5) {
            ++x;
        }
        if (!player.canPlayerEdit(x, y, z, side, stack)) {
            return false;
        } else {
            if (world.isAirBlock(x, y, z)) {
                world.setBlock(x, y, z, RegistrationsList.liquidBloodBlock);
                if (!player.capabilities.isCreativeMode) --stack.stackSize;
            }
            return true;
        }
    }
}
