package alexsocol.scprein.entity.mob;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class Doc extends EntityMob {

	public boolean isAngry = false;
	
    public Doc(World world) {
        super(world);
        this.setSize(0.8F, 1.8F);
        this.setCustomNameTag("SCP-049");
        this.setAlwaysRenderNameTag(true);
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(40.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(2.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(10.0D);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}
    
	@Override
    public boolean attackEntityAsMob(Entity entity) {
        if (!(entity instanceof EntityPlayer)) {
    		super.attackEntityAsMob(entity);
        }
        int dif = this.worldObj.difficultySetting.getDifficultyId();
		if (dif > 0) {
			((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.wither.id, dif * 100, 0));
		}
        return false;
    }
	
	@Override
	public boolean attackEntityFrom(DamageSource source, float amount) {
		Entity var3 = source.getEntity();
		if (var3 instanceof EntityPlayer) {
			isAngry = true;
			entityToAttack = var3;
		}
		return super.attackEntityFrom(source, amount);
	}

    @Override
	public Entity findPlayerToAttack() {
		return entityToAttack;
	}

    @Override
	public boolean interact(EntityPlayer player) {
		ItemStack stack = player.inventory.getCurrentItem();        
		if (stack != null && stack.getItem() == RegistrationsList.amulet) {
			if(!worldObj.isRemote) {
				player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "Villager: " + EnumChatFormatting.RESET + "Damn You."));
			}
			this.setDead();
			this.worldObj.spawnParticle("largeexplode", this.posX, this.posY + (double)(this.height / 2.0F), this.posZ, 0.0D, 0.0D, 0.0D);
			if (!this.worldObj.isRemote) {
				EntityVillager villager = new EntityVillager(this.worldObj);
				villager.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, this.rotationPitch);
				villager.setHealth(this.getHealth());
				villager.renderYawOffset = this.renderYawOffset;
				this.worldObj.spawnEntityInWorld(villager);
			}
			return true;
		} else {
			say(player);
			return super.interact(player);
		}
	}

    @Override
	public void onLivingUpdate() {
		EntityPlayer player = this.worldObj.getClosestPlayerToEntity(this, 256.0D);
		if (rand.nextInt(400) == 0) say(player);
		super.onLivingUpdate();
	}
	
	public void say(EntityPlayer player) {
		int q = rand.nextInt(8);
		if(player != null) {
			if(q == 1) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-049: " + EnumChatFormatting.RESET + "What is this place?"));
			if(q == 2) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-049: " + EnumChatFormatting.RESET + "Hello you marvelous sir!"));
			if(q == 3) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-049: " + EnumChatFormatting.RESET + "My cure is the most effective, doctor!"));
			if(q == 4) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-049: " + EnumChatFormatting.RESET + "The Pestilence is here, and I can sense it."));
			if(q == 5) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-049: " + EnumChatFormatting.RESET + "It is my duty in life to rid the world of the Great Pestilence."));
			if(q == 6) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-049: " + EnumChatFormatting.RESET + "I assume you are also a doctor?"));
			if(q == 7) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-049: " + EnumChatFormatting.RESET + "Is this a laboratory? It is quite marvelous."));					
		}
	}

    @Override
	public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound) {
		super.writeEntityToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setBoolean("Anger", isAngry);
	}

    @Override
	public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound) {
		super.readEntityFromNBT(par1NBTTagCompound);
		this.isAngry = par1NBTTagCompound.getBoolean("Anger");
	}

    @Override
	public String getLivingSound() {
		return "mob.villager.idle";
	}

    @Override
	public String getHurtSound() {
		return "mob.villager.hit";
	}

    @Override
    public String getDeathSound() {
		return "mob.villager.death";
	}
}
