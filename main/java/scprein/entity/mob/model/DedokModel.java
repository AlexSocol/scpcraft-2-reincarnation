package alexsocol.scprein.entity.mob.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;

public class DedokModel extends ModelBase {

    ModelRenderer head;
    ModelRenderer body;
    ModelRenderer leftarm;
    ModelRenderer rightleg;
    ModelRenderer leftleg;
    ModelRenderer rightarm;
    ModelRenderer head1;
    ModelRenderer head2;
    ModelRenderer head3;
    ModelRenderer head4;
    ModelRenderer head5;
    ModelRenderer head6;

    public DedokModel() {
        textureWidth = 64;
        textureHeight = 32;
        body = new ModelRenderer(this, 16, 16);
        body.addBox(-4F, 0F, -2F, 8, 12, 4);
        body.setRotationPoint(0F, 0F, 0F);
        setRotation(body, 0F, 0F, 0F);
        leftarm = new ModelRenderer(this, 40, 16);
        leftarm.addBox(-1F, -2F, -2F, 4, 12, 4);
        leftarm.setRotationPoint(5F, 2F, 0F);
        setRotation(leftarm, 0F, 0F, 0F);
        rightleg = new ModelRenderer(this, 0, 16);
        rightleg.addBox(-2F, 0F, -2F, 4, 12, 4);
        rightleg.setRotationPoint(-2F, 12F, 0F);
        setRotation(rightleg, 0F, 0F, 0F);
        leftleg = new ModelRenderer(this, 0, 16);
        leftleg.addBox(-2F, 0F, -2F, 4, 12, 4);
        leftleg.setRotationPoint(2F, 12F, 0F);
        setRotation(leftleg, 0F, 0F, 0F);
        rightarm = new ModelRenderer(this, 40, 16);
        rightarm.addBox(-3F, -2F, -2F, 4, 12, 4);
        rightarm.setRotationPoint(-5F, 2F, 0F);
        setRotation(rightarm, 0F, 0F, 0F);
        head = new ModelRenderer(this, 6, 6);
        head.addBox(2.9F, -2F, -4F, 1, 2, 8);
        head.setRotationPoint(0F, 0F, 0F);
        setRotation(head, 0F, 0F, 0F);
        head1 = new ModelRenderer(this, 0, 0);
        head1.addBox(-3.2F, -7.8F, -4F, 7, 1, 8);
        head1.setRotationPoint(0F, 0F, 0F);
        setRotation(head1, 0F, 0F, 0F);
        head2 = new ModelRenderer(this, 6, 7);
        head2.addBox(2.9F, -1F, -4.1F, 1, 1, 8);
        head2.setRotationPoint(0F, 0F, 0F);
        setRotation(head2, 0F, 0F, 0F);
        head3 = new ModelRenderer(this, 0, 6);
        head3.addBox(-3.9F, -2F, -4F, 7, 2, 8);
        head3.setRotationPoint(0F, 0F, 0F);
        setRotation(head3, 0F, 0F, 0F);
        head4 = new ModelRenderer(this, 0, 6);
        head4.addBox(-3.9F, -1F, -4.1F, 1, 1, 8);
        head4.setRotationPoint(0F, 0F, 0F);
        setRotation(head4, 0F, 0F, 0F);
        head5 = new ModelRenderer(this, 0, 1);
        head5.addBox(-4F, -7F, -4F, 8, 5, 8);
        head5.setRotationPoint(0F, 0F, 0F);
        setRotation(head5, 0F, 0F, 0F);
        head6 = new ModelRenderer(this, 0, 0);
        head6.addBox(-3.8F, -7.8F, -4F, 1, 1, 8);
        head6.setRotationPoint(0F, 0F, 0F);
        setRotation(head6, 0F, 0F, 0F);
    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        head.render(f5);
        body.render(f5);
        leftarm.render(f5);
        rightleg.render(f5);
        leftleg.render(f5);
        rightarm.render(f5);
        head1.render(f5);
        head2.render(f5);
        head3.render(f5);
        head4.render(f5);
        head5.render(f5);
        head6.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        
        this.rightleg.rotateAngleX = MathHelper.cos(f * 0.6662F) * 1.4F * f1;
        this.leftleg.rotateAngleX = MathHelper.cos(f * 0.6662F + (float)Math.PI) * 1.4F * f1;
        this.leftarm.rotateAngleX = MathHelper.cos(f * 0.6662F) * 2.0F * f1 * 0.5F;
        this.rightarm.rotateAngleX = MathHelper.cos(f * 0.6662F + (float)Math.PI) * 2.0F * f1 * 0.5F;
        
        this.head.rotateAngleY = f3 / (180F / (float)Math.PI);
        this.head.rotateAngleX = f4 / (180F / (float)Math.PI);
        this.head1.rotateAngleY = f3 / (180F / (float)Math.PI);
        this.head1.rotateAngleX = f4 / (180F / (float)Math.PI);
        this.head2.rotateAngleY = f3 / (180F / (float)Math.PI);
        this.head2.rotateAngleX = f4 / (180F / (float)Math.PI);
        this.head3.rotateAngleY = f3 / (180F / (float)Math.PI);
        this.head3.rotateAngleX = f4 / (180F / (float)Math.PI);
        this.head4.rotateAngleY = f3 / (180F / (float)Math.PI);
        this.head4.rotateAngleX = f4 / (180F / (float)Math.PI);
        this.head5.rotateAngleY = f3 / (180F / (float)Math.PI);
        this.head5.rotateAngleX = f4 / (180F / (float)Math.PI);
        this.head6.rotateAngleY = f3 / (180F / (float)Math.PI);
        this.head6.rotateAngleX = f4 / (180F / (float)Math.PI);
    }
}
