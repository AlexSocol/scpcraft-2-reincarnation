package alexsocol.scprein.blocks;

import java.util.Random;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.SmokerTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPistonBase;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class Smoker extends Block implements ITileEntityProvider {

    public Smoker() {
        super(Material.iron);
        this.setBlockName("Smoker");
        this.setBlockTextureName(ModInfo.MODID + ":Smoker");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(1.0F);
        this.setHarvestLevel("pickaxe", 2);
        this.setResistance(5.0F);
        this.stepSound = Block.soundTypeMetal;
    }

	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return new SmokerTileEntity();
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	
	@Override
	public int getRenderType() {
		return -1;
	}
	
    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
        int l = BlockPistonBase.determineOrientation(world, x, y, z, entity);
        world.setBlockMetadataWithNotify(x, y, z, l, 2);
    }
}
