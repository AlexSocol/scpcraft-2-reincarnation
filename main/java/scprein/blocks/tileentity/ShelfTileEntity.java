package alexsocol.scprein.blocks.tileentity;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class ShelfTileEntity extends TileEntity {
	public ItemStack item;

	public ShelfTileEntity() { }

	public ItemStack getItem() {
		return this.item;
	}

	public ItemStack setItem(ItemStack it) {
		return this.item = it;
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
		this.readFromNBT(pkt.func_148857_g());
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound var1 = new NBTTagCompound();
		this.writeToNBT(var1);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, var1);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		if (nbt.hasKey("item"))
			setItem(ItemStack.loadItemStackFromNBT(nbt.getCompoundTag("item")));
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		NBTTagCompound compound = new NBTTagCompound();
		nbt.setTag("item", compound);
		if (getItem() != null)
			getItem().writeToNBT(compound);
	}
}
