package alexsocol.scprein.entity.mob;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.utils.AISCPList;

public class Dedok extends EntityMob {

    public Dedok(World world) {
        super(world);
		this.setSize(0.8F, 1.8F);
        this.setCustomNameTag("SCP-106");
        this.setAlwaysRenderNameTag(true);
		isImmuneToFire = true;
		stepHeight = 2F;
		getNavigator().setAvoidsWater(true);
		getNavigator().clearPathEntity();
		getNavigator().getPathToEntityLiving(entityToAttack);
		getNavigator().tryMoveToEntityLiving(entityToAttack, 1F);
		getNavigator().setEnterDoors(true);
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(100.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.3D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(6.0D);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}
    
    @Override
    public void onUpdate() {
    	int X = (int) this.posX;
		int Y = (int) this.posY;
		int Z = (int) this.posZ;

		if (this.posX < 0) {
			X = (int) this.posX - 1;
		}
		if (this.posZ < 0) {
			Z = (int) this.posZ - 1;
		}
		
		for (int y = -1; y < 2; y++) {
			if (this.worldObj.rand.nextInt(5) == 0) AISCPList.corruptBlock(this.worldObj, X, Y + y, Z);
		}
		
		if (this.entityToAttack != null && this.entityToAttack.getDistanceSqToEntity(this) > 64.0D) {
			AISCPList.teleportToEntity(this.entityToAttack, this);
		}
		
    	super.onUpdate();
    }

    @Override
    public void moveEntity(double x, double y, double z) {
		this.worldObj.theProfiler.startSection("move");
		boundingBox.offset(x, 0, z);
		ySize *= 0.4F;
		double d2 = x;
		double d3 = y;
		double d4 = z;
		AxisAlignedBB axisalignedbb = boundingBox.copy();

		List<?> list = worldObj.getCollidingBoundingBoxes(this, boundingBox.addCoord(x, y, z));

		for (int i = 0; i < list.size(); i++) {
			y = ((AxisAlignedBB)list.get(i)).calculateYOffset(boundingBox, y);
		}

		boundingBox.offset(0.0D, y, 0.0D);

		if (!field_70135_K && d3 != y) {
			x = y = z = 0.0D;
		}

		boolean flag1 = onGround || d3 != y && d3 < 0.0D;

		for (int j = 0; j < list.size(); j++) {
			x = ((AxisAlignedBB)list.get(j)).calculateXOffset(boundingBox, x);
		}

		boundingBox.offset(x, 0.0D, 0.0D);

		if (!field_70135_K && d2 != x) {
			x = y = z = 0.0D;
		}

		for (int k = 0; k < list.size(); k++) {
			z = ((AxisAlignedBB)list.get(k)).calculateZOffset(boundingBox, z);
		}

		boundingBox.offset(0.0D, 0.0D, z);

		if (!field_70135_K && d4 != z) {
			x = y = z = 0.0D;
		}

		if (stepHeight > 0.0F && flag1 && ySize < 0.05F && (d2 != x || d4 != z)) {
			double d6 = x;
			double d8 = y;
			double d10 = z;
			x = d2;
			y = stepHeight;
			z = d4;
			AxisAlignedBB axisalignedbb1 = boundingBox.copy();
			boundingBox.setBB(axisalignedbb);
			List<?> list1 = worldObj.getCollidingBoundingBoxes(this, boundingBox.addCoord(x, y, z));

			for (int j2 = 0; j2 < list1.size(); j2++) {
				y = ((AxisAlignedBB)list1.get(j2)).calculateYOffset(boundingBox, y);
			}

			boundingBox.offset(0.0D, y, 0.0D);

			if (!field_70135_K && d3 != y) {
				x = y = z = 0.0D;
			}

			for (int k2 = 0; k2 < list1.size(); k2++) {
				x = ((AxisAlignedBB)list1.get(k2)).calculateXOffset(boundingBox, x);
			}

			boundingBox.offset(x, 0.0D, 0.0D);

			if (!field_70135_K && d2 != x) {
				x = y = z = 0.0D;
			}

			for (int l2 = 0; l2 < list1.size(); l2++) {
				z = ((AxisAlignedBB)list1.get(l2)).calculateZOffset(boundingBox, z);
			}

			boundingBox.offset(0.0D, 0.0D, z);

			if (!field_70135_K && d4 != z) {
				x = y = z = 0.0D;
			}

			if (!field_70135_K && d3 != y) {
				x = y = z = 0.0D;
			} else {
				y = -stepHeight;

				for (int i3 = 0; i3 < list1.size(); i3++) {
					y = ((AxisAlignedBB)list1.get(i3)).calculateYOffset(boundingBox, y);
				}

				boundingBox.offset(0.0D, y, 0.0D);
			}

			if (d6 * d6 + d10 * d10 >= x * x + z * z) {
				x = d6;
				y = d8;
				z = d10;
				boundingBox.setBB(axisalignedbb1);
			} else {
				double d11 = boundingBox.minY - (double)(int)boundingBox.minY;

				if (d11 > 0.0D) {
					ySize += d11 + 0.01D;
				}
			}
		}	        	     
		posX = (boundingBox.minX + boundingBox.maxX) / 2D;
		posY = (boundingBox.minY + (double)yOffset) - (double)ySize;
		posZ = (boundingBox.minZ + boundingBox.maxZ) / 2D;
		onGround = d3 != y && d3 < 0.0D;
		updateFallState(y, onGround);	        
		this.worldObj.theProfiler.endSection();
	}
    
    @Override
    public Entity findPlayerToAttack() {
        EntityPlayer entityplayer = this.worldObj.getClosestVulnerablePlayerToEntity(this, 256.0D);
        return entityplayer != null ? entityplayer : null;
    }

    @Override
	public String getLivingSound() {
		return ModInfo.MODID + ":scp106living";
	}

    @Override
	public String getHurtSound() {
		return "mob.blaze.breathe";
	}

    @Override
    public String getDeathSound() {
		return "mob.blaze.death";
	}
}