package alexsocol.scprein.blocks.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;

public class SmokerModel extends ModelBase {
	ModelRenderer Shape1;
	ModelRenderer Shape2;
	ModelRenderer Shape3;
	ModelRenderer Shape4;
	ModelRenderer Shape5;
	ModelRenderer Shape6;
	ModelRenderer Shape7;
	ModelRenderer Shape8;
	ModelRenderer Shape9;
	ModelRenderer Shape10;
	ModelRenderer Shape11;
	ModelRenderer Shape12;
	ModelRenderer Shape13;
	ModelRenderer Shape14;

	public SmokerModel() {
		textureWidth = 68;
		textureHeight = 31;

		Shape1 = new ModelRenderer(this, 0, 0);
		Shape1.addBox(-8F, -8F, 20F, 16, 16, 4);
		Shape1.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape1, -1.570796F, 0F, 0F);
		Shape2 = new ModelRenderer(this, 0, 20);
		Shape2.addBox(-4F, 10F, 6F, 8, 10, 1);
		Shape2.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape2, 0F, 0F, 0F);
		Shape3 = new ModelRenderer(this, 0, 20);
		Shape3.addBox(-4F, 10F, 6F, 8, 10, 1);
		Shape3.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape3, 0F, 1.570796F, 0F);
		Shape4 = new ModelRenderer(this, 0, 20);
		Shape4.addBox(-4F, 10F, 6F, 8, 10, 1);
		Shape4.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape4, 0F, 3.141593F, 0F);
		Shape5 = new ModelRenderer(this, 0, 20);
		Shape5.addBox(-4F, 10F, 6F, 8, 10, 1);
		Shape5.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape5, 0F, -1.570796F, 0F);
		Shape6 = new ModelRenderer(this, 0, 20);
		Shape6.addBox(-5F, 10F, 5F, 1, 10, 1);
		Shape6.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape6, 0F, -1.570796F, 0F);
		Shape7 = new ModelRenderer(this, 0, 20);
		Shape7.addBox(4F, 10F, 5F, 1, 10, 1);
		Shape7.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape7, 0F, -1.570796F, 0F);
		Shape8 = new ModelRenderer(this, 0, 20);
		Shape8.addBox(-5F, 10F, 5F, 1, 10, 1);
		Shape8.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape8, 0F, 0F, 0F);
		Shape9 = new ModelRenderer(this, 0, 20);
		Shape9.addBox(4F, 10F, 5F, 1, 10, 1);
		Shape9.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape9, 0F, 0F, 0F);
		Shape10 = new ModelRenderer(this, 0, 20);
		Shape10.addBox(-5F, 10F, 5F, 1, 10, 1);
		Shape10.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape10, 0F, 1.570796F, 0F);
		Shape11 = new ModelRenderer(this, 0, 20);
		Shape11.addBox(4F, 10F, 5F, 1, 10, 1);
		Shape11.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape11, 0F, 1.570796F, 0F);
		Shape12 = new ModelRenderer(this, 0, 20);
		Shape12.addBox(-5F, 10F, 5F, 1, 10, 1);
		Shape12.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape12, 0F, 3.141593F, 0F);
		Shape13 = new ModelRenderer(this, 0, 20);
		Shape13.addBox(4F, 10F, 5F, 1, 10, 1);
		Shape13.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape13, 0F, 3.141593F, 0F);
		Shape14 = new ModelRenderer(this, 40, 0);
		Shape14.addBox(-7F, -7F, -10F, 14, 14, 0);
		Shape14.setRotationPoint(0F, 0F, 0F);
		setRotation(Shape14, 1.570796F, 0F, 0F);
	}

	public void render(float f5) {
		Shape1.render(f5);
		Shape2.render(f5);
		Shape3.render(f5);
		Shape4.render(f5);
		Shape5.render(f5);
		Shape6.render(f5);
		Shape7.render(f5);
		Shape8.render(f5);
		Shape9.render(f5);
		Shape10.render(f5);
		Shape11.render(f5);
		Shape12.render(f5);
		Shape13.render(f5);
		Shape14.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}
}
