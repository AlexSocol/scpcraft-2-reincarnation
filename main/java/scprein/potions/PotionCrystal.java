package alexsocol.scprein.potions;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.util.DamageSource;

public class PotionCrystal extends Potion {

	public static PotionCrystal instance = null;
	
    public PotionCrystal(int par1, boolean par2, int par3) {
        super(par1, par2, par3);
        setIconIndex(0, 0);
    }
    
    public static void init() {
        instance.setIconIndex(5, 3);
        instance.setPotionName("Crystal");
    }
}
