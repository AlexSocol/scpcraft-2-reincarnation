package alexsocol.scprein.blocks;

import java.util.List;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.ChairTileEntity;
import alexsocol.scprein.entity.SCPEntityMountableBlock;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class Chair extends Block implements ITileEntityProvider {

    public Chair() {
        super(Material.iron);
        this.setBlockBounds(0.125F, 0.0F, 0.125F, 0.875F, 0.625F, 0.875F);
        this.setBlockName("Chair");
        this.setBlockTextureName(ModInfo.MODID + ":ChairIcon");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(2.0F);
        this.setHarvestLevel("pickaxe", 1);
        this.setResistance(50.0F);
        this.setStepSound(soundTypeMetal);
    }

    public TileEntity createNewTileEntity(World world, int par2) {
        return new ChairTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
        int l = MathHelper.floor_double((double)(entity.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
        world.setBlockMetadataWithNotify(x, y, z, l, 2);
    }
    
    @Override
	public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, int side, float hitX, float hitY, float hitZ) {
		return onBlockActivated(world, i, j, k, entityplayer, 0.5F, 0.5F, 0.5F, 0, 0, 0, 0);
	}
    
    public static boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, float x, float y, float z, int north, int south, int east, int west) {
		if (!world.isRemote) {
			//Looks for EMBs up to 1 block away from the activated block. Hopefully you didn't set the mounting position further away than this.
			List <SCPEntityMountableBlock> listEMB = world.getEntitiesWithinAABB(SCPEntityMountableBlock.class, AxisAlignedBB.getBoundingBox(i, j, k, i + 1.0D, j + 1.0D, k + 1.0D).expand(1D, 1D, 1D));
			for (SCPEntityMountableBlock entitytocheck : listEMB) {
				//Looks for an EMB created by this block.
				if (entitytocheck.getOrgBlockPosX() == i && entitytocheck.getOrgBlockPosY() == j && entitytocheck.getOrgBlockPosZ() == k) {
					entitytocheck.interactFirst(entityplayer);
					return true;
				}
			}
			//Sets coordinates for mounting a north oriented block.
			float mountingX = i + x;
			float mountingY = j + y;
			float mountingZ = k + z;
			//Changes coordinates for mounting to compensate for none-north block orientation.
			if(north != south)  {
				int md = world.getBlockMetadata(i, j, k);
				if (md == east) {
					mountingX = i + 1 - z; 
					mountingZ = k + x; 
				} else if (md == south) {
					mountingX = i + 1 - x; 
					mountingZ = k + 1 - z; 
				} else if (md == west) {
					mountingX = i + z; 
					mountingZ = k + 1 - x; 
				}
			}
			//Creates a new EMB if none had been created already or if the old one was bugged.
			SCPEntityMountableBlock nemb = new SCPEntityMountableBlock(world, entityplayer, i, j, k, mountingX, mountingY, mountingZ); 
			world.spawnEntityInWorld(nemb);
			nemb.interactFirst(entityplayer);
			return true;
		}
		return true;
	}
}
