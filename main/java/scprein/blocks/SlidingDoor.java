package alexsocol.scprein.blocks;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.proxy.ClientProxy;
import alexsocol.scprein.utils.RegistrationsList;

public class SlidingDoor extends Block {
	@SideOnly(Side.CLIENT)
	public static IIcon lower;
	@SideOnly(Side.CLIENT)
	public static IIcon upper;
	
	public SlidingDoor() {
		super(Material.iron);
		this.setBlockName("SlidingDoor");
		this.setBlockTextureName(ModInfo.MODID + ":SlidingDoor");
		this.setHardness(5.0F);
		this.setResistance(100.0F);
		this.setStepSound(soundTypeMetal);
	}

	// Tech stuff
	@Override
	public IIcon getIcon(int side, int meta) {
		return lower;
	}
	
	@Override
	public void registerBlockIcons(IIconRegister reg) {
    	lower = reg.registerIcon(this.getTextureName() + "Lower");
    	upper = reg.registerIcon(this.getTextureName() + "Upper");
    }
	
	public boolean isOpaqueCube() {
		return false;
	}

	public int getRenderBlockPass() {
		return 0;
	}

	public boolean canRenderInPass(int pass) {
		return true;
	}

	public boolean shouldSideBeRendered(IBlockAccess world, int x, int y, int z, int side) {
		return true;
	}

	public boolean renderAsNormalBlock() {
		return false;
	}

	public int getRenderType() {
		return ClientProxy.slidingRenderID;
	}
	
	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int i, int j, int k) {
		int l = world.getBlockMetadata(i, j, k);           
		if(l == 0 || l == 8) setBlockBounds(0F, 0F, 0.33F, 1F, 1F, 0.66F);
		if(l == 1 || l == 9) setBlockBounds(0.33F, 0F, 0F, 0.66F, 1F, 1F);
		if(l == 2 || l == 10) setBlockBounds(0F, 0F, 0.33F, 1F, 1F, 0.66F);
		if(l == 3 || l == 11) setBlockBounds(0.33F, 0F, 0F, 0.66F, 1F, 1F);
		if(l == 4 || l == 5 || l == 6 || l == 7 || l == 12 || l == 13 || l == 14 || l == 15) setBlockBounds(0F, 0F, 0F, 0F, 0F, 0F);
	} 

	// Block placing stuff
	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block) {
		int meta = world.getBlockMetadata(x, y, z);
		boolean isPowered = world.isBlockIndirectlyGettingPowered(x, y, z) || world.isBlockIndirectlyGettingPowered(x, y + 1, z) || world.isBlockIndirectlyGettingPowered(x, y - 1, z);
		if (block != Blocks.air && block.canProvidePower()) {
			if (isPowered) {
				// Bottom rotation 0 open
				if (meta == 0) {
					world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 12, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 4, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
				// Bottom rotation 1 open
				if (meta == 1) {
					world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 13, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 5, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
				// Bottom rotation 2 open
				if (meta == 2) {
					world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 14, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 6, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
				// Bottom rotation 3 open
				if (meta == 3) {
					world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 15, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 7, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
				// Top rotation 0 open
				if (meta == 8) {
					world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 4, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 12, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
				// Top rotation 1 open
				if (meta == 9) {
					world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 5, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 13, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
				// Top rotation 2 open
				if (meta == 10) {
					world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 6, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 14, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
				// Top rotation 3 open
				if (meta == 11) {
					world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 7, 2);
					world.setBlock(x, y, z, RegistrationsList.slidingDoor, 15, 2);
					world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
					world.markBlockRangeForRenderUpdate(x, y + 1, z, x, y, z);
					world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
				}
			}
		} else {
			// Bottom rotation 0 close
			if (meta == 4) {
				world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 8, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 0, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
			// Bottom rotation 1 close
			if (meta == 5) {
				world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 9, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 1, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
			// Bottom rotation 2 close
			if (meta == 6) {
				world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 10, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 2, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
			// Bottom rotation 3 close
			if (meta == 7) {
				world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 11, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 3, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
			// Top rotation 0 close
			if (meta == 12) {
				world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 0, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 8, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
			// Top rotation 1 close
			if (meta == 13) {
				world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 1, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 9, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
			// Top rotation 2 close
			if (meta == 14) {
				world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 2, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 10, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
			// Top rotation 3 close
			if (meta == 15) {
				world.setBlock(x, y - 1, z, RegistrationsList.slidingDoor, 3, 2);
				world.setBlock(x, y, z, RegistrationsList.slidingDoor, 11, 2);
				world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
				world.markBlockRangeForRenderUpdate(x, y - 1, z, x, y, z);
				world.playSoundEffect(x, (double) y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			}
		}
		// BLOCK DESTROY LOGIC
		if ((meta == 0 || meta == 1 || meta == 2 || meta == 3 || meta == 4 || meta == 5 || meta == 6 || meta == 7) && world.getBlock(x, y + 1, z) != RegistrationsList.slidingDoor) {
			world.setBlock(x, y, z, Blocks.air);
		}
		
		if ((meta == 8 || meta == 9 || meta == 10 || meta == 11 || meta == 12 || meta == 13 || meta == 14 || meta == 15) && world.getBlock(x, y - 1, z) != RegistrationsList.slidingDoor) {
			world.setBlock(x, y, z, Blocks.air);
		}
		world.scheduleBlockUpdate(x, y, z, this, meta);
	 }
	 
	
	 public void updateTick(World world, int x, int y, int z, Random random) {
		 int meta = world.getBlockMetadata(x, y, z);
		 boolean isPowered = world.isBlockIndirectlyGettingPowered(x, y, z) || world.isBlockIndirectlyGettingPowered(x, y + 1, z) || world.isBlockIndirectlyGettingPowered(x, y - 1, z);
		 if (isPowered) {
			 if(meta == 0) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 12, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 4, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }
			 
			 if(meta == 1) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 13, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 5, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }
			 
			 if(meta == 2) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 14, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 6, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }
			 
			 if(meta == 3) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 15, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 7, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":dooropen", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }
		 } else {
			 if(meta == 4) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 8, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 0, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }	 
			 if(meta == 5) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 9, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 1, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }	 
			 if(meta == 6) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 10, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 2, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }	 
			 if(meta == 7) {
				 world.setBlock(x, y + 1, z, RegistrationsList.slidingDoor, 11, 2);
				 world.setBlock(x, y, z, RegistrationsList.slidingDoor, 3, 2);
				 world.playSoundEffect(x, (double)y + 0.5D, z, ModInfo.MODID + ":doorclose", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
			 }
		 }
		 world.scheduleBlockUpdate(x, y, z, this, meta);
	 }

	 @Override
	 public Item getItemDropped(int meta, Random random, int chance) {
		 return RegistrationsList.slidingDoorItem;
	 }
	    
	public boolean canPlaceBlockAt(World world, int x, int y, int z) {
		return y >= world.getHeight() - 1 ? false :
				World.doesBlockHaveSolidTopSurface(world, x, y - 1, z)
				&& super.canPlaceBlockAt(world, x, y, z)
				&& super.canPlaceBlockAt(world, x, y + 1, z);
	}
	 
	public int getMobilityFlag() {
		return 2;
	}
	 
	@SideOnly(Side.CLIENT)
	public Item getItem(World p_149694_1_, int p_149694_2_, int p_149694_3_, int p_149694_4_) {
		return RegistrationsList.slidingDoorItem;
	}
}
