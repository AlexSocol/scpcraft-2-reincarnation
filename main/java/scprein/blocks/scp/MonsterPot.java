package alexsocol.scprein.blocks.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.scp.tileentity.MonsterPotTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class MonsterPot extends Block implements ITileEntityProvider, SCPBlock {

    //SCP-019
    public MonsterPot() {
        super(Material.clay);
        this.setBlockBounds(0.2F, 0.0F, 0.2F, 0.8F, 1.6F, 0.8F);
        this.setBlockName("MonsterPot");
        this.setBlockUnbreakable();
        this.setCreativeTab(SCPMain.SCPBlocks);
        this.setResistance(100500);
        setBlockTextureName(ModInfo.MODID + ":MonsterPotIcon");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new MonsterPotTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public int getMobilityFlag() {
        return 2;
    }
}
