package alexsocol.scprein.blocks.render;
 
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureCompass;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;

import org.lwjgl.opengl.GL11;

import alexsocol.scprein.blocks.tileentity.ShelfTileEntity;
import cpw.mods.fml.common.registry.GameRegistry;

public class ShelfRender extends TileEntitySpecialRenderer {

	public RenderBlocks renderBlocksInstance = new RenderBlocks();
	private Minecraft mc = Minecraft.getMinecraft();
	
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f) {
		renderShelfAt((ShelfTileEntity)tile, x, y, z, f);		
	}
	
	private void renderShelfAt(ShelfTileEntity tile, double x, double y, double z, float f) {
		if(tile.item == null) return;
        GL11.glPushMatrix();
        GL11.glTranslated(x, y, z);
        GL11.glTranslatef(0.5F, 0.25F, 0.5F);
        renderItem(tile);
        GL11.glPopMatrix();
        renderDisplayedName(tile, x, y, z);
	}
	
	public void renderItem(ShelfTileEntity tile) {
		ItemStack itemstack = tile.getItem();
		if (itemstack != null) {
			EntityItem entityitem = new EntityItem(tile.getWorldObj(), 0.0D, 0.0D, 0.0D, itemstack);
			Item item = entityitem.getEntityItem().getItem();
			entityitem.getEntityItem().stackSize = tile.getItem().stackSize;
			entityitem.hoverStart = 0.0F;
			GL11.glPushMatrix();
			int rotation = tile.blockMetadata == 0 ? 2 : tile.blockMetadata == 1 ? 1 : tile.blockMetadata == 2 ? 0 : tile.blockMetadata == 3 ? -1 : 0;
			GL11.glRotatef(180.0F + 90 * rotation, 0.0F, 1.0F, 0.0F);
			if (item == Items.compass) {
				TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
				texturemanager.bindTexture(TextureMap.locationItemsTexture);
				TextureAtlasSprite textureatlassprite1 = ((TextureMap) texturemanager.getTexture(TextureMap.locationItemsTexture)).getAtlasSprite(Items.compass.getIconIndex(entityitem.getEntityItem()).getIconName());

				if (textureatlassprite1 instanceof TextureCompass) {
					TextureCompass texturecompass = (TextureCompass) textureatlassprite1;
					double d0 = texturecompass.currentAngle;
					double d1 = texturecompass.angleDelta;
					texturecompass.currentAngle = 0.0D;
					texturecompass.angleDelta = 0.0D;
					texturecompass.updateCompass(tile.getWorldObj(), tile.xCoord, tile.zCoord, (double) MathHelper.wrapAngleTo180_float((float) (180 + tile.blockMetadata * 90)), false, true);
					texturecompass.currentAngle = d0;
					texturecompass.angleDelta = d1;
				}
				
				TextureAtlasSprite textureatlassprite = ((TextureMap) Minecraft.getMinecraft().getTextureManager().getTexture(TextureMap.locationItemsTexture)).getAtlasSprite(Items.compass.getIconIndex(entityitem.getEntityItem()).getIconName());

				if (textureatlassprite.getFrameCount() > 0) {
					textureatlassprite.updateAnimation();
				}
			}
			RenderItem.renderInFrame = true;
			RenderManager.instance.renderEntityWithPosYaw(entityitem, 0.0D, 0.0D, 0.0D, 0.0F, 0.0F);
			RenderItem.renderInFrame = false;

			GL11.glPopMatrix();
        }
    }
	
	public void renderDisplayedName(ShelfTileEntity tile, double x, double y, double z) {
		GL11.glPushMatrix();
		int facing1 = (tile.getWorldObj().getBlockMetadata(tile.xCoord, tile.yCoord, tile.zCoord) & 3);
		float pos = 0F;
		if(facing1 == 0 )pos = 180F;
		else if (facing1 == 1) pos = 90F;
		else if (facing1 == 2) pos = 0F;
		else if (facing1 == 3) pos = 270F;
		if(tile.item.getMaxDamage() > 1){
			renderLivingLabel(tile.item.getMaxDamage() - tile.item.getItemDamage() + "/" + tile.item.getMaxDamage(), x + 0.5D, y + 0.85D, z + 0.5D, pos);
		} else if (tile.item.isStackable()) {
			renderLivingLabel(tile.item.getDisplayName() + " (" + tile.item.stackSize + ")", x + 0.5D, y + 0.85D, z + 0.5D, pos);
		} else {
			renderLivingLabel(tile.item.getDisplayName(), x + 0.5D, y + 0.85D, z + 0.5D, pos);	
		}
        GL11.glPopMatrix();
	}
	
	public void renderLivingLabel(String name, double x, double y, double z, float position) {
		FontRenderer var12 = this.mc.fontRenderer;
		float var13 = 0.75F;
		float var14 = 0.012666668F * var13;
		float var17 = 0F;
		if (var12.getStringWidth(name) > 70) {
			var17 = 0.9F / var12.getStringWidth(name);
		} else {
			var17 = var14;
		}
		GL11.glPushMatrix();
		GL11.glTranslatef((float) x, (float) y, (float) z);
		GL11.glNormal3f(0.0F, 0.0F, 0.0F);
		GL11.glRotatef(position, 0.0F, 1.0F, 0.0F);
		GL11.glScalef(-var17, -var14, var17);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDepthMask(false);
		byte var16 = 0;
		var12.drawString(name, -var12.getStringWidth(name) / 2, var16, 553648127);
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glPopMatrix();
	}
}
