package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemSword;

public class BladeSword extends ItemSword {

    public BladeSword() {
        super(SCPMain.BLADE);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setTextureName(ModInfo.MODID + ":BladeSword");
        this.setUnlocalizedName("BladeSword");
    }
}
