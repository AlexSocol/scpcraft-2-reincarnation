package alexsocol.scprein.entity.mob;

import java.util.Iterator;
import java.util.List;

import alexsocol.scprein.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;

public class StaircaseGhost extends EntityMob {

    public float heightOffset;
    public int heightOffsetUpdateTime;
    public int field_40152_d;
    public int breakLight;
    public int breakNextLight;
    public boolean canSeeSkyAndDay;
    public boolean destroyPerGo;
	
    public StaircaseGhost(World world) {
        super(world);
        this.setSize(0.8F, 2.5F);
        this.setCustomNameTag("SCP-087-1");
        this.setAlwaysRenderNameTag(true);
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(50.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.000001D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(100.0D);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public boolean canTriggerWalking() {
        return false;
    }

    @Override
    public void onLivingUpdate() {
        if (!worldObj.isRemote) {
            heightOffsetUpdateTime--;

            if (heightOffsetUpdateTime <= 0) {
                heightOffsetUpdateTime = 100;
                heightOffset = 0.5F + (float)rand.nextGaussian() * 3F;
            }
        }
        super.onLivingUpdate();
    }
    
    @Override
    public void onUpdate() {
    	AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(this.posX - 2, this.posY, this.posZ - 2, this.posX + 3, this.posY + 3, this.posZ + 3);
    	List list = null;
    	list = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, aabb);
    	if (list != null) {
    		Iterator iterator = list.iterator();
    		EntityPlayer player = null;
	        while(iterator.hasNext()) {
		       player = (EntityPlayer)iterator.next();
		       this.attackEntityAsMob(player);
	        }
    	}
    	
    	destroyPerGo = false;
        this.getNavigator().setSpeed(entityToAttack != null ? 7F : 0.3F);
        isJumping = false;
        int i = MathHelper.floor_double(posX);
        int j = MathHelper.floor_double(posY);
        int k = MathHelper.floor_double(posZ);

        if (worldObj.isDaytime()) {
            float f = getBrightness(1.0F);

            if (f > 0.5F && worldObj.canBlockSeeTheSky(MathHelper.floor_double(posX), MathHelper.floor_double(posY), MathHelper.floor_double(posZ)) && rand.nextFloat() * 30F < (f - 0.4F) * 2.0F) {
                canSeeSkyAndDay = true;
            } else {
                canSeeSkyAndDay = false;
            }
        }
        
        for(int x = -8; x <= 8; x++) {
        	for(int y = -16; y <= 16; y++) {
        		for(int z = -8; z <= 8; z++) {
    		        int meta = this.worldObj.getBlockMetadata(i + x, j + y, k + z);
                    Block block = this.worldObj.getBlock(i + x, j + y, k + z);
        			if(block.getIdFromBlock(block) > 0 && worldObj.getSavedLightValue(EnumSkyBlock.Block, i + x, j + y, k + z) >= 1){
    			        this.worldObj.playAuxSFX(2001, i + x, j + y, k + z, Block.getIdFromBlock(block) + (meta << 12));
    			        block.dropBlockAsItem(this.worldObj, i + x, j + y, k + z, meta, 0);
        				worldObj.setBlock(i + x, j + y, k + z, Blocks.air);
        			}
        		}
        	}
        }


        if (entityToAttack != null && (entityToAttack instanceof EntityPlayer)) {            
            faceEntity(entityToAttack, 100F, 100F);
        }
    	super.onUpdate();
    }
    
    @Override
    public void func_145780_a(int x, int y, int z, Block block) {
    	return;
    }

    @Override
	public int getBrightnessForRender(float par1) {
		return 0xf000f0;
	}

    @Override
	public float getBrightness(float par1) {
		return 1.0F;
	}

    @Override
	public void fall(float f) {
		return;
	}
    
    @Override
	public String getLivingSound() {
		return ModInfo.MODID + ":scp087living";
	}

    @Override
    public String getDeathSound() {
		return "mob.ghast.death";
	}
}
