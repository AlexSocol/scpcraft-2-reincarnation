package alexsocol.scprein.world.generation;

import java.util.Random;

import alexsocol.scprein.SCPMain;
import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;

public class SCPWorldGeneration implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch(world.provider.dimensionId) {
		case 0: generateOverworld(world, random, chunkX*16, chunkZ*16); break;
		case SCPMain.SCP1499ID: generateMoskow(world, random, chunkX*16, chunkZ*16); break;
		}
	}

	public void generateMoskow(World world, Random random, int x, int z) {
		//if (world.rand.nextInt(100) == 0) {
			for (int c = 0; c < 6; c++) {
				int i = x + random.nextInt(16);
				int k = z + random.nextInt(16);
				int j = world.getHeightValue(i, k);
				/*switch(c) {
					case 0: new CubeSmall().generate(world, random, i, j, k); break;
					case 1: new CubeBig().generate(world, random, i, j, k); break;
					case 2: new LadderGiant().generate(world, random, i, j, k); break;
					case 3: new CornerSmall().generate(world, random, i, j, k); break;
					case 4: new CornerBig().generate(world, random, i, j, k); break;
					case 5: new Stadium().generate(world, random, i, j, k); break;
				}*/
				new CubeSmall().generate(world, random, i, j, k);
			
		}
	}

	public void generateOverworld(World world, Random random, int x, int z) {
		
	}
}
