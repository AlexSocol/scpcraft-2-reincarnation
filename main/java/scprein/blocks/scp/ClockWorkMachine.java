package alexsocol.scprein.blocks.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class ClockWorkMachine extends Block implements SCPBlock {

    public ClockWorkMachine() {
        super(Material.iron);
        this.setBlockName("ClockWorkMachine");
        setBlockTextureName(ModInfo.MODID + ":ClockWorkMachine");
        this.setCreativeTab(SCPMain.SCPBlocks);
        this.setHardness(3.0F);
        this.setHarvestLevel("Pickaxe", 5);
        this.setResistance(500.0F);
    }

    @Override
    public void registerBlockIcons(IIconRegister reg) {
        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                icons[i] = reg.registerIcon(textureName + "Front");
            }
            if (i == 1) {
                icons[i] = reg.registerIcon(textureName + "Side");
            }
            if (i == 2) {
                icons[i] = reg.registerIcon(textureName + "Top");
            }
        }
    }

    @Override
    public IIcon getIcon(int side, int meta) {
        switch (side) {
            case 1:
                return icons[2];
            case 2:
                return icons[0];
            default:
                return icons[1];
        }
    }

    private final IIcon[] icons = new IIcon[3];
}
