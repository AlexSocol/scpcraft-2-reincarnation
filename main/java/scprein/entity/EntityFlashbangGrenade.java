package alexsocol.scprein.entity;

import java.util.Iterator;
import java.util.List;

import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.PotionRegistry;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityFlashbangGrenade extends EntityThrowable {

    public EntityFlashbangGrenade(World p_77659_2_, EntityPlayer p_77659_3_) {
        super(p_77659_2_, p_77659_3_);
        setSize(0.5F, 0.5F);
        yOffset = height / 2.0F;
    }

    public EntityFlashbangGrenade(World w) {
        super(w);
    }

    @Override
    public void onUpdate() {
        prevPosX = posX;
        prevPosY = posY;
        prevPosZ = posZ;
        motionY -= 0.039999999105930328D;
        moveEntity(motionX, motionY, motionZ);
        motionX *= 0.98000001907348633D;
        motionY *= 0.98000001907348633D;
        motionZ *= 0.98000001907348633D;
        if (onGround) {
            motionX *= 0.69999998807907104D;
            motionZ *= 0.69999998807907104D;
            motionY *= -0.5D;
        }
        if (fuse++ < 80) {
            worldObj.spawnParticle("smoke", posX, posY + 0.5d, posZ, 0, 0, 0);
        }
        if (fuse++ >= 80) {
            AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBox(posX, posY, posZ, posX + 1, posY + 1, posZ + 1).expand(5, 5, 5);
            List list = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, axisalignedbb);
            Iterator iterator = list.iterator();
            EntityPlayer entityplayer;

            while (iterator.hasNext()) {
                entityplayer = (EntityPlayer) iterator.next();
                entityplayer.addPotionEffect(new PotionEffect(PotionRegistry.potionFlashBangID, 20 * 10, 5));
            }
            setDead();
        }
    }

    @Override
    protected void onImpact(MovingObjectPosition p_70184_1_) {
    }
    private int fuse = 0;
}
