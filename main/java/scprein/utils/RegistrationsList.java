package alexsocol.scprein.utils;

import static cpw.mods.fml.common.registry.GameRegistry.addRecipe;
import static cpw.mods.fml.common.registry.GameRegistry.addShapelessRecipe;
import static cpw.mods.fml.common.registry.GameRegistry.registerBlock;
import static cpw.mods.fml.common.registry.GameRegistry.registerItem;
import static cpw.mods.fml.common.registry.GameRegistry.registerTileEntity;
import static net.minecraft.block.Block.soundTypeCloth;
import static net.minecraft.block.Block.soundTypeGrass;
import static net.minecraft.block.Block.soundTypeGravel;
import static net.minecraft.block.Block.soundTypeMetal;
import static net.minecraft.block.Block.soundTypeSnow;
import static net.minecraft.block.Block.soundTypeStone;
import static net.minecraft.block.Block.soundTypeWood;

import alexsocol.asjlib.BlockPattern;
import alexsocol.asjlib.ItemPattern;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.Alarm;
import alexsocol.scprein.blocks.ArmoredGlass;
import alexsocol.scprein.blocks.BarbedWire;
import alexsocol.scprein.blocks.Camera;
import alexsocol.scprein.blocks.Chair;
import alexsocol.scprein.blocks.CorrodedIronDoor;
import alexsocol.scprein.blocks.Jelly;
import alexsocol.scprein.blocks.KCSlotLevel1;
import alexsocol.scprein.blocks.KCSlotLevel2;
import alexsocol.scprein.blocks.KCSlotLevel3;
import alexsocol.scprein.blocks.KCSlotLevel4;
import alexsocol.scprein.blocks.KCSlotLevel5;
import alexsocol.scprein.blocks.SearchLight;
import alexsocol.scprein.blocks.Shelf;
import alexsocol.scprein.blocks.SlidingDoor;
import alexsocol.scprein.blocks.Smoker;
import alexsocol.scprein.blocks.Table;
import alexsocol.scprein.blocks.scp.BladesTreeLog;
import alexsocol.scprein.blocks.scp.BladesTreeSapling;
import alexsocol.scprein.blocks.scp.BlockPatternSCP;
import alexsocol.scprein.blocks.scp.ClockWorkMachine;
import alexsocol.scprein.blocks.scp.CowBell;
import alexsocol.scprein.blocks.scp.ElectricalWool;
import alexsocol.scprein.blocks.scp.HealingWaterBlock;
import alexsocol.scprein.blocks.scp.InfectionCrystal;
import alexsocol.scprein.blocks.scp.LiquidBloodBlock;
import alexsocol.scprein.blocks.scp.MonsterPot;
import alexsocol.scprein.blocks.scp.RedIce;
import alexsocol.scprein.blocks.tileentity.AlarmTileEntity;
import alexsocol.scprein.blocks.tileentity.BarbedWireTileEntity;
import alexsocol.scprein.blocks.tileentity.CameraTileEntity;
import alexsocol.scprein.blocks.tileentity.ChairTileEntity;
import alexsocol.scprein.blocks.tileentity.KCSlotTileEntity;
import alexsocol.scprein.blocks.tileentity.SearchLightTileEntity;
import alexsocol.scprein.blocks.tileentity.ShelfTileEntity;
import alexsocol.scprein.blocks.tileentity.SmokerTileEntity;
import alexsocol.scprein.blocks.tileentity.TableTileEntity;
import alexsocol.scprein.entity.EntityFlashbangGrenade;
import alexsocol.scprein.entity.EntityGrenade;
import alexsocol.scprein.entity.EntitySmokeGrenade;
import alexsocol.scprein.entity.SCPEntityMountableBlock;
import alexsocol.scprein.entity.mob.BurningMan;
import alexsocol.scprein.entity.mob.DClass;
import alexsocol.scprein.entity.mob.Dedok;
import alexsocol.scprein.entity.mob.Doc;
import alexsocol.scprein.entity.mob.EyesInTheDark;
import alexsocol.scprein.entity.mob.HDReptile;
import alexsocol.scprein.entity.mob.Heart;
import alexsocol.scprein.entity.mob.PeripheralJumper;
import alexsocol.scprein.entity.mob.Sculpture;
import alexsocol.scprein.entity.mob.ShyGuy;
import alexsocol.scprein.entity.mob.StaircaseGhost;
import alexsocol.scprein.entity.mob.TikleMonster;
import alexsocol.scprein.entity.mob.Vialer;
import alexsocol.scprein.items.CorrodedIronDoorItem;
import alexsocol.scprein.items.DClassArmor;
import alexsocol.scprein.items.FlashbangGrenade;
import alexsocol.scprein.items.Grenade;
import alexsocol.scprein.items.GuardArmor;
import alexsocol.scprein.items.SlidingDoorItem;
import alexsocol.scprein.items.SmokeGrenade;
import alexsocol.scprein.items.scp.AvengeApple;
import alexsocol.scprein.items.scp.BladeAxe;
import alexsocol.scprein.items.scp.BladeBucket;
import alexsocol.scprein.items.scp.BladeBucketBlood;
import alexsocol.scprein.items.scp.BladeBucketHeal;
import alexsocol.scprein.items.scp.BladeHoe;
import alexsocol.scprein.items.scp.BladePickaxe;
import alexsocol.scprein.items.scp.BladeSpade;
import alexsocol.scprein.items.scp.BladeStick;
import alexsocol.scprein.items.scp.BladeSword;
import alexsocol.scprein.items.scp.BrokenJian;
import alexsocol.scprein.items.scp.ClockWorkVirus;
import alexsocol.scprein.items.scp.CureAllPill;
import alexsocol.scprein.items.scp.InfiniteFlask;
import alexsocol.scprein.items.scp.InfinitePizza;
import alexsocol.scprein.items.scp.JadeRing;
import alexsocol.scprein.items.scp.LovecraftianLocket;
import alexsocol.scprein.items.scp.SovietGasmask;
import alexsocol.scprein.items.scp.TheBestXXXInTheWorld;
import alexsocol.scprein.items.scp.TheWorldsBestTothBrush;
import alexsocol.scprein.items.scp.ZombiePlague;
import cpw.mods.fml.common.registry.EntityRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityList;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class RegistrationsList {

    // Blocks
	public static Block alarm;
    public static Block armoredGlass;
    public static Block armoredStone;
    public static Block barbedWire;
    public static Block bloodBlock;
    public static Block camera;
    public static Block chair;
    public static Block corrodedIronBlock;
    public static Block corrodedIronDoor;
    public static Block electricalWool;
    public static Block fancyIronBlock;
    public static Block granite;
    public static Block ironGrid;
    public static Block ironWool;
    public static Block jelly;
    public static Block lock1;
    public static Block lock2;
    public static Block lock3;
    public static Block lock4;
    public static Block lock5;
    public static Block reinforcedIronBlock;
    public static Block shelf;
    public static Block slidingDoor;
    public static Block smoker;
    public static Block searchlight;
    public static Block table;
    public static Block vent;

    //SCP-Blocks
    public static Block bladesTreeSapling;
    public static Block bladesTreeLog;
    public static Block bladesTreeLeaves;
    public static Block bladesTreePlanks;
    public static Block clockWorkMachine;
    public static Block cowBell;
    public static Block infectionCrystal;
    public static Block liveRoomFleshWall;
    public static Block livingRoomBoneFloor;
    public static Block monsterVial;
    public static Block moskowDirt;
    public static Block moskowRock;
    public static Block redIce;
    
    // Fluids
    public static Fluid liquidBlood;
    public static Block liquidBloodBlock;
    public static Fluid healingWater;
    public static Block healingWatertBlock;
    
    // Items
    public static Item corrodedIronDoorItem;
    public static Item keyCard1;
    public static Item keyCard2;
    public static Item keyCard3;
    public static Item keyCard4;
    public static Item keyCard5;
    public static Item slidingDoorItem;
    public static Item wrench;
    public static Item wrenchOmni;
    
    // Armor
    public static Item dboots;
    public static Item dpants;
    public static Item dshirt;
    public static Item gasmask;

    public static Item gshoes;
    public static Item glegs;
    public static Item gchest;
    public static Item ghelmet;
    
    //SPC-Items		
    public static Item amulet;
    public static Item apple;
    public static Item boomGrenade;
    public static Item brokenJian;
    public static Item clockworkVirus;
    public static Item drug;
    public static Item flashbangGrenade;
    public static Item flask;
    public static Item sovietGasmask;
    public static Item jadeRing;
    public static Item panacea;
    public static Item pizza;
    public static Item smokeGrenade;
    public static Item toothBrush;
    public static Item zombieVirus;
    
    public static Item bladeAxe;
    public static Item bladeBucket;
    public static Item bladeBucketLiquidBlood;
    public static Item bladeBucketHealingWater;
    public static Item bladeHoe;
    public static Item bladePickaxe;
    public static Item bladeShovel;
    public static Item bladeStick;
    public static Item bladeSword;
	
	public RegistrationsList() {}
	
	public void construct() {
		// Blocks
		alarm = new Alarm();
        armoredGlass = new ArmoredGlass();
        armoredStone = new BlockPattern(ModInfo.MODID, Material.rock, "ArmoredStone", SCPMain.NotSCPBlocks, 24.0F, "pickaxe", 3, 6000.0F, soundTypeStone, true);
        barbedWire = new BarbedWire();
        bloodBlock = new BlockPattern(ModInfo.MODID, Material.rock, "BloodBlock", SCPMain.SCPBlocks, 2.0F, null , 0, 5.0F, soundTypeSnow, true);
        chair = new Chair();
        camera = new Camera();
        corrodedIronBlock = new BlockPattern(ModInfo.MODID, Material.iron, "CorrodedIronBlock", SCPMain.NotSCPBlocks, 16.0F, "pickaxe", 2, 500.0F, soundTypeMetal, true);
        corrodedIronDoor = new CorrodedIronDoor();
        fancyIronBlock = new BlockPattern(ModInfo.MODID, Material.iron, "FancyIron", SCPMain.NotSCPBlocks, 16.0F, "pickaxe", 2, 500.0F, soundTypeMetal, true);
        granite = new BlockPattern(ModInfo.MODID, Material.rock, "Granite", SCPMain.NotSCPBlocks, 24.0F, "pickaxe", 3, 6000.0F, soundTypeStone, true);
        ironGrid = new BlockPattern(ModInfo.MODID, Material.iron, "IronGrid", SCPMain.NotSCPBlocks, 1.0F, "pickaxe", 1, 50.0F, soundTypeMetal, false).setLightOpacity(0);
        ironWool = new BlockPattern(ModInfo.MODID, Material.carpet, "IronWool", SCPMain.NotSCPBlocks, 2.0F, null, 0, 5.0F, soundTypeCloth, true);
        jelly = new Jelly();
        lock1 = new KCSlotLevel1();
        lock2 = new KCSlotLevel2();
        lock3 = new KCSlotLevel3();
        lock4 = new KCSlotLevel4();
        lock5 = new KCSlotLevel5();
        reinforcedIronBlock = new BlockPattern(ModInfo.MODID, Material.iron, "ReinforcedIronBlock", SCPMain.NotSCPBlocks, 20.0F, "pickaxe", 3, 6000.0F, soundTypeMetal, true);
        searchlight = new SearchLight();
        shelf = new Shelf();
        slidingDoor = new SlidingDoor();
        smoker = new Smoker();
        table = new Table();
        vent = new BlockPattern(ModInfo.MODID, Material.iron, "Vent", SCPMain.NotSCPBlocks, 5.0F, "pickaxe", 1, 600.0F, soundTypeMetal, true);

        // SCP-Blocks
        bladesTreeLeaves = new BlockPatternSCP(ModInfo.MODID, Material.leaves, "BladesTreeLeaves", SCPMain.SCPBlocks, 1.0F, null, 0, 50.0F, soundTypeGrass, false);
        bladesTreeLog = new BladesTreeLog();
        bladesTreePlanks = new BlockPatternSCP(ModInfo.MODID, Material.wood, "BladesTreePlanks", SCPMain.SCPBlocks, 2.0F, null, 0, 150.0F, soundTypeWood, true);
        bladesTreeSapling = new BladesTreeSapling();
        clockWorkMachine = new ClockWorkMachine();
        cowBell = new CowBell();
        electricalWool = new ElectricalWool();
        infectionCrystal = new InfectionCrystal();
        liveRoomFleshWall = new BlockPatternSCP(ModInfo.MODID, Material.sponge, "LiveRoomWallBlock", SCPMain.SCPBlocks, 5.0F, null, 0, 5.0F, soundTypeCloth, true);
        livingRoomBoneFloor = new BlockPatternSCP(ModInfo.MODID, Material.rock, "LiveRoomFloorBlock", SCPMain.SCPBlocks, 5.0F, null, 0, 5.0F, soundTypeStone, true);
        monsterVial = new MonsterPot();
        moskowDirt = new BlockPatternSCP(ModInfo.MODID, Material.ground, "MoskowDirt", SCPMain.SCPBlocks, 0, null, 0, Float.MAX_VALUE, soundTypeGravel, true).setBlockUnbreakable();
        moskowRock = new BlockPatternSCP(ModInfo.MODID, Material.rock, "MoskowRock", SCPMain.SCPBlocks, 0, null, 0, Float.MAX_VALUE, soundTypeStone, true).setBlockUnbreakable();
        redIce = new RedIce();

        // Items
        boomGrenade = new Grenade();
        corrodedIronDoorItem = new CorrodedIronDoorItem();
        flashbangGrenade = new FlashbangGrenade();
        keyCard1 = new ItemPattern(ModInfo.MODID, "KeyCardLevel1", SCPMain.NotSCPItems, 1).setFull3D();
        keyCard2 = new ItemPattern(ModInfo.MODID, "KeyCardLevel2", SCPMain.NotSCPItems, 1).setFull3D();
        keyCard3 = new ItemPattern(ModInfo.MODID, "KeyCardLevel3", SCPMain.NotSCPItems, 1).setFull3D();
        keyCard4 = new ItemPattern(ModInfo.MODID, "KeyCardLevel4", SCPMain.NotSCPItems, 1).setFull3D();
        keyCard5 = new ItemPattern(ModInfo.MODID, "KeyCardLevel5", SCPMain.NotSCPItems, 1).setFull3D();
        slidingDoorItem = new SlidingDoorItem();
        smokeGrenade = new SmokeGrenade();
        wrench = new ItemPattern(ModInfo.MODID, "Wrench", SCPMain.NotSCPItems, 1).setFull3D();
        wrenchOmni = new ItemPattern(ModInfo.MODID, "WrenchOmni", SCPMain.NotSCPItems, 1).setFull3D();

        //SCP-Items
        amulet = new LovecraftianLocket();
        apple = new AvengeApple();
        brokenJian = new BrokenJian();
        clockworkVirus = new ClockWorkVirus();
        drug = new TheBestXXXInTheWorld();
        flask = new InfiniteFlask();
        jadeRing = new JadeRing();
        panacea = new CureAllPill();
        pizza = new InfinitePizza();
        sovietGasmask = new SovietGasmask(0, 0).setTextureName(ModInfo.MODID + ":SovietGasmask").setUnlocalizedName("SovietGasmask");
        toothBrush = new TheWorldsBestTothBrush();
        zombieVirus = new ZombiePlague();

        bladeAxe = new BladeAxe();
        bladeBucket = new BladeBucket(Blocks.air);
        bladeBucketLiquidBlood = new BladeBucketBlood();
        bladeBucketHealingWater = new BladeBucketHeal();
        bladeHoe = new BladeHoe();
        bladePickaxe = new BladePickaxe();
        bladeShovel = new BladeSpade();
        bladeStick = new BladeStick();
        bladeSword = new BladeSword();

        // Armor
        dboots = new DClassArmor(0, 3).setTextureName(ModInfo.MODID + ":DClassBotts").setUnlocalizedName("DClassBotts");
        dpants = new DClassArmor(0, 2).setTextureName(ModInfo.MODID + ":DClassPants").setUnlocalizedName("DClassPants");
        dshirt = new DClassArmor(0, 1).setTextureName(ModInfo.MODID + ":DClassShirt").setUnlocalizedName("DClassShirt");
        gasmask = new DClassArmor(0, 0).setTextureName(ModInfo.MODID + ":Gasmask").setUnlocalizedName("Gasmask");
        gshoes = new GuardArmor(0, 3).setTextureName(ModInfo.MODID + ":GuardShoes").setUnlocalizedName("GuardShoes");
        glegs = new GuardArmor(0, 2).setTextureName(ModInfo.MODID + ":GuardLegs").setUnlocalizedName("GuardLegs");
        gchest = new GuardArmor(0, 1).setTextureName(ModInfo.MODID + ":GuardChest").setUnlocalizedName("GuardChest");
        ghelmet = new GuardArmor(0, 0).setTextureName(ModInfo.MODID + ":GuardHelmet").setUnlocalizedName("GuardHelmet");
	}
	
	public static void RegisterFluids() {
        healingWater = new Fluid("healingWater");
        FluidRegistry.registerFluid(healingWater);
        healingWatertBlock = new HealingWaterBlock(healingWater, Material.water);
        registerBlock(healingWatertBlock, "HealingWater");
        healingWater.setUnlocalizedName(healingWatertBlock.getUnlocalizedName()).setDensity(890).setLuminosity(5).setTemperature(300).setViscosity(890);
        
        liquidBlood = new Fluid("liquidBlood");
        FluidRegistry.registerFluid(liquidBlood);
        liquidBloodBlock = new LiquidBloodBlock(liquidBlood, Material.water);
        registerBlock(liquidBloodBlock, "LiquidBlood");
        liquidBlood.setUnlocalizedName(liquidBloodBlock.getUnlocalizedName()).setDensity(1055).setLuminosity(3).setTemperature(309).setViscosity(5000);
	}
	
	public static void RegisterBlocks() {
		registerBlock(alarm, "Alarm");
        registerBlock(armoredGlass, "ArmoredGlass");
        registerBlock(armoredStone, "ArmoredStone");
        registerBlock(barbedWire, "BarbedWire");
		registerBlock(bloodBlock, "BloodBlock");
        registerBlock(camera, "Camera");
        registerBlock(chair, "Chair");
        registerBlock(corrodedIronBlock, "CorrodedIronBlock");
        registerBlock(corrodedIronDoor, "CorrodedIronDoorBlock");
        registerBlock(electricalWool, "ElectricalWool");
        registerBlock(fancyIronBlock, "FancyIronBlock");
        registerBlock(granite, "Granite");
        registerBlock(ironGrid, "IronGrid");
        registerBlock(ironWool, "ironWool");
        registerBlock(jelly, "Jelly");
        registerBlock(lock1, "KeyCardSlot1");
        registerBlock(lock2, "KeyCardSlot2");
        registerBlock(lock3, "KeyCardSlot3");
        registerBlock(lock4, "KeyCardSlot4");
        registerBlock(lock5, "KeyCardSlot5");
        registerBlock(reinforcedIronBlock, "ReinforcedIronBlock");
        registerBlock(searchlight, "SearchLight");
        registerBlock(shelf, "Shelf");
        registerBlock(slidingDoor, "SlidingDoorBlock");
        registerBlock(smoker, "Smoker");
        registerBlock(table, "Table");
        registerBlock(vent, "Vent");
	}
	
	public static void RegisterSCPBlocks() {
        registerBlock(bladesTreeLeaves, "BladesTreeLeaves");
        registerBlock(bladesTreeLog, "BladesTreeLog");
        registerBlock(bladesTreePlanks, "BladesTreePlanks");
        registerBlock(bladesTreeSapling, "BladesTreeSapling");
        registerBlock(clockWorkMachine, "ClockWorkMachine");
        registerBlock(cowBell, "Cowbell");
		registerBlock(infectionCrystal, "InfectionCrystal");
        registerBlock(liveRoomFleshWall, "LiveRoomWallBlock");
        registerBlock(livingRoomBoneFloor, "LiveRoomFloorBlock");
        registerBlock(monsterVial, "MonsterPotBlock");
        registerBlock(moskowDirt, "MoskowDirt");
        registerBlock(moskowRock, "MoskowRock");
        registerBlock(redIce, "RedIce");
	}
	
	public static void RegisterItems() {
		registerItem(corrodedIronDoorItem, "CorrodedIronDoor");
		registerItem(keyCard1, "KeyCardLevel1");
        registerItem(keyCard2, "KeyCardLevel2");
        registerItem(keyCard3, "KeyCardLevel3");
        registerItem(keyCard4, "KeyCardLevel4");
        registerItem(keyCard5, "KeyCardLevel5");
		registerItem(slidingDoorItem, "SlidingDoor");
        registerItem(wrench, "Wrench");
        registerItem(wrenchOmni, "wrenchOmni");
	}
	
	public static void RegisterSCPItems() {
		registerItem(amulet, "LovecraftianLocket");
	    registerItem(apple, "AvengeApple");
	    registerItem(brokenJian, "BrokenJian");
	    registerItem(clockworkVirus, "ClockWorkVirus");
	    registerItem(drug, "TheBestXXXInTheWorld");
	    registerItem(flask, "InfiniteFlask");
	    registerItem(jadeRing, "JadeRing");
	    registerItem(panacea, "CureAllPill");
	    registerItem(pizza, "InfinitePizza");
	    registerItem(sovietGasmask, "SovietGasmask");
	    registerItem(toothBrush, "TheWorldsBestTothBrush");
	    registerItem(zombieVirus, "ZombiePlague");
	    
	    registerItem(bladeBucket, "BladeBucket");
	    registerItem(bladeBucketLiquidBlood, "BladeBucketBlood");
	    registerItem(bladeBucketHealingWater, "BladeBucketHeal");
	    registerItem(bladeStick, "BladeStick");
	    registerItem(bladeSword, "BladeSword");
	    registerItem(bladePickaxe, "BladePickaxe");
	    registerItem(bladeAxe, "BladeAxe");
	    registerItem(bladeShovel, "BladeSpade");
	    registerItem(bladeHoe, "BladeHoe");
	}
	
	public static void RegisterArmor() {
		registerItem(dboots, "DClassBotts");
        registerItem(dpants, "DClassPants");
        registerItem(dshirt, "DClassShirt");
        registerItem(gasmask, "Gasmask");
        
        registerItem(gshoes, "GuardShoes");
        registerItem(glegs, "GuardLegs");
        registerItem(gchest, "GuardChest");
        registerItem(ghelmet, "GuardHelmet");
	}
	
	public static void RegisterEntities() {
		// Entities
        EntityRegistry.registerModEntity(EntitySmokeGrenade.class, "SmokeGrenade", 1, SCPMain.instance, 250, 20, true);
        EntityRegistry.registerModEntity(EntityGrenade.class, "Grenade(CraftZ)", 2, SCPMain.instance, 250, 20, true);
        EntityRegistry.registerModEntity(EntityFlashbangGrenade.class, "FlashbangGrenade", 3, SCPMain.instance, 250, 20, true);
        EntityRegistry.registerModEntity(SCPEntityMountableBlock.class, "Sit", 124, SCPMain.instance, 250, 20, false);
        // Mobs
        EntityList.addMapping(DClass.class, "D-Class", 110, 0xf86200, 0x000000);
        EntityList.addMapping(Vialer.class, "SCP-019-2", 111, 0xff6666, 0x000000);
        EntityList.addMapping(Doc.class, "SCP-049", 112, 0x000000, 0xffffff);
        EntityList.addMapping(Heart.class, "SCP-058", 113, 0x000000, 0x800000);
        EntityList.addMapping(StaircaseGhost.class, "SCP-087-1", 114, 0xffffff, 0xffffff);
        EntityList.addMapping(ShyGuy.class, "SCP-096", 115, 0xedd6aa, 0xd8a239);
        EntityList.addMapping(Dedok.class, "SCP-106", 116, 0x392a2a, 0x191e1e);
        EntityList.addMapping(Sculpture.class, "SCP-173", 117, 0xbea173, 0x00bb00);
        EntityList.addMapping(EyesInTheDark.class, "SCP-280", 118, 0x000000, 0xffffff);
        EntityList.addMapping(PeripheralJumper.class, "SCP-372", 119, 0x00ff00, 0x00ff00);
        EntityList.addMapping(BurningMan.class, "SCP-457", 121, 0x000000, 0xfffb00);
        EntityList.addMapping(HDReptile.class, "SCP-682", 122, 0xd5c1ad, 0xbe2626);
        EntityList.addMapping(TikleMonster.class, "SCP-999", 123, 0xff8000, 0x000000);
	}
	
	public static void RegisterTileEntities() {
		registerTileEntity(AlarmTileEntity.class, "Alarm");
		registerTileEntity(BarbedWireTileEntity.class, "BarbedWire");
		registerTileEntity(CameraTileEntity.class, "Camera");
		registerTileEntity(ChairTileEntity.class, "Chair");
		registerTileEntity(SearchLightTileEntity.class, "Searchlight");
		registerTileEntity(ShelfTileEntity.class, "Shelf");
		registerTileEntity(KCSlotTileEntity.class, "KeyCardSlot");
		registerTileEntity(SmokerTileEntity.class, "Smoker");
		registerTileEntity(TableTileEntity.class, "Table");
	}
	
	public static void RegisterRecipes() {
		// Blocks
		// Shaped
        addRecipe(new ItemStack(reinforcedIronBlock, 1),
        	new Object[]{" I ", "IBI", " I ",
            'B', Blocks.iron_block, 'I', Items.iron_ingot});
        
        addRecipe(new ItemStack(granite, 4),
            new Object[]{"FSF", "SDS", "FSF",
            'D', Items.diamond, 'F', Items.flint, 'S', Blocks.stone});
        
        addRecipe(new ItemStack(barbedWire, 2),
            new Object[]{" I ", "SBS", "PIP",
            'B', Blocks.iron_bars, 'I', Items.iron_ingot, 'P', Items.stick, 'S', Items.string});
        
        addRecipe(new ItemStack(camera, 1),
            new Object[]{"GIB", "SRC", "FIN",
            'B', Blocks.stone_slab, 'C', Items.comparator, 'F', Items.dye, 'G', Blocks.glass_pane, 'I', Items.iron_ingot, 'N', Items.string, 'R', Items.repeater, 'S', Items.redstone});
        
        addRecipe(new ItemStack(table, 1),
            new Object[]{"III", "III", "I I",
            'I', Items.iron_ingot});
        
        addRecipe(new ItemStack(chair, 1),
            new Object[]{"I I", "III", "I I",
            'I', Items.iron_ingot});
        
        addRecipe(new ItemStack(searchlight, 1),
            new Object[]{"GGG", "ILI", "IRI",
            'G', Blocks.glass, 'I', Items.iron_ingot, 'L', Blocks.glowstone, 'R', Items.redstone});
        
        // Shapeless
        addShapelessRecipe(new ItemStack(corrodedIronBlock), Blocks.iron_block, Items.water_bucket);
        addShapelessRecipe(new ItemStack(corrodedIronBlock), reinforcedIronBlock, Items.water_bucket);
        addShapelessRecipe(new ItemStack(corrodedIronBlock), fancyIronBlock, Items.water_bucket);
        addShapelessRecipe(new ItemStack(corrodedIronDoorItem), Items.iron_door, Items.water_bucket);
        addShapelessRecipe(new ItemStack(ironWool, 1), Blocks.wool, Items.iron_ingot, Items.iron_ingot);
        addShapelessRecipe(new ItemStack(ironGrid, 1), Blocks.iron_bars, Blocks.iron_bars, Blocks.iron_bars, Blocks.iron_bars);
        addShapelessRecipe(new ItemStack(armoredGlass, 2), Blocks.glass, ironGrid);
        addShapelessRecipe(new ItemStack(armoredStone, 2), Blocks.stone, ironGrid);
        addShapelessRecipe(new ItemStack(jelly, 1), Items.slime_ball, Items.slime_ball, Items.slime_ball, Items.slime_ball, Items.slime_ball, Items.slime_ball, Items.slime_ball, Items.slime_ball, Items.slime_ball);
        addShapelessRecipe(new ItemStack(bladesTreePlanks, 4), bladesTreeLog);
        addShapelessRecipe(new ItemStack(bladeStick, 4), bladesTreePlanks, bladesTreePlanks);
        
        // Items
        addRecipe(new ItemStack(keyCard1, 1),
            new Object[]{"   ", "PPP", "III",
            'I', Items.iron_ingot, 'P', Items.paper});

        addRecipe(new ItemStack(wrench, 1),
            new Object[]{"G G", "III", " I ",
            'G', Items.gold_ingot, 'I', Items.iron_ingot});
        
        addRecipe(new ItemStack(dboots, 1),
            new Object[]{"   ", "W W", "W W",
            'W', ironWool});
        
        addRecipe(new ItemStack(dpants, 1),
            new Object[]{"WWW", "W W", "W W",
            'W', ironWool});
        
        addRecipe(new ItemStack(dshirt, 1),
            new Object[]{"W W", "WWW", "WWW",
            'W', ironWool});
        
        addRecipe(new ItemStack(gasmask, 1),
            new Object[]{"GWG", "WWW", "BWB",
            'B', Blocks.iron_bars, 'G', Blocks.glass, 'W', ironWool});
        
        addRecipe(new ItemStack(gshoes, 1),
            new Object[]{"   ", "I I", "B B",
            'B', reinforcedIronBlock, 'I', Items.iron_ingot});
        
        addRecipe(new ItemStack(glegs, 1),
            new Object[]{"BBB", "I I", "B B",
            'B', reinforcedIronBlock, 'I', Items.iron_ingot});
        
        addRecipe(new ItemStack(gchest, 1),
            new Object[]{"B B", "III", "BBB",
            'B', reinforcedIronBlock, 'I', Items.iron_ingot});
        
        addRecipe(new ItemStack(ghelmet, 1),
            new Object[]{"BBB", "III", "B B",
            'B', reinforcedIronBlock, 'I', Items.iron_ingot});
        

        // BladeTree stuff
        // Axe
        addRecipe(new ItemStack(bladeAxe, 1),
                new Object[]{"WW ", "WS ", " S ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeAxe, 1),
                new Object[]{" WW", " SW", " S ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeAxe, 1),
                new Object[]{"WW ", "SW ", "S  ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeAxe, 1),
                new Object[]{" WW", " WS", "  S",
                    'S', bladeStick, 'W', bladesTreePlanks});
        // Bucket
        addRecipe(new ItemStack(bladeBucket, 1),
                new Object[]{"   ", "W W", " W ",
                    'W', bladesTreePlanks});
        // Hoe
        addRecipe(new ItemStack(bladeHoe, 1),
                new Object[]{"WW ", " S ", " S ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeHoe, 1),
                new Object[]{" WW", " S ", " S ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeHoe, 1),
                new Object[]{"WW ", "S  ", "S  ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeHoe, 1),
                new Object[]{" WW", "  S", "  S",
                    'S', bladeStick, 'W', bladesTreePlanks});
        // Pickaxe
        addRecipe(new ItemStack(bladePickaxe, 1),
                new Object[]{"WWW", " S ", " S ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        // Shovel
        addRecipe(new ItemStack(bladeShovel, 1),
                new Object[]{"W  ", "S  ", "S  ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeShovel, 1),
                new Object[]{" W ", " S ", " S ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeShovel, 1),
                new Object[]{"  W", "  S", "  S",
                    'S', bladeStick, 'W', bladesTreePlanks});
        // Sword
        addRecipe(new ItemStack(bladeSword, 1),
                new Object[]{"W  ", "W  ", "S  ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeSword, 1),
                new Object[]{" W ", " W ", " S ",
                    'S', bladeStick, 'W', bladesTreePlanks});
        addRecipe(new ItemStack(bladeSword, 1),
                new Object[]{"  W", "  W", "  S",
                    'S', bladeStick, 'W', bladesTreePlanks});
	}
}
