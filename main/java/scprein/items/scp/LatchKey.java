package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class LatchKey extends Item {

    //SCP-005
    public LatchKey() {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":LatchKey");
        this.setUnlocalizedName("LatchKey");
    }
}
