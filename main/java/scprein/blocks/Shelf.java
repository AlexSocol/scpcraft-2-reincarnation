package alexsocol.scprein.blocks;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.ShelfTileEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class Shelf extends BlockContainer {

	public Shelf() {
		super(Material.iron);
		this.setBlockBounds(0F, 0F, 0F, 1F, 0.25F, 1F);
		this.setBlockName("ItemShelf");
		this.setBlockTextureName(ModInfo.MODID + ":FancyIron");
		this.setCreativeTab(SCPMain.NotSCPBlocks);
		this.setHardness(2.0F);
		this.setLightOpacity(0);
		this.setResistance(60.0F);
		this.setStepSound(soundTypeMetal);
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int var2, int var3, int var4, EntityPlayer entityplayer, int var6, float var7, float var8, float var9) {
		ShelfTileEntity te = (ShelfTileEntity) world.getTileEntity(var2, var3, var4);
		ItemStack stack = entityplayer.inventory.getCurrentItem();
		if(entityplayer.isSneaking()) return false;
		if(te != null) {
			if(te.getItem() != null) {
				if(!world.isRemote){
					EntityItem entityitem = new EntityItem(world, var2 + 0.5, var3 + 0.5, var4 + 0.5, te.item);
					world.spawnEntityInWorld(entityitem);				
				}
				te.setItem(null);
			}
			if(stack != null && stack.stackSize > 0) {
				te.setItem(stack.copy());
				te.getItem().stackSize = stack.stackSize;
				stack.stackSize = 0;
				world.setBlockMetadataWithNotify(var2, var3, var4, MathHelper.floor_double((double)((entityplayer.rotationYaw * 4F) / 360F) + 2.5D) & 3, 0x02); //

			}
			
			world.setTileEntity(var2, var3, var4, te);
			world.updateLightByType(EnumSkyBlock.Sky, var2, var3, var4);	
			world.markTileEntityChunkModified(var2, var3, var4, te);
		}
		return true;
	}

	@Override
	public void onNeighborBlockChange(World world, int par2, int par3, int par4, Block block) {
		ShelfTileEntity te = (ShelfTileEntity)world.getTileEntity(par2, par3, par4);
		if (!world.isRemote) {
			world.markTileEntityChunkModified(par2, par3, par4, te);
			super.onNeighborBlockChange(world, par2, par3, par4, block);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
    public int getMixedBrightnessForBlock(IBlockAccess world, int x, int y, int z) {
		return world.getLightBrightnessForSkyBlocks(x, y, z, world.getBlock(x, y, z).getLightValue(world, x, y - 1, z));
    }
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
		ShelfTileEntity te = (ShelfTileEntity)world.getTileEntity(x, y, z);
		if(te != null) {
			if(te.getItem() != null) {
				EntityItem entityitem = new EntityItem(world, x + 0.5, y + 0.5, z + 0.5, te.getItem());
				world.spawnEntityInWorld(entityitem);
				te.setItem(null);
			}
		}
		super.breakBlock(world, x, y, z, block, meta);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta) {
		return new ShelfTileEntity();
	}
}