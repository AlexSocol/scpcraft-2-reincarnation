package alexsocol.scprein.entity.mob;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.utils.AISCPList;

public class Sculpture extends EntityMob {

	public int slowPeriod;
	public int timeTillNextTeleport;
	public boolean timeLocked;
	
    public Sculpture(World world) {
        super(world);
        this.setSize(0.8F, 2.8F);
        this.setCustomNameTag("SCP-173");
        this.setAlwaysRenderNameTag(true);
		this.stepHeight = 4.0F;
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(Double.MAX_VALUE);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(12.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(200.0D);
    }
    
    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public boolean isEntityInvulnerable() {
        return true;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
	public Entity findPlayerToAttack() {
		EntityPlayer entityplayer = worldObj.getClosestPlayerToEntity(this, 64D);
		if (entityplayer != null && AISCPList.canSCPBeSeen(this, entityplayer)) {
			return entityplayer;
		} else {
			return null;
		}
	}

    @Override
	public void attackEntity(Entity entity, float f) {		
		if (entityToAttack != null && (entityToAttack instanceof EntityPlayer) && !AISCPList.canSCPBeSeen(this, (EntityPlayer)entityToAttack)) {
			if (rand.nextInt(20) != 0) {
				super.attackEntity(entity, f);
			}
		}
	}

    @Override
	public void updateEntityActionState() {
		if (entityToAttack != null) {
			if (!AISCPList.canSCPBeSeen(this, (EntityPlayer)entityToAttack)) {
				super.updateEntityActionState();
			}
		} else {
			super.updateEntityActionState();
		}
	}

    @Override
    public void onCollideWithPlayer(EntityPlayer par1EntityPlayer) {}

    @Override
	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2) {       
		return false;
	}

    @Override
	public void onLivingUpdate() {			
        getNavigator().setSpeed(80.0D);
		isJumping = false;
		super.onLivingUpdate();
	}

    @Override
	public void onUpdate() {	
		isJumping = false;
		if (entityToAttack != null && (entityToAttack instanceof EntityPlayer)) {
			if (!AISCPList.canSCPBeSeen(this, (EntityPlayer)entityToAttack)) {
				if (getDistanceToEntityToAttack() > 63D && timeTillNextTeleport-- < 0) {
					timeTillNextTeleport = rand.nextInt(60) + 20;
				}
			}

			if (slowPeriod > 0) {
				slowPeriod--;
				entityToAttack.motionX *= 0.01D;
				entityToAttack.motionZ *= 0.01D;
			}

			if ((entityToAttack instanceof EntityPlayer) && (AISCPList.canSCPBeSeen(this, (EntityPlayer)entityToAttack) || timeLocked)) {
				AISCPList.SCPDirectLook(this, (EntityPlayer)entityToAttack);
				moveStrafing = moveForward = 0.0F;
				getNavigator().setSpeed(0.0F);

			} else {
				faceEntity(entityToAttack, 100F, 100F);
			}
		}
		super.onUpdate();
	}

    @Override
    public String getLivingSound() {
		return ModInfo.MODID + ":scp173living";
    }
	
	/** Get step sound */
	public void func_145780_a(int p_145780_1_, int p_145780_2_, int p_145780_3_, Block p_145780_4_) {
		this.playSound(ModInfo.MODID + ":scp173step", 1.0F, this.worldObj.rand.nextFloat() * 0.1F + 0.9F);
    }
    
	public double getDistanceToEntityToAttack() {
		if (entityToAttack instanceof EntityPlayer) {
			double d = entityToAttack.posX - posX;
			double d2 = entityToAttack.posY - posY;
			double d4 = entityToAttack.posZ - posZ;
			return (double)MathHelper.sqrt_double(d * d + d2 * d2 + d4 * d4);
		}
		
		EntityPlayer entityplayer = worldObj.getClosestPlayerToEntity(this, 64D);

		if (entityplayer != null) {
			double d1 = entityplayer.posX - posX;
			double d3 = entityplayer.posY - posY;
			double d5 = entityplayer.posZ - posZ;
			return (double)MathHelper.sqrt_double(d1 * d1 + d3 * d3 + d5 * d5);
		} else {
			return 40000D;
		}
	}
	
    /*public boolean isSeenByTarget(EntityLivingBase player, EntityLiving stalker) {
        Vec3 visionVec = player.getLook(1.0F).normalize();
        Vec3 targetVec = Vec3.createVectorHelper(stalker.posX - player.posX, stalker.boundingBox.minY + (double) (stalker.height / 2.0F) - (player.posY + (double) player.getEyeHeight()), stalker.posZ - player.posZ);
        targetVec = targetVec.normalize();
        double dotProduct = visionVec.dotProduct(targetVec);
        boolean inFOV = dotProduct > 0.1 && player.canEntityBeSeen(stalker);
        return inFOV;
    }

    @Override
    public boolean isAIEnabled() {
        return getPlayerTarget() != null;
    }

    @Override
    public EntityLivingBase getAttackTarget() {
        return getPlayerTarget();
    }

    public EntityPlayer getPlayerTarget() {
        int distance = 15;
        AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBox(posX, posY, posZ, posX + 1, posY + 1, posZ + 1).expand(distance, distance, distance);
        List<EntityPlayer> list = worldObj.getEntitiesWithinAABB(EntityPlayer.class, axisalignedbb);
        List<EntityPlayer> targetsList = new ArrayList<EntityPlayer>();
        for(EntityPlayer pl: list){
        	if(!pl.capabilities.isCreativeMode&&!isSeenByTarget(pl,this)) {
        		targetsList.add(pl);
        	}
    	}
    	return !targetsList.isEmpty() ? targetsList.get(0) : null;
    }*/
	
	// From Repository
	/*public boolean isSeenByTarget(EntityLivingBase player, EntityLiving stalker) {
        Vec3 visionVec = player.getLook(1.0F).normalize();
        Vec3 targetVec = Vec3.createVectorHelper(stalker.posX - player.posX,
                stalker.boundingBox.minY + (double) (stalker.height / 2.0F) - (player.posY + (double) player.getEyeHeight()),
                stalker.posZ - player.posZ);
        targetVec = targetVec.normalize();
        double dotProduct = visionVec.dotProduct(targetVec);
        boolean inFOV = dotProduct > 0.1 && player.canEntityBeSeen(stalker);
        return inFOV;
    }

    @Override
    protected boolean isAIEnabled() {
        return getPlayerTarget() != null;
    }

    @Override
    public EntityLivingBase getAttackTarget() {
        return getPlayerTarget();
    }

    public EntityPlayer getPlayerTarget() {
        int distance = 15;
        AxisAlignedBB axisalignedbb = AxisAlignedBB.getBoundingBox(posX, posY, posZ, posX + 1, posY + 1, posZ + 1).expand(distance, distance, distance);
        List<EntityPlayer> list = worldObj.getEntitiesWithinAABB(EntityPlayer.class, axisalignedbb);
        List<EntityPlayer> targetsList = new ArrayList<>();
        list.stream().filter((player) -> !player.capabilities.isCreativeMode && !isSeenByTarget(player, this)).forEach(targetsList::add);
        return !targetsList.isEmpty() ? targetsList.get(0) : null;
    }*/
}