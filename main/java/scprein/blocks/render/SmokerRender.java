package alexsocol.scprein.blocks.render;

import org.lwjgl.opengl.GL11;

import alexsocol.scprein.blocks.model.SmokerModel;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class SmokerRender extends TileEntitySpecialRenderer {

    public static final SmokerModel model = new SmokerModel();
    public static final ResourceLocation texture = new ResourceLocation("scprein:textures/blocks/Smoker.png");

    @Override
    public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float scale) {
        GL11.glPushMatrix();
        GL11.glTranslated(x + 0.5, y + 1.5, z + 0.5);
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        if (tile != null) {
            switch(tile.getBlockMetadata()) {
            case 0: GL11.glRotated(180.0F, 1.0F, 0.0F, 0.0F); GL11.glTranslatef(0.0F, -2.0F, 0.0F); break; // From Bottom
            case 1: break; // From Top
            case 2: GL11.glRotated(90.0F, 1.0F, 0.0F, 0.0F); GL11.glRotated(180.0F, 0.0F, 1.0F, 0.0F); GL11.glTranslatef(0.0F, -1.0F, 1.0F); break; // To South
            case 3: GL11.glRotated(-90.0F, 1.0F, 0.0F, 0.0F); GL11.glTranslatef(0.0F, -1.0F, 1.0F); break; // To North
            case 4: GL11.glRotated(90.0F, 0.0F, 0.0F, 1.0F); GL11.glRotated(90.0F, 0.0F, 1.0F, 0.0F); GL11.glTranslatef(0.0F, -1.0F, 1.0F);  break; // To East
            case 5: GL11.glRotated(-90.0F, 0.0F, 0.0F, 1.0F); GL11.glRotated(-90.0F, 0.0F, 1.0F, 0.0F); GL11.glTranslatef(0.0F, -1.0F, 1.0F); break; // To West
            }
        }
        Minecraft.getMinecraft().renderEngine.bindTexture(texture);
        model.render(0.0625F);
        GL11.glPopMatrix();
    }
}
