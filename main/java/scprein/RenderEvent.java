package alexsocol.scprein;

import alexsocol.scprein.utils.PotionRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.event.EntityViewRenderEvent;

import org.lwjgl.opengl.GL11;

public class RenderEvent {

    private static final Minecraft mc = Minecraft.getMinecraft();

    @SubscribeEvent
    public void fogDistance(EntityViewRenderEvent.RenderFogEvent e) {
        EntityPlayer player = mc.thePlayer;
        if (player.isPotionActive(PotionRegistry.potionFlashBangID)) {
            GL11.glFogf(GL11.GL_FOG_START, 0.7F);
            GL11.glFogf(GL11.GL_FOG_END, 3.5F);
        }
    }

    @SubscribeEvent
    public void fogColor(EntityViewRenderEvent.FogColors e) {
        EntityPlayer player = mc.thePlayer;
        if (player.isPotionActive(PotionRegistry.potionFlashBangID)) {
            e.red = 1F;
            e.green = 1F;
            e.blue = 1F;
        }
    }
}
