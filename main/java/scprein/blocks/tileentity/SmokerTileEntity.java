package alexsocol.scprein.blocks.tileentity;

import java.util.Iterator;
import java.util.List;

import alexsocol.scprein.SCPMain;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;

public class SmokerTileEntity extends TileEntity {
	
	public void updateEntity() {
		super.updateEntity();
		double pmotionX = 0, pmotionY = 0, pmotionZ = 0;
		int meta = this.blockMetadata;
		double speed = 0.2D;
		switch(meta) {
			case 0: pmotionY -= speed; break;
			case 1: pmotionY += speed; break;
			case 2: pmotionZ -= speed; break;
			case 3: pmotionZ += speed; break;
			case 4: pmotionX -= speed; break;
			case 5: pmotionX += speed; break;
		}
		
		for (int i = 0; i < 1; i++) {
			SCPMain.proxy.generateSmokeParticle(this.worldObj, xCoord + 0.5D + pmotionX*i, yCoord + 0.5D + pmotionY*i, zCoord + 0.5D + pmotionZ*i, pmotionX, pmotionY, pmotionZ);
		}
	}
}
