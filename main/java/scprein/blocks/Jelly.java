package alexsocol.scprein.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.Facing;
import net.minecraft.world.IBlockAccess;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Jelly extends Block {

    public Jelly() {
        super(Material.cake);
        this.setBlockName("Jelly");
        this.setBlockTextureName(ModInfo.MODID + ":Jelly");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(0.5F);
        this.setLightOpacity(0);
        this.setResistance(1000.0F);
        this.setStepSound(soundTypeCloth);
    }

	@SideOnly(Side.CLIENT)
    @Override
    public int getRenderBlockPass() {
        return 1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess access, int x, int y, int z, int side) {
		Block block = access.getBlock(x, y, z);

		if (access.getBlockMetadata(x, y, z) != access.getBlockMetadata(x - Facing.offsetsXForSide[side], y - Facing.offsetsYForSide[side], z - Facing.offsetsZForSide[side])) {
			return true;
		}
		
		return block == this ? false : super.shouldSideBeRendered(access, x, y, z, side);
	}
}
