package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemHoe;

public class BladeHoe extends ItemHoe {

    public BladeHoe() {
        super(SCPMain.BLADE);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setTextureName(ModInfo.MODID + ":BladeHoe");
        this.setUnlocalizedName("BladeHoe");
    }
}
