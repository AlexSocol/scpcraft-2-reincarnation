package alexsocol.scprein.proxy;

import alexsocol.scprein.RenderEvent;
import alexsocol.scprein.blocks.SlidingDoor;
import alexsocol.scprein.blocks.render.BarbedWireRender;
import alexsocol.scprein.blocks.render.CameraRender;
import alexsocol.scprein.blocks.render.ChairRender;
import alexsocol.scprein.blocks.render.KCSlotRender;
import alexsocol.scprein.blocks.render.SearchLightRender;
import alexsocol.scprein.blocks.render.ShelfRender;
import alexsocol.scprein.blocks.render.SlidingDoorRender;
import alexsocol.scprein.blocks.render.SmokerRender;
import alexsocol.scprein.blocks.render.TableRender;
import alexsocol.scprein.blocks.scp.render.CowBellRender;
import alexsocol.scprein.blocks.scp.render.MonsterPotRender;
import alexsocol.scprein.blocks.scp.tileentity.CowBellTileEntity;
import alexsocol.scprein.blocks.scp.tileentity.MonsterPotTileEntity;
import alexsocol.scprein.blocks.tileentity.BarbedWireTileEntity;
import alexsocol.scprein.blocks.tileentity.CameraTileEntity;
import alexsocol.scprein.blocks.tileentity.ChairTileEntity;
import alexsocol.scprein.blocks.tileentity.KCSlotTileEntity;
import alexsocol.scprein.blocks.tileentity.SearchLightTileEntity;
import alexsocol.scprein.blocks.tileentity.ShelfTileEntity;
import alexsocol.scprein.blocks.tileentity.SmokerTileEntity;
import alexsocol.scprein.blocks.tileentity.TableTileEntity;
import alexsocol.scprein.entity.EntityFlashbangGrenade;
import alexsocol.scprein.entity.EntityGrenade;
import alexsocol.scprein.entity.EntitySmokeGrenade;
import alexsocol.scprein.entity.RenderGrenade;
import alexsocol.scprein.entity.mob.BurningMan;
import alexsocol.scprein.entity.mob.DClass;
import alexsocol.scprein.entity.mob.Dedok;
import alexsocol.scprein.entity.mob.Doc;
import alexsocol.scprein.entity.mob.EyesInTheDark;
import alexsocol.scprein.entity.mob.HDReptile;
import alexsocol.scprein.entity.mob.Heart;
import alexsocol.scprein.entity.mob.PeripheralJumper;
import alexsocol.scprein.entity.mob.Sculpture;
import alexsocol.scprein.entity.mob.ShyGuy;
import alexsocol.scprein.entity.mob.StaircaseGhost;
import alexsocol.scprein.entity.mob.TikleMonster;
import alexsocol.scprein.entity.mob.Vialer;
import alexsocol.scprein.entity.mob.model.BurningManModel;
import alexsocol.scprein.entity.mob.model.DedokModel;
import alexsocol.scprein.entity.mob.model.DocModel;
import alexsocol.scprein.entity.mob.model.EyesInTheDarkModel;
import alexsocol.scprein.entity.mob.model.HDReptileModel;
import alexsocol.scprein.entity.mob.model.HeartModel;
import alexsocol.scprein.entity.mob.model.PeripheralJumperModel;
import alexsocol.scprein.entity.mob.model.SculptureModel;
import alexsocol.scprein.entity.mob.model.ShyGuyModel;
import alexsocol.scprein.entity.mob.model.StaircaseGhostModel;
import alexsocol.scprein.entity.mob.model.TikleMonsterModel;
import alexsocol.scprein.entity.mob.model.VialerModel;
import alexsocol.scprein.entity.mob.render.BurningManRender;
import alexsocol.scprein.entity.mob.render.DClassRender;
import alexsocol.scprein.entity.mob.render.DedokRender;
import alexsocol.scprein.entity.mob.render.DocRender;
import alexsocol.scprein.entity.mob.render.EyesInTheDarkRender;
import alexsocol.scprein.entity.mob.render.HDReptileRender;
import alexsocol.scprein.entity.mob.render.HeartRender;
import alexsocol.scprein.entity.mob.render.PeripheralJumperRender;
import alexsocol.scprein.entity.mob.render.SculptureRender;
import alexsocol.scprein.entity.mob.render.ShyGuyRender;
import alexsocol.scprein.entity.mob.render.StaircaseGhostRender;
import alexsocol.scprein.entity.mob.render.TikleMonsterRender;
import alexsocol.scprein.entity.mob.render.VialerRender;
import alexsocol.scprein.entity.particle.SmokeParticleEntity;
import alexsocol.scprein.proxy.network.ClientEventHandler;
import alexsocol.scprein.utils.RegistrationsList;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.GameData;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy {

	public static final int slidingRenderID = RenderingRegistry.getNextAvailableRenderId();
	
    @Override
    public void registerRenderThings() {
        RenderEvent renderEvent = new RenderEvent();
        //Blocks
        ClientRegistry.bindTileEntitySpecialRenderer(BarbedWireTileEntity.class, new BarbedWireRender());
        ClientRegistry.bindTileEntitySpecialRenderer(CameraTileEntity.class, new CameraRender());
        ClientRegistry.bindTileEntitySpecialRenderer(CowBellTileEntity.class, new CowBellRender());
        ClientRegistry.bindTileEntitySpecialRenderer(ChairTileEntity.class, new ChairRender());
        ClientRegistry.bindTileEntitySpecialRenderer(KCSlotTileEntity.class, new KCSlotRender());
        ClientRegistry.bindTileEntitySpecialRenderer(MonsterPotTileEntity.class, new MonsterPotRender());
        ClientRegistry.bindTileEntitySpecialRenderer(SearchLightTileEntity.class, new SearchLightRender());
        ClientRegistry.bindTileEntitySpecialRenderer(ShelfTileEntity.class, new ShelfRender());
        ClientRegistry.bindTileEntitySpecialRenderer(SmokerTileEntity.class, new SmokerRender());
        ClientRegistry.bindTileEntitySpecialRenderer(TableTileEntity.class, new TableRender());
        
        RenderingRegistry.registerBlockHandler(RegistrationsList.slidingDoor.getRenderType(), new SlidingDoorRender());
        
        //Mobs
        RenderingRegistry.registerEntityRenderingHandler(EntitySmokeGrenade.class, new RenderGrenade());
        RenderingRegistry.registerEntityRenderingHandler(EntityGrenade.class, new RenderGrenade());
        RenderingRegistry.registerEntityRenderingHandler(EntityFlashbangGrenade.class, new RenderGrenade());

        RenderingRegistry.registerEntityRenderingHandler(DClass.class, new DClassRender(new ModelBiped(), 0.5F));
        RenderingRegistry.registerEntityRenderingHandler(Vialer.class, new VialerRender(new VialerModel(), 0.1F));
        RenderingRegistry.registerEntityRenderingHandler(Doc.class, new DocRender(new DocModel(), 0.5F));
        RenderingRegistry.registerEntityRenderingHandler(Heart.class, new HeartRender(new HeartModel(), 0.25F));
        RenderingRegistry.registerEntityRenderingHandler(StaircaseGhost.class, new StaircaseGhostRender(new StaircaseGhostModel(), 0F));
        RenderingRegistry.registerEntityRenderingHandler(ShyGuy.class, new ShyGuyRender(new ShyGuyModel(), 0.5F));
        RenderingRegistry.registerEntityRenderingHandler(Dedok.class, new DedokRender(new DedokModel(), 0.5F));
        RenderingRegistry.registerEntityRenderingHandler(Sculpture.class, new SculptureRender(new SculptureModel(), 0.5F));
        RenderingRegistry.registerEntityRenderingHandler(EyesInTheDark.class, new EyesInTheDarkRender(new EyesInTheDarkModel(), 0F));
        RenderingRegistry.registerEntityRenderingHandler(PeripheralJumper.class, new PeripheralJumperRender(new PeripheralJumperModel(), 0F));
        RenderingRegistry.registerEntityRenderingHandler(BurningMan.class, new BurningManRender(new BurningManModel(), 0F));
        RenderingRegistry.registerEntityRenderingHandler(HDReptile.class, new HDReptileRender(new HDReptileModel(), 1.5F));
        RenderingRegistry.registerEntityRenderingHandler(TikleMonster.class, new TikleMonsterRender(new TikleMonsterModel(), 0.25F));
        MinecraftForge.EVENT_BUS.register(new RenderEvent());
        FMLCommonHandler.instance().bus().register(new RenderEvent());
    }
    
    public void initializeAndRegisterHandlers() {
    	super.initializeAndRegisterHandlers();
    	MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
		FMLCommonHandler.instance().bus().register(new ClientEventHandler());
    }
    
	public void generateSmokeParticle(World world, double X, double Y, double Z, double mx, double my, double mz) {
		 EntityFX particle = new SmokeParticleEntity(world, X, Y, Z, mx, my, mz);
		 Minecraft.getMinecraft().effectRenderer.addEffect(particle);        
	}
}
