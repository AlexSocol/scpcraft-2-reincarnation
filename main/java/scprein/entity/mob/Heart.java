package alexsocol.scprein.entity.mob;

import alexsocol.scprein.ModInfo;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.item.EntityPainting;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class Heart extends EntityMob {

    public Heart(World world) {
        super(world);
        this.setSize(0.6F, 0.5F);
        this.setCustomNameTag("SCP-058");
        this.setAlwaysRenderNameTag(true);
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(150.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(12.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(10.0D);
    }

    @Override
	public boolean canDespawn() {
		return false;
	}

    @Override
    public void onUpdate() {
        addPotionEffect(new PotionEffect(Potion.resistance.id, 10, 2));
        super.onUpdate();
    }

    @Override
    public String getLivingSound() {
    	return ModInfo.MODID + ":scp058living";
    }

    @Override
    public String getHurtSound() {
        return "mob.spider.say";
    }

    @Override
    public String getDeathSound() {
        return "mob.spider.death";
    }

    @Override
    public void func_145780_a(int p_145780_1_, int p_145780_2_, int p_145780_3_, Block p_145780_4_) {
        this.playSound("mob.spider.step", 0.15F, 1.0F);
    }
    
    @Override
    public boolean attackEntityAsMob(Entity entity) {
        if (entity instanceof EntityLivingBase) {
            int b0 = worldObj.difficultySetting == EnumDifficulty.HARD ? 15 : 7;
            if (b0 > 0) {
                ((EntityLivingBase) entity).addPotionEffect(new PotionEffect(Potion.wither.id, b0 * 15, 3));
            }
            if (entity instanceof EntityPlayer && rand.nextInt(4) == 0) say((EntityPlayer)entity);
        }
        return super.attackEntityAsMob(entity);
    }
    
    public void say(EntityPlayer player) {
    	int q = rand.nextInt(8);
    	if(player != null) {
    		if(q == 1) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-058: " + EnumChatFormatting.ITALIC.WHITE + "I had dreams of the queen wonders that lived inside the hearts of love and silent treatments of all the elderly that I knew were once whole."));
    		if(q == 2) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-058: " + EnumChatFormatting.ITALIC.WHITE + "I seek the revelations of all that the holy told to the unwise in the dreams of cold embers in sunlight that fade across lakes of black blood and snakes that eat the loaves of children from lamb trees in autumn."));
    		if(q == 3) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-058: " + EnumChatFormatting.ITALIC.WHITE + "Endless suffering is the woe of ignorant men who never lack to seek the depth of their own hearts and only see the wealth of a poor world suffering to flay its own back in knife wounds of silver and brutal gladness."));
    		if(q == 4) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-058: " + EnumChatFormatting.ITALIC.WHITE + "The nightmare is a dream to the nameless slug that wanders across minefield and the remains of deer and kings."));
    		if(q == 5) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-058: " + EnumChatFormatting.ITALIC.WHITE + "Nightshade is shadows in all honest blinks that sort through the bile of newborn plagues, instant warmth is a mother's milk in dreams before anything was ever evil."));
    		if(q == 6) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-058: " + EnumChatFormatting.ITALIC.WHITE + "In seconds the sun is beating like drums in all hearts eat the ear of noise."));
    		if(q == 7) player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_BLUE + "SCP-058: " + EnumChatFormatting.ITALIC.WHITE + "The sensual violence of lust is all the assurance you will ever need to know the worth of life."));
    	}
    }
}
