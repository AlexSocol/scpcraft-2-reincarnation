package alexsocol.scprein.entity.mob.render;

import alexsocol.scprein.ModInfo;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class TikleMonsterRender extends RenderLiving {

	public static final ResourceLocation texture = new ResourceLocation(ModInfo.MODID, "textures/entity/TikleMonster.png");

    public TikleMonsterRender(ModelBase par1ModelBase, float par2) {
        super(par1ModelBase, par2);
    }

    @Override
    public ResourceLocation getEntityTexture(Entity par1Entity) {
        return texture;
    }
}
