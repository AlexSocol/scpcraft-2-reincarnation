package alexsocol.scprein.items;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class DClassArmor extends ItemArmor {

    public DClassArmor(int id, int armorType) {
        super(SCPMain.DClass, id, armorType);
        this.setCreativeTab(SCPMain.NotSCPItems);
        this.setMaxStackSize(1);
    }

    public String getTextureName(int slot) {
        return ModInfo.MODID + ":textures/model/armor/DClassArmor_" + (slot == 2 ? 2 : 1) + ".png";
    }

    @Override
    public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, String type) {
        return getTextureName(slot);
    }
}
