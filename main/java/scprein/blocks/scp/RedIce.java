package alexsocol.scprein.blocks.scp;

import java.util.Random;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.block.BlockPackedIce;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class RedIce extends BlockPackedIce implements SCPBlock {

    //SCP-009
    public RedIce() {
        this.setBlockName("RedIce");
        this.setBlockTextureName(ModInfo.MODID + ":RedIce");
        this.setCreativeTab(SCPMain.SCPBlocks);
        this.setHardness(5.0F);
        this.setHarvestLevel("Pickaxe", 1);
        this.setResistance(15.0F);
        this.setStepSound(soundTypeGlass);
        setTickRandomly(true);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public void updateTick(World world, int x, int y, int z, Random random) {
    	if (!world.isRemote) {
	        if (random.nextInt(100) > 70) {
	            if (world.getBlock(x, y + 1, z) == Blocks.air) {
	                world.setBlock(x, y + 1, z, RegistrationsList.redIce);
	            }
	            if (world.getBlock(x, y - 1, z) == Blocks.air) {
	                world.setBlock(x, y - 1, z, RegistrationsList.redIce);
	            }
	            if (world.getBlock(x + 1, y, z) == Blocks.air) {
	                world.setBlock(x + 1, y, z, RegistrationsList.redIce);
	            }
	            if (world.getBlock(x - 1, y, z) == Blocks.air) {
	                world.setBlock(x - 1, y, z, RegistrationsList.redIce);
	            }
	            if (world.getBlock(x, y, z + 1) == Blocks.air) {
	                world.setBlock(x, y, z + 1, RegistrationsList.redIce);
	            }
	            if (world.getBlock(x, y, z - 1) == Blocks.air) {
	                world.setBlock(x, y, z - 1, RegistrationsList.redIce);
	            }
	        }
    	}
    }
}