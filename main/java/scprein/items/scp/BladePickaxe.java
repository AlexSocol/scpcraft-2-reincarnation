package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;

public class BladePickaxe extends ItemPickaxe {

    public BladePickaxe() {
        super(SCPMain.BLADE);
        this.setCreativeTab(SCPMain.SCPItems);
        this.setTextureName(ModInfo.MODID + ":BladePickaxe");
        this.setUnlocalizedName("BladePickaxe");
    }
}
