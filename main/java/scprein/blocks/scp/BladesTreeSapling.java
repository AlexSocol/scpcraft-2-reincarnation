package alexsocol.scprein.blocks.scp;

import java.util.Random;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.block.BlockBush;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class BladesTreeSapling extends BlockBush implements SCPBlock {

    public BladesTreeSapling() {
        this.setBlockName("BladesTreeSapling");
        this.setBlockTextureName(ModInfo.MODID + ":BladesTreeSapling");
        this.setCreativeTab(SCPMain.SCPBlocks);
        this.setHardness(0.01F);
        this.setResistance(0.01F);
        this.setStepSound(soundTypeGrass);
        setTickRandomly(true);
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public int getRenderType() {
        return 1;
    }

    public boolean isAreaEmpty(World w, int x, int y, int z) {
        for (int i = -3; i <= 3; i++) {
            for (int k = -3; k <= 3; k++) {
                for (int h = 0; h <= 8; h++) {
                    if (i == 0 && k == 0 && h == 0) {
                        continue;
                    }
                    if (!w.isAirBlock(x + i, y + h, z + k)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void updateTick(World world, int par2, int par3, int par4, Random random) {
        if (isAreaEmpty(world, par2, par3, par4)) {
            for (int y = 1; y <= 6; y++) {
                if ((y == 3) || (y == 4)) {
                    for (int x = -2; x <= 2; x++) {
                        for (int z = -2; z <= 2; z++) {
                            world.setBlock(par2 + x, par3 + y, par4 + z, RegistrationsList.bladesTreeLeaves);
                        }
                    }
                }
                if (y == 4) {
                    world.setBlock(par2 - 2, par3 + 4, par4 - 2, Blocks.air);
                    world.setBlock(par2 - 2, par3 + 4, par4 + 2, Blocks.air);
                    world.setBlock(par2 + 2, par3 + 4, par4 - 2, Blocks.air);
                    world.setBlock(par2 + 2, par3 + 4, par4 + 2, Blocks.air);
                }
                if ((y == 5) || (y == 6)) {
                    for (int x = -1; x <= 1; x++) {
                        for (int z = -1; z <= 1; z++) {
                            world.setBlock(par2 + x, par3 + y, par4 + z, RegistrationsList.bladesTreeLeaves);
                        }
                    }
                }
                if (y == 6) {
                    world.setBlock(par2 - 1, par3 + 6, par4 - 1, Blocks.air);
                    world.setBlock(par2 - 1, par3 + 6, par4 + 1, Blocks.air);
                    world.setBlock(par2 + 1, par3 + 6, par4 - 1, Blocks.air);
                    world.setBlock(par2 + 1, par3 + 6, par4 + 1, Blocks.air);
                    world.setBlock(par2, par3 + 1, par4, RegistrationsList.bladesTreeLog);
                    world.setBlock(par2, par3 + 2, par4, RegistrationsList.bladesTreeLog);
                    world.setBlock(par2, par3 + 3, par4, RegistrationsList.bladesTreeLog);
                    world.setBlock(par2, par3 + 4, par4, RegistrationsList.bladesTreeLog);
                    world.setBlock(par2, par3 + 5, par4, RegistrationsList.bladesTreeLog);
                }
                if (world.getBlock(par2, par3, par4) == RegistrationsList.bladesTreeSapling) {
                    world.setBlock(par2, par3, par4, RegistrationsList.bladesTreeLog);
                }
            }
        }
    }
}
