package alexsocol.scprein.proxy;

import alexsocol.gasgiants.world.dimension.Jupiter.WorldProviderJupiterLower;
import alexsocol.gasgiants.world.dimension.Jupiter.WorldProviderJupiterMiddle;
import alexsocol.gasgiants.world.dimension.Jupiter.WorldProviderJupiterUpper;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.proxy.network.CommonEventHandler;
import alexsocol.scprein.utils.RegistrationsList;
import alexsocol.scprein.world.dimension.Dim1499.WorldProvider1499;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;

public class CommonProxy {

	public RegistrationsList regs;
	
	public void generateSmokeParticle(World world, double X, double Y, double Z, double mx, double my, double mz) {}
	
    public void registerRenderThings() {}
    
    public CommonProxy() {
		regs = new RegistrationsList();
	}
    
    public void preinit() {
    	regs.construct();
    	regs.RegisterBlocks();
    	regs.RegisterSCPBlocks();
    	regs.RegisterFluids();
    	regs.RegisterItems();
    	regs.RegisterSCPItems();
    	regs.RegisterArmor();
    	regs.RegisterEntities();
    	regs.RegisterTileEntities();
    	regs.RegisterRecipes();
    	
    	DimensionManager.registerProviderType(SCPMain.SCP1499ID, WorldProvider1499.class, false);
		DimensionManager.registerDimension(SCPMain.SCP1499ID, SCPMain.SCP1499ID);
	}
    
    public void initializeAndRegisterHandlers() {
    	MinecraftForge.EVENT_BUS.register(new CommonEventHandler());
		FMLCommonHandler.instance().bus().register(new CommonEventHandler());
		NetworkRegistry.INSTANCE.registerGuiHandler(SCPMain.instance, SCPMain.instance.guiHandler);
    }
}
