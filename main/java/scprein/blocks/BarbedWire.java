package alexsocol.scprein.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.blocks.tileentity.BarbedWireTileEntity;

public class BarbedWire extends Block implements ITileEntityProvider {

    public BarbedWire() {
        super(Material.iron);
        this.setBlockName("BarbedWire");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(1.5F);
        this.setHarvestLevel("pickaxe", 2);
        this.setResistance(360.0F);
        setBlockTextureName(ModInfo.MODID + ":BarbedWireIcon");
    }

    @Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new BarbedWireTileEntity();
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int par2, int par3, int par4) {
        return null;
    }

    @Override
    public void onEntityCollidedWithBlock(World world, int par2, int par3, int par4, Entity entity) {
        entity.attackEntityFrom(DamageSource.cactus, 2.0F);
        entity.setInWeb();
    }

    public int getMobilityFlag() {
        return 1;
    }
    
    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack) {
        int l = MathHelper.floor_double((double)(entity.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
        world.setBlockMetadataWithNotify(x, y, z, l, 2);
    }
}
