package alexsocol.scprein.entity.mob;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.utils.AISCPList;

public class EyesInTheDark extends EntityMob {
	
    public EyesInTheDark(World world) {
        super(world);
        this.setSize(0.8F, 1.8F);
        this.setCustomNameTag("SCP-280");
        this.setAlwaysRenderNameTag(true);
    }

    @Override
    public void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(25.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(2.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(5.0D);
    }

    @Override
    public void onLivingUpdate() {
        int i = MathHelper.floor_double(posX);
        int j = MathHelper.floor_double(posY);
        int i1 = MathHelper.floor_double(posZ);
        
        if (!worldObj.isRemote) {
            if((worldObj.isDaytime() && worldObj.canBlockSeeTheSky(i, j, i1)) || worldObj.getSavedLightValue(EnumSkyBlock.Block, i, j, i1) > 6) {
                AISCPList.teleportRandomly(this);
                entityToAttack = null;
            }
        }
        
        if(entityToAttack != null) {
        	AISCPList.teleportToEntity(this.entityToAttack, this);
        }

        super.onLivingUpdate();
    }

    
    @Override
	public boolean canDespawn() {
		return false;
	}
    
    @Override
	public String getLivingSound() {
		return ModInfo.MODID + ":scp280living";
	}

    @Override
    public String getDeathSound() {
		return "mob.ghast.death";
	}
}
