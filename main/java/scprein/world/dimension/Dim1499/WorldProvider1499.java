package alexsocol.scprein.world.dimension.Dim1499;

import alexsocol.asjlib.ChunkProviderFlat;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.proxy.ClientProxy;
import alexsocol.scprein.utils.RegistrationsList;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.Vec3;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.WorldChunkManagerHell;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraftforge.client.IRenderHandler;

public class WorldProvider1499 extends WorldProvider {

	public void registerWorldChunkManager() {
		this.dimensionId = SCPMain.SCP1499ID;
		this.getStarBrightness(0.0F);
		this.getSunBrightness(1.0F);
		this.isHellWorld = false;
		this.worldChunkMgr = new WorldChunkManagerHell(BiomeGenBase.frozenOcean, 0.0F);
		this.setSpawnPoint(0, 250, 0);
	}

	public IChunkProvider createChunkGenerator() {
		return new ChunkProviderFlat(this.worldObj, this.worldObj.getSeed(), "2;" + Block.getIdFromBlock(RegistrationsList.moskowDirt) + ";1");
	}

	public boolean isSurfaceWorld() {
		return true;
	}

	public boolean canCoordinateBeSpawn(int par1, int par2) {
		return false;
	}

	public boolean canRespawnHere() {
		return false;
	}

	public String getDimensionName() {
		return "SCP1499";
	}

	@Override
	public double getMovementFactor() {
		return 0.0;
    }
	
	@Override
	public int getRespawnDimension(EntityPlayerMP player) {
        return 0;
    }

	@Override
	public boolean canDoRainSnowIce(Chunk chunk) {
        return false;
    }
	
	@SideOnly(Side.CLIENT)
    public Vec3 getSkyColor(Entity entity, float partialTicks) {
        return Vec3.createVectorHelper(1.0F, 1.0F, 1.0F);
    }
	
	@Override
    public float calculateCelestialAngle(long par1, float par3) {
        return 0;
    }

	@SideOnly(Side.CLIENT)
	public boolean doesXZShowFog(int par1, int par2) {
		return false;
	}
	
	@SideOnly(Side.CLIENT)
    public Vec3 getFogColor(float f1, float f2) {
		return Vec3.createVectorHelper(1.0D, 1.0D, 1.0D);
    }
	
	@SideOnly(Side.CLIENT)
	public double getVoidFogYFactor() {
		return 0;
	}
}