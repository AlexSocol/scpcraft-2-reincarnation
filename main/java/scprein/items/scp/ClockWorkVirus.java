package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ClockWorkVirus extends Item {

    //SCP-217
    public ClockWorkVirus() {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":ClockWorkVirus");
        this.setUnlocalizedName("ClockWorkVirus");
    }
}
