package alexsocol.scprein.blocks.tileentity;

import alexsocol.scprein.blocks.KCSlotLevel1;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class KCSlotTileEntity extends TileEntity {
	
	public int timer;
	
	public KCSlotTileEntity() {
		timer = 0;
	}
	
	@Override
	public void updateEntity() {
		if (timer > 0) timer--;
		if (timer == 0) {
			this.worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, this.getBlockMetadata() & 3, 2);
		}
		notifyAboutChange(worldObj, xCoord, yCoord, zCoord, this.blockMetadata);
	}

	public void notifyAboutChange(World world, int x, int y, int z, int meta) {
		worldObj.notifyBlocksOfNeighborChange(x, y, z, worldObj.getBlock(x, y, z));
		worldObj.notifyBlocksOfNeighborChange(x - 1, y, z, worldObj.getBlock(x, y, z));
		worldObj.notifyBlocksOfNeighborChange(x + 1, y, z, worldObj.getBlock(x, y, z));
		worldObj.notifyBlocksOfNeighborChange(x, y, z - 1, worldObj.getBlock(x, y, z));
		worldObj.notifyBlocksOfNeighborChange(x, y, z + 1, worldObj.getBlock(x, y, z));
		worldObj.notifyBlocksOfNeighborChange(x, y - 1, z, worldObj.getBlock(x, y, z));
		worldObj.notifyBlocksOfNeighborChange(x, y + 1, z, worldObj.getBlock(x, y, z));
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		this.timer = nbt.getInteger("timer");
		super.readFromNBT(nbt);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		nbt.setInteger("timer", this.timer);
		super.writeToNBT(nbt);
	}
}
