package alexsocol.scprein.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.Facing;
import net.minecraft.world.IBlockAccess;

public class ArmoredGlass extends Block {

    public ArmoredGlass() {
        super(Material.glass);
        this.setBlockName("ArmoredGlass");
        this.setBlockTextureName(ModInfo.MODID + ":ArmoredGlass");
        this.setCreativeTab(SCPMain.NotSCPBlocks);
        this.setHardness(16.0F);
        this.setHarvestLevel("pickaxe", 3);
        this.setLightOpacity(0);
        this.setResistance(6000.0F);
        this.setStepSound(soundTypeMetal);
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

	@SideOnly(Side.CLIENT)
	public int getRenderBlockPass() {
		return 0;
	}

	public boolean renderAsNormalBlock() {
		return false;
	}
	
	@Override
	protected boolean canSilkHarvest() {
		return true;
	}

	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess access, int x, int y, int z, int side) {
		Block block = access.getBlock(x, y, z);

		if (access.getBlockMetadata(x, y, z) != access.getBlockMetadata(x - Facing.offsetsXForSide[side], y - Facing.offsetsYForSide[side], z - Facing.offsetsZForSide[side])) {
			return true;
		}
		
		return block == this ? false : super.shouldSideBeRendered(access, x, y, z, side);
	}
}
