package alexsocol.scprein.entity.mob.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class EyesInTheDarkModel extends ModelBase {

    ModelRenderer head;
    ModelRenderer body;
    ModelRenderer rightarm;
    ModelRenderer leftarm;
    ModelRenderer rightarmo;
    ModelRenderer leftarmo;
    ModelRenderer bodya;
    ModelRenderer bodyb;
    ModelRenderer bodyc;
    ModelRenderer bodyd;
    ModelRenderer bodye;
    ModelRenderer bodyf;
    ModelRenderer bodyg;
    ModelRenderer bodyh;
    ModelRenderer bodyi;
    ModelRenderer rightarma;

    public EyesInTheDarkModel() {
        textureWidth = 64;
        textureHeight = 32;
        head = new ModelRenderer(this, 0, 0);
        head.addBox(-4F, -8F, -4F, 8, 8, 8);
        head.setRotationPoint(0F, 0F, 0F);
        head.setTextureSize(64, 32);
        head.mirror = true;
        setRotation(head, 0F, 0F, 0F);
        body = new ModelRenderer(this, 16, 16);
        body.addBox(-4F, 0F, -2F, 1, 3, 1);
        body.setRotationPoint(7F, 10F, 1F);
        body.setTextureSize(64, 32);
        body.mirror = true;
        setRotation(body, 0F, 0F, 0F);
        rightarm = new ModelRenderer(this, 40, 16);
        rightarm.addBox(3F, 0F, 9F, 3, 6, 4);
        rightarm.setRotationPoint(1F, 2F, -8.7F);
        rightarm.setTextureSize(64, 32);
        rightarm.mirror = true;
        setRotation(rightarm, -0.6283185F, 0F, -0.1919862F);
        leftarm = new ModelRenderer(this, 40, 16);
        leftarm.addBox(-1F, -2F, -2F, 3, 10, 4);
        leftarm.setRotationPoint(5F, 2F, 0F);
        leftarm.setTextureSize(64, 32);
        leftarm.mirror = true;
        setRotation(leftarm, -0.0523599F, 0F, -0.1919862F);
        rightarmo = new ModelRenderer(this, 40, 16);
        rightarmo.addBox(-3F, -2F, -2F, 3, 10, 4);
        rightarmo.setRotationPoint(-4F, 2F, 0F);
        rightarmo.setTextureSize(64, 32);
        rightarmo.mirror = true;
        setRotation(rightarmo, -0.0523599F, 0F, 0.1919862F);
        leftarmo = new ModelRenderer(this, 40, 16);
        leftarmo.addBox(-1F, -2F, -2F, 3, 10, 4);
        leftarmo.setRotationPoint(5F, 2F, 0F);
        leftarmo.setTextureSize(64, 32);
        leftarmo.mirror = true;
        setRotation(leftarmo, -0.0523599F, 0F, -0.1919862F);
        bodya = new ModelRenderer(this, 16, 16);
        bodya.addBox(-4F, 0F, -2F, 8, 10, 4);
        bodya.setRotationPoint(0F, 0F, 0F);
        bodya.setTextureSize(64, 32);
        bodya.mirror = true;
        setRotation(bodya, 0F, 0F, 0F);
        bodyb = new ModelRenderer(this, 16, 16);
        bodyb.addBox(-4F, 0F, -2F, 1, 3, 1);
        bodyb.setRotationPoint(0F, 10F, 0F);
        bodyb.setTextureSize(64, 32);
        bodyb.mirror = true;
        setRotation(bodyb, 0F, 0F, 0F);
        bodyc = new ModelRenderer(this, 16, 16);
        bodyc.addBox(-4F, 0F, -2F, 1, 3, 1);
        bodyc.setRotationPoint(0F, 11F, 3F);
        bodyc.setTextureSize(64, 32);
        bodyc.mirror = true;
        setRotation(bodyc, 0F, 0F, 0F);
        bodyd = new ModelRenderer(this, 16, 16);
        bodyd.addBox(-4F, 0F, -2F, 1, 1, 1);
        bodyd.setRotationPoint(2F, 12F, 3F);
        bodyd.setTextureSize(64, 32);
        bodyd.mirror = true;
        setRotation(bodyd, 0F, 0F, 0F);
        bodye = new ModelRenderer(this, 16, 16);
        bodye.addBox(-4F, 0F, -2F, 1, 2, 1);
        bodye.setRotationPoint(5F, 10F, 3F);
        bodye.setTextureSize(64, 32);
        bodye.mirror = true;
        setRotation(bodye, 0F, 0F, 0F);
        bodyf = new ModelRenderer(this, 16, 16);
        bodyf.addBox(-4F, 0F, -2F, 1, 2, 1);
        bodyf.setRotationPoint(7F, 10F, 0F);
        bodyf.setTextureSize(64, 32);
        bodyf.mirror = true;
        setRotation(bodyf, 0F, 0F, 0F);
        bodyg = new ModelRenderer(this, 16, 16);
        bodyg.addBox(-4F, 0F, -2F, 1, 3, 1);
        bodyg.setRotationPoint(2F, 11F, 0F);
        bodyg.setTextureSize(64, 32);
        bodyg.mirror = true;
        setRotation(bodyg, 0F, 0F, 0F);
        bodyh = new ModelRenderer(this, 16, 16);
        bodyh.addBox(-4F, 0F, -2F, 1, 1, 1);
        bodyh.setRotationPoint(5F, 12F, 0F);
        bodyh.setTextureSize(64, 32);
        bodyh.mirror = true;
        setRotation(bodyh, 0F, 0F, 0F);
        bodyi = new ModelRenderer(this, 16, 16);
        bodyi.addBox(-4F, 0F, -2F, 1, 1, 1);
        bodyi.setRotationPoint(4F, 10F, 0F);
        bodyi.setTextureSize(64, 32);
        bodyi.mirror = true;
        setRotation(bodyi, 0F, 0F, 0F);
        rightarma = new ModelRenderer(this, 40, 16);
        rightarma.addBox(-3F, 0F, 9F, 3, 6, 4);
        rightarma.setRotationPoint(-4F, 2F, -8.7F);
        rightarma.setTextureSize(64, 32);
        rightarma.mirror = true;
        setRotation(rightarma, -0.6283185F, 0F, 0.1919862F);
    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        head.render(f5);
        body.render(f5);
        rightarm.render(f5);
        leftarm.render(f5);
        rightarmo.render(f5);
        leftarmo.render(f5);
        bodya.render(f5);
        bodyb.render(f5);
        bodyc.render(f5);
        bodyd.render(f5);
        bodye.render(f5);
        bodyf.render(f5);
        bodyg.render(f5);
        bodyh.render(f5);
        bodyi.render(f5);
        rightarma.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}
