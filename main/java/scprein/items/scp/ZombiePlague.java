package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class ZombiePlague extends Item {

    //SCP-008
    public ZombiePlague() {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":ZombiePlague");
        this.setUnlocalizedName("ZombiePlague");
    }

    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player) {
        if (!world.isRemote) {
            player.addPotionEffect((new PotionEffect(Potion.hunger.id, 1200, 4)));
            player.addPotionEffect((new PotionEffect(Potion.resistance.id, 1200, 2)));
            player.addPotionEffect((new PotionEffect(Potion.moveSlowdown.id, 1200, 4)));
            player.addPotionEffect((new PotionEffect(Potion.poison.id, 1200, 4)));
            player.addPotionEffect((new PotionEffect(Potion.wither.id, 1200, 2)));
            MinecraftServer.getServer().addChatMessage(new ChatComponentText("[Securiry]: Atention to all SCP personnel!"));
            MinecraftServer.getServer().addChatMessage(new ChatComponentText("[Securiry]: SCP-008 Special Contaiment Procedures have been violated!"));
            MinecraftServer.getServer().addChatMessage(new ChatComponentText("[Securiry]: Virus infection occurred!"));
            MinecraftServer.getServer().addChatMessage(new ChatComponentText("[Securiry]: Immediately take control of infected!"));
        }
        return itemstack;
    }
}
