package alexsocol.scprein.entity.mob.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class VialerModel extends ModelBase {

    //fields
    ModelRenderer Shape11;
    ModelRenderer Shape12;
    ModelRenderer Shape13;
    ModelRenderer Shape14;
    ModelRenderer Shape15;
    ModelRenderer Shape16;
    ModelRenderer Shape17;
    ModelRenderer Shape18;
    ModelRenderer Shape19;

    public VialerModel() {
        textureWidth = 64;
        textureHeight = 32;
        Shape11 = new ModelRenderer(this, 0, 0);
        Shape11.addBox(-1F, 1F, -1F, 1, 3, 1);
        Shape11.setRotationPoint(2F, 19.5F, 2F);
        Shape11.setTextureSize(64, 32);
        Shape11.mirror = true;
        setRotation(Shape11, -0.2268928F, 0F, -0.1919862F);
        Shape12 = new ModelRenderer(this, 12, 0);
        Shape12.addBox(0F, 0F, 0F, 3, 3, 2);
        Shape12.setRotationPoint(-1F, 18.5F, -1F);
        Shape12.setTextureSize(64, 32);
        Shape12.mirror = true;
        setRotation(Shape12, 0F, 0F, 0F);
        Shape13 = new ModelRenderer(this, 0, 0);
        Shape13.addBox(-1F, 1F, -2F, 1, 1, 2);
        Shape13.setRotationPoint(-0.4F, 20.5F, 2F);
        Shape13.setTextureSize(64, 32);
        Shape13.mirror = true;
        setRotation(Shape13, 0.9075712F, 0.3490659F, 0F);
        Shape14 = new ModelRenderer(this, 0, 0);
        Shape14.addBox(0F, 1F, -1F, 1, 3, 1);
        Shape14.setRotationPoint(-1F, 19.5F, 2F);
        Shape14.setTextureSize(64, 32);
        Shape14.mirror = true;
        setRotation(Shape14, -0.2268928F, 0F, 0.1919862F);
        Shape15 = new ModelRenderer(this, 0, 0);
        Shape15.addBox(0F, -0.2F, 0F, 3, 1, 1);
        Shape15.setRotationPoint(-1F, 21.3F, 2.5F);
        Shape15.setTextureSize(64, 32);
        Shape15.mirror = true;
        setRotation(Shape15, 1.029744F, 0F, 0F);
        Shape16 = new ModelRenderer(this, 0, 0);
        Shape16.addBox(-0.7F, 1.2F, -1.4F, 1, 2, 1);
        Shape16.setRotationPoint(-0.4F, 21.5F, 3F);
        Shape16.setTextureSize(64, 32);
        Shape16.mirror = true;
        setRotation(Shape16, 0F, 0.3490659F, 0F);
        Shape17 = new ModelRenderer(this, 0, 0);
        Shape17.addBox(0F, 1F, -2F, 1, 1, 2);
        Shape17.setRotationPoint(1.4F, 20.5F, 2F);
        Shape17.setTextureSize(64, 32);
        Shape17.mirror = true;
        setRotation(Shape17, 0.9075712F, -0.3490659F, 0F);
        Shape18 = new ModelRenderer(this, 0, 0);
        Shape18.addBox(-0.3F, 1.2F, -1.4F, 1, 2, 1);
        Shape18.setRotationPoint(1.4F, 21.5F, 3F);
        Shape18.setTextureSize(64, 32);
        Shape18.mirror = true;
        setRotation(Shape18, 0F, -0.3490659F, 0F);
        Shape19 = new ModelRenderer(this, 0, 0);
        Shape19.addBox(0F, 1F, -1F, 3, 2, 1);
        Shape19.setRotationPoint(-1F, 19.5F, 0F);
        Shape19.setTextureSize(64, 32);
        Shape19.mirror = true;
        setRotation(Shape19, 1.291544F, 0F, 0F);
    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        Shape11.render(f5);
        Shape12.render(f5);
        Shape13.render(f5);
        Shape14.render(f5);
        Shape15.render(f5);
        Shape16.render(f5);
        Shape17.render(f5);
        Shape18.render(f5);
        Shape19.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z) {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity) {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}
