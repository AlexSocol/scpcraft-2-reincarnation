package alexsocol.scprein.proxy.network;

import alexsocol.asjlib.Utilities;
import alexsocol.scprein.SCPMain;
import alexsocol.scprein.entity.mob.Dedok;
import alexsocol.scprein.potions.PotionBlinkDelay;
import alexsocol.scprein.utils.PotionRegistry;
import alexsocol.scprein.utils.RegistrationsList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class CommonEventHandler {
	
	@SubscribeEvent
	public void onEntityHurt(LivingHurtEvent event) {
		if (event.entityLiving instanceof Dedok && event.source.equals(DamageSource.inWall)) {
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event) {
		if (event.entityLiving.isPotionActive(PotionRegistry.potionCrystalID)) {
			event.entityLiving.attackEntityFrom(DamageSource.wither, 1);
			event.entityLiving.motionX = event.entityLiving.motionZ = event.entityLiving.moveForward = event.entityLiving.moveStrafing = 0;
		}
	}
	
	@SubscribeEvent
	public void onPlayerUpdate(LivingUpdateEvent event) {
		if (event.entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) event.entity;				
			int X = (int) player.posX;
			int Y = (int) player.posY;
			int Z = (int) player.posZ;

			if (player.posX < 0) {
				X = (int) player.posX - 1;
			}
			if (player.posZ < 0) {
				Z = (int) player.posZ - 1;
			}
			
			if (!player.capabilities.isCreativeMode) {
				if (!player.isPotionActive(PotionRegistry.potionBlinkDelayID)) {
					player.addPotionEffect(new PotionEffect(PotionRegistry.potionBlinkDelayID, 160, 0));
				} else if (player.isPotionActive(PotionRegistry.potionBlinkDelayID) && player.getActivePotionEffect(PotionBlinkDelay.instance).getDuration() <= 20) {
					player.addPotionEffect(new PotionEffect(Potion.blindness.id, 25, 173));
				}
				if (player.isPotionActive(Potion.blindness) && player.getActivePotionEffect(Potion.blindness).getAmplifier() == 173 && player.getActivePotionEffect(Potion.blindness).getDuration() <= 24) {
					player.removePotionEffect(Potion.blindness.id);
				}
				if (player.isPotionActive(PotionRegistry.potionCrystalID)) {
					player.attackEntityFrom(DamageSource.wither, 1);
					player.motionX = player.motionZ = player.moveForward = player.moveStrafing = 0;
				}	
				if((player.getCurrentArmor(3) != null && player.dimension != SCPMain.SCP1499ID && player.getCurrentArmor(3).getItem().equals(RegistrationsList.sovietGasmask))) {
					Utilities.sendToDimensionWithoutPortal(player, SCPMain.SCP1499ID, player.posX, player.worldObj.getTopSolidOrLiquidBlock((int)player.posX, (int)player.posZ) + 1.0, player.posZ);
				} else if ((((player.getCurrentArmor(3) != null && !player.getCurrentArmor(3).getItem().equals(RegistrationsList.sovietGasmask)) || player.getCurrentArmor(3) == null) && player.dimension == SCPMain.SCP1499ID)) {
					Utilities.sendToDimensionWithoutPortal(player, 0, player.worldObj.getSpawnPoint().posX, player.worldObj.getSpawnPoint().posY, player.worldObj.getSpawnPoint().posZ);
				}
			}
		}
	}
}
