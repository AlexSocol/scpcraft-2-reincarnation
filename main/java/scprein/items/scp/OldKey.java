package alexsocol.scprein.items.scp;

import alexsocol.scprein.ModInfo;
import alexsocol.scprein.SCPMain;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class OldKey extends Item {

    //SCP-004-2
    public OldKey(String number) {
        this.setCreativeTab(SCPMain.SCPItems);
        this.setFull3D();
        this.setMaxStackSize(1);
        this.setTextureName(ModInfo.MODID + ":OldKey");
        this.setUnlocalizedName("OldKey" + number);
    }
}
